                if (!data.TimekeepingLicense) { data.TimekeepingLicense = 'Hourly Timekeeping' }
                var LicenseMap = []
                if (CheckEmpty(data.TimekeepingLicense) == true){LicenseMap.push(data.TimekeepingLicense)}
                if (CheckEmpty(data.SchedulingLicense) == true){LicenseMap.push(data.SchedulingLicense)}
                if (CheckEmpty(data.WAMLicense) == true){LicenseMap.push(data.WAMLicense)}
                if (CheckEmpty(data.AnalyticsLicense) == true){LicenseMap.push(data.AnalyticsLicense)}
                if (CheckEmpty(data.WorkLicense) == true){LicenseMap.push(data.WorkLicense)}
                if (CheckEmpty(data.EmployeeLicense) == true){LicenseMap.push(data.EmployeeLicense)}
                if (CheckEmpty(data.ManagerLicense) == true){LicenseMap.push(data.ManagerLicense)}

                LicenseMap = LicenseMap
                    .filter(function (e) {
                        return e;
                    })
                    .map(function (y) {
                        return {
                            "activeFlag": true,
                            "licenseTypeName": y
                        }
                    })

                /*
                if (data.cdataCSV != "" && data.cdataCSV != null) {
                    var CustomDataMap =
                        data.cdataCSV
                            .split('|')
                            .map(function (y) {
                                return {
                                    "customDataTypeName": y.split(':')[0],
                                    "text": y.split(':')[1]
                                }
                            })
                }
                else CustomDataMap = []
                */

                //CUSTOM DATA
                cdata = []
                if (data.cdataNames.indexOf('|') > - 1 && data.cdataValues.indexOf('|') > - 1) {
                    names = data.cdataNames.split('|')
                    vals = data.cdataValues.split('|')

                    names.forEach((v,k) => {
                        cdata.push(
                            {
                                "customDataTypeName": v,
                                "text": vals[k]
                            }
                        )
                    });

                }

                 //CUSTOM DATES
                 cdates = []
                 if (data.cdateNames.indexOf('|') > - 1 && data.cdateValues.indexOf('|') > - 1) {
                     names = data.cdateNames.split('|')
                     vals = data.cdateValues.split('|')
 
                     names.forEach((v,k) => {
                        cdates.push(
                             {
                                 "customDateTypeName": v,
                                 "text": vals[k]
                             }
                         )
                     });
                 }

                function piDateLogic(data,key) {
                    if (data.masterEffDate) return data.masterEffDate
                    else if (data[key]) return data[key]
                    else return data.hireDate
                }

                x = [{
                    "personIdentity": {
                        "personNumber": data.personNum
                    },
                    "gdapAssignments": [{
                        "gdapName": data.gdapName,
                        "defaultSwitch":true,
                        "role": "MANAGER_ROLE",
                        "effectiveDate": ()=>{ if (data.gdapEffDt) return data.gdapEffDt; else return new Date().toISOString().split('T')[0]}, "expirationDate": "3000-01-01"
                    }],
                    "jobAssignment": {
                        "scheduleGroupName": data.scheduleGroup,
                        "jobAssignmentDetails": {
                            "payRuleEffectiveDate": piDateLogic(data,'payRuleEffectiveDate'),
                            "payRuleName": data.payRule,
                            "timeZoneName": data.timezone,
                            "deviceGroupName": data.deviceGroupName,
                            "workerTypeName": data.workerType,
                            "supervisorPersonNumber": data.supervisorID

                        },
                        "employmentTermAssignments": [{
                            "startDate": piDateLogic(data,'empTermstartDate'), "endDate": "3000-01-01",
                            "name": data.employmentTerm

                        }],
                        "baseWageRates": [{
                            "effectiveDate": piDateLogic(data,'wageRateEffDate'), "expirationDate": "3000-01-01",
                            "hourlyRate": parseInt(data.wageRate, 10)
                        }],

                        "primaryLaborAccounts": [{
                            "effectiveDate": piDateLogic(data,'primlabEffDate'), "expirationDate": "3000-01-01",
                            "organizationPath": data.primaryJob,
                            "laborCategoryName": data.laborCategory
                        }]
                    },
                    "personInformation": {
                        "employeeCurrencyAssignment": { "currencyCode": data.employeeCurrency },


                        "telephoneNumbers": [
                            {
                                "contactTypeName": "MOBILE", "isSMSSwitch": data.isSMSSwitch, "phoneNumber": data.phoneNumber
                            }

                        ],
                        "personAuthenticationTypes": [{ "authenticationTypeName": data.authenticationTypeName, "activeFlag": true }],
                        "customDateList": cdates,
                        "postalAddresses": [
                            {
                                "city": data.city, "state": data.state, "postalCode": data.postalCode, "country": data.country
                            }
                        ],
                        "expectedHoursList": [
                            {
                                "timePeriodTypeName": "WEEKLY", "quantity": data.weeklyHours
                            },
                            {
                                "timePeriodTypeName": "DAILY", "quantity": data.dailyHours
                            },
                            {
                                "timePeriodTypeName": "PAY_PERIOD", "quantity": data.payperiodHours
                            }

                        ],
                        "emailAddresses": [
                            {
                                "contactTypeName": "WORK", "address": data.email
                            }
                        ],
                        "customDataList": cdata,
                        "accessAssignment": {
                            "accessProfileName": data.accessProfileName,
                            "timeEntryTypeName": data.timeEntryTypeName,
                            //"timeEntryTypeEffectiveDate": new Date().toISOString().split('T')[0],
                            "timeEntryTypeEffectiveDate": piDateLogic(data,'timeEntryTypeEffectiveDate'),
                            "preferenceProfileName": data.preferenceProfileName,
                            "notificationProfileName": data.notProfileName,
                            "localePolicyName": data.localePolicyName,
                            "managerWorkRuleName": data.managerWorkRule,
                            "managerViewPayCodeName": data.managerPCV,
                            "managerPayCodeName": data.managerPCE,
                            "managerLaborCategoryProfileName": data.managerLCP,
                            "professionalPayCodeName": data.employeePCE,
                            "professionalWorkRuleName": data.professionalWorkRule,
                            "employeeLaborCategoryProfileName": data.employeeLCP,
                            "schedulePatternName": data.schedulePatternName,
                            "shiftCodeName": data.shiftCodeName,
                            "reportName": data.reportName,
                            "groupScheduleName": data.groupScheduleName,
                            "forecastingCategoryProfileName": data.forecastingCategoryProfileName,
                            "mgrEmplLaborCategoryProfileName": data.mgrEmplLaborCategoryProfileName,
                            "delegateProfileName": data.delegateProfileName

                        },
                        "personAccessAssignments": [{
                            "effectiveDate": piDateLogic(data,'empMgrProfEffDate'), "expirationDate": "3000-01-01",
                            "managerEmployeeGroupName": data.employeeGroup,
                            "managerTransferOrganizationSetName": data.organisationalSet,
                            "empMgrTransferOrganizationSetName": data.empMgrTransferOrganizationSetName,
                            "professionalTransferOrganizationSetName": data.JTS,
                            "homeHyperFindQueryName": data.homeHyperFindQueryName
                        }],
                        "person": {
                            "personNumber": data.personNum,
                            "lastName": data.lastName,
                            "hireDate": data.hireDate,
                            "middleInitial": data.middleInitial,
                            "shortName": data.shortName,
                            "birthDate": data.birthDate,
                            "firstName": data.firstName,
                            "accrualProfileName": data.accrualProfile,
                            "accrualProfileEffectiveDate": piDateLogic(data,'accrualProfileEffDate'),
                            "fullTimeEquivalencies": [{
                                "effectiveDate": piDateLogic(data,'fteEffDate'), "expirationDate": "3000-01-01",
                                "employeeStandardHours": data.employeeHours,
                                "fullTimeStandardHours": data.fullTimeHours,
                                "fullTimePercentage": parseInt(data.fullTimePct, 10)
                            }]
                        },
                        "personLicenseTypes": LicenseMap,
                        "userAccountStatusList": [{
                            "effectiveDate": piDateLogic(data,'userStatusEffDate'),
                            "userAccountStatusName": data.userStatus
                        }],
                        "employmentStatusList": [{
                            "effectiveDate": piDateLogic(data,'empStatusEffDate'), "expirationDate": "3000-01-01",
                            "employmentStatusName": data.empStatus

                        }],
                        "badgeAssignments": [{
                            "badgeNumber": data.badgeNumber,
                            "effectiveDate": ()=>{ if (data.badgeNumberEffDate) return data.badgeNumberEffDate; else return new Date().toISOString().split('T')[0]}
                        }],
                        "analyticsLaborTypesList": [
                            {
                                "analyticsLaborTypeName": data.analyticsLaborTypeName,
                                "effectiveDate": piDateLogic(data,'analyticsLaborTypeEffDate'), "expirationDate": "3000-01-01"
                            }
                        ],
                        "workEmployee": {
                            "defaultActivityName": data.defaultActivityName,
                            "idleActivityName": data.idleActivityName,
                            "paidActivityName": data.paidActivityName,
                            "profileName": data.workProfileName,
                            "unpaidActivityName": data.unpaidActivityName
                        }
                    },

                    "user": {
                        "userAccount": {
                            "logonProfileName": data.logonProfileName,
                            "userName": data.userName,
                            "userPassword": data.userPassword,
                            "mfaRequired": data.mfaRequired
                        },
                        "userCurrency": { "currencyCode": data.currencyCode }
                    }
                }]