findExisting: 
            {
                setDataSource:(da,ds)=>{
                    empList = []
                    $(da).each(function(k,v){empList.push(v.personnum)})
    
                    try {
                        ds.employees.qualifiers = empList
                    }
                    catch(e){}
                    return ds
                },
                datasource: {
                    type:'empData',
                    apiUrl: 'v1/commons/persons/extensions/multi_read/',
                    pdata: {
                        "where": {
                            "employees": {
                                "key": "personnumber",
                                "values": []
                            },
                            "snapshotDate": new Date().toISOString().split('T')[0]
                        }
                    },
                    apiBatchOn: 'where.employees.values',
                    apiBatchQty: 2500
                },
                updateExists: function(o,existRes,calls,apiType) { 
                    if (apiType == 'leaveCases') {
                        extMatch = false
                        $(existRes).each(function(k,res){
                            $(calls).each((k,call)=>{
                                if (res.id == call.tdata.extid) {
                                    extMatch = true
                                    call.api.leaveCase.id = res.id
                                }
                                else if (
                                    extMatch == false && 
                                    res.employee.qualifier == call.tdata.pnum &&
                                    res.startDate == dateAdjust(call.tdata.sdt) &&
                                    res.caseCode == call.tdata.case_code
                                ) call.api.leaveCase.id = res.id
                            })
                        })
                    }
                    //console.log('o',o)
                    //console.log('existRes',existRes)
                    //console.log('calls',calls)
                    //console.log('apiType',apiType)
                }
            },