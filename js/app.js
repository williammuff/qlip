//Node Elements
var gui = require('nw.gui');
var fs = require('fs-extra');
var os = require('os');
var path = require( 'path' );
var exec = require('child_process').exec;
const zlib = require('zlib');
var AdmZip = require("adm-zip");
var https = require('https');
var rimraf = require('rimraf')
var BBPromise = require("bluebird");
var ProgressPromise = require('progress-promise');
var sanitizeFilename = require("sanitize-filename");

//Global Vars
var conn_path = 'connections.JSON'
var pswd_path = 'misc/password'
var hist_path = 'history'
var lTableMaster = [];
let hot;
var wfc_url;
var wfc_uname;
var wfc_passw;
var wfc_int_name;
var access_data = {appkey:'',token:''}
var gl_table_edited;
var cwd = process.cwd();
var allowAPICalls = true
let iwfdAPI = new wfdAPI()

var nwin = gui.Window.get();
//nwin.maximize();
var autoConnectionTestMode
//autoConnectionTestMode = { connectionName:'Gil C70', uploadType: 'Person Assignments - Export'}
//autoConnectionTestMode = { connectionName:'McDonalds', uploadType: 'POS - Import' }

$(document).ready(function() {

    // //TESTING OF EXPORT (OFFLINE CODE)
    /*
    et = QLIPTypes.exports[0]
    et.type_name = et.type_name + ' - Export'
    so = et
    QLIPTypes.exportRender(et)
    $('#exportModal').modal()
    */
    //=========================

    //AUTO UPDATE
    startChecks(10); 

    //POPULATE CONNECTIONS
    populateConnections()
    progressCleanup()

    //GOT AUTO TEST MODE WORKING (autoConnectionTestMode)
    if (autoConnectionTestMode)
    {
        var selectize = $('#connectionsSelect').get(0).selectize;
        selectize.setValue(autoConnectionTestMode.connectionName)
        conn = connectionSelectizeDropDownClose()

        setTimeout(()=>{
            if (conn.hasOwnProperty('username')) $('#detailedLogin').click();
            $('#wfd_connect').click()
        },300)
    }

    //KEYBOARD SHORTCUTS
    $(document).on("keydown", function (e) { 
        var charCode = (e.which) ? e.which : event.keyCode, keyP;     
        tag = e.target.tagName.toLowerCase();
        
        if( event.which === 67  && event.altKey) { //ALT + C
            if ($('.modal.in').length == 0) {
                so = $('#connectionsSelect').selectize()[0].selectize;
                if (so) so.open()
            }
        }

        //F5
        if( event.which === 116) { location.reload(); }

        if( event.which === 90  && event.altKey) { //ALT + Z
            if ($('.tables').css('visibility') != 'hidden' && $('.modal.in').length == 0  ){
                    so = $('#uploadSelection').selectize()[0].selectize;
                    if (so) so.open()
                }
        }

        if( event.which === 68  && event.altKey) { //ALT + A
            if ($('.tables').css('visibility') != 'hidden' && $('.modal.in').length == 0  ){
                $('#importToAPI').click()
            }
        }
    })

    //Nprogress init/ajax link
    NProgress.configure({ showSpinner: false });
	$(document).ajaxStart(function() { NProgress.start(); });
	$(document).ajaxStop(function() { NProgress.done(); });
    $(document).ajaxError(function() { NProgress.done(); });

    //===================================================================================================
    //      CLICK EVENTS
    //===================================================================================================
    $('#about').click(function(){
        cur_package = getPackageDetails(cwd + '\\package.json')
        $('#about_version').html(cur_package['version'])
        $.get('https://kgintegration.bitbucket.io/QLIP/help.html?' +  new Date().getTime(),(r)=>{
            $('#aboutLinks').html(r)
        })
        $('#aboutModal').modal()
    })
    
    $('#copyAllData').click(function(){
        if (hot) {
            oData = ''
            dtArr = hot.getData()
            dtArr.unshift(hot.getColHeader())
            dtArr.pop()
            oData = toCSV(dtArr,'\t')
            copyToClipboard(oData)
        }
    })

    $('#clearAllData').click(function(){
        $.confirm({
            title: 'Confirm',
            content: 'Are you sure you want to clear the data table?',
            type: 'red',
            confirmButton: 'Yes',
            useBootstrap:false,
            boxWidth:'50%',
            backgroundDismiss: true,
            buttons: {
                yes: {
                    text: 'Yes',
                    btnClass: 'btn-dark',
                    keys: ['enter'],
                    action: function(){ hot.updateSettings({data : []}); }
                },
                cancel: {}
            }
        });  
    })

    $('#about_link').click(function(){
        gui.Shell.openExternal('https://paragon.kronos.com/Unity/Downloads/DownloadFile?filename=Paragon%20-%20Deployment%20Tools%20and%20Technology%20Guide.pdf');
    })

    
    $(document).on("click", "#aboutLinks" , function(clk) {
        lnk = $(clk.target).attr('link')
        if (lnk != "") gui.Shell.openExternal(lnk);
    });

    $('#connectionHelp').click(function(){
        let helpContent = '<i>Please view in fullscreen to avoid any issues.</i>\n\n'
            helpContent += '<b>Alias</b>\n Give your connection a unique name. This is just for display and personal reference. \n\n'
            helpContent += '<b>Workforce Dimensions URL</b>\n This is the actual URL of your Dimensions environment in the following format (https://example.dev.mykronos.com). \n\n'
            helpContent += '<b>Tenant ID</b>\n This can be found in the SDM Menu inside Dimensions as the Target ID. \n\n'
            helpContent += '<b>Dimensions Username</b>\n Dimensions Username with Super Access. \n\n'
            helpContent += '<b>Dimensions Password</b>\n Password for the Username login. \n\n'
            helpContent += '<b>Client ID</b>\n Unique identifier for each Customer. Must be requested through Cloud Ops via Salesforce Case. \n\n'
            helpContent += '<b>Client Secret</b>\n Password related to the Client ID. Must be requested through Cloud Ops via Salesforce Case. \n\n'
            helpContent += '<b>App Key</b>\n Created via the Developer Portal in WFD. Administration -> Developer Portal -> My Apps \n\n'

        $.alert({
            useBootstrap: false,
            columnClass: 'medium',
            title: 'Connection Help',
            type: 'blue',
            content: helpContent
        })
    })

    $('#detailedLoginOptions').hide()

    $('#detailedLogin').click(function(){
        $('#detailedLoginOptions').toggle()
    })

    $('#showHistory').click(function(){
        showHistory()
    })
    
    $('#openHistory').click(function(){
        bl = getSelectizeOption('#backupListing')
        c_url = $('#wfd_url').val()
        c_url_folder = wfdUrlToFolder(c_url)
        var histObj = JSON.parse(fs.readFileSync(hist_path + '\\' + c_url_folder + '\\' + bl.name,'utf8'))
        $('#historyModal').modal('hide');
        loadHistory(histObj)
	})

    $('#wfd_connect').click(function(){
        //CLEANUP SUPPORT URL (might have been better to look for / past http:// but this works for now.)
        $('#wfd_url').val($('#wfd_url').val().replace('/?support=true',''))
        
        //SET CONNECTION VARS
        let alias = $('#wfd_alias').val()
        let url = $('#wfd_url').val()
        let username = $('#wfd_uname').val()
        let password = $('#wfd_passwd').val()
        let clientId = $('#wfd_clientId').val()
        let clientSecret = $('#wfd_clientSecret').val()
        let appKey = $('#wfd_appKey').val()

        access_data.appkey = appKey
        access_data.token = ''

        //VERIFY CONNECTION DATA WA ENTERED PROPERLY
        if (
            ($('#detailedLoginOptions').is(":visible") && (alias == '' || url == '' || clientId == '' || appKey == '' || username == '' || password == '' || clientSecret == ''))
            ||
            ($('#detailedLoginOptions').is(":visible") == false && (alias == '' || url == '' || clientId == '' || appKey == ''))
        )
        {
            $('#connection_error_message').css('color','red')
            $('#connection_error_message').html('Please ensure all fields are properly filled out and are not empty.')
            $("#connectionModal").effect("shake")
            $('#connection_error_message').fadeIn()
        }
        //FIELDS WERE GOOD (FORGE ROCK or DETAILED LOG IN)
        else {
            $('#connection_error_message').hide() //hide the error if it was shown

            //SAVE CONNECTION
            addUpdateConnection(
            {
                name: $('#wfd_alias').val(),
                url: $('#wfd_url').val(), //ur
                //tenantId: $('#wfd_tenantId').val(),
                username: $('#wfd_uname').val(),
                password: $('#wfd_passwd').val(), //passwd/ps
                clientId: $('#wfd_clientId').val(),
                clientSecret: $('#wfd_clientSecret').val(),
                appKey: $('#wfd_appKey').val()
            })

            //GET ENVIRONMENT TYPE
            environmentType = QLIPTypes.findEnvironmentTypeByUrl(url)
            if (environmentType) $('#wfd_environmentType').val(environmentType.name)
        
            //DETAILED LOGIN
            if ($('#detailedLoginOptions').is(":visible"))  {
                
                iwfdAPI.getOAuthToken(access_data.token)
                    .then((scope) => { 
                        if (scope.errorCode != null) {
                            $.alert({
                                useBootstrap: false,
                                title: 'Authentication Error',
                                type: scope.errorColor,
                                content: "<b>Authentication error. Please check your credentials.</b>\n\n" + scope.errorCode + "\n" + scope.errorMessage
                            })
                        } 
                        else {
                            showQLIPTypes()
                            if (autoConnectionTestMode) {
                                setTimeout(()=>{
                                    var selectizeUT = $('#uploadSelection').get(0).selectize;
                                    selectizeUT.setValue(autoConnectionTestMode.uploadType)
                                },300)
                               
                            }
                        }
                    })
                    .catch((err)=>{
                        $.confirm({
                            title: 'Connection Error',
                            type: 'red',
                            useBootstrap:false,
                            boxWidth:'50%',
                            content: 'There was a problem establishing a connection to the tenant. <br>Error (' + err.errorMessage + ').',
                            buttons: {
                                close: {
                                    text: 'Yes',
                                    keys: ['enter'],
                                }
                            }
                          });
                    })  
            }
            
            //FORGEROCK
            else {
            //MAKE SURE YOU SET THE FRAME SOURCE so that the LOCK redirect thing doesn't happen.
            $('#iframeModal').find('iframe').attr('src',url)
            $('#iframeModal').find('iframe').attr('src',url + '?support=true')
            $('#iframeModal').modal()
            }
        }
    })

    $('#progShowAll').click(function(){
        errorHandsonUpdate(true)
        progressCleanup()
    })

    $('#progShowErrors').click(function(){
        errorHandsonUpdate(false)
        progressCleanup()
    })

    $('#progShowDismiss').click(function(){
        errorHandsonUpdate(true)
        progressCleanup()
    })

    $('#progressModal').on('hidden.bs.modal', function () {
        progressCleanup()
    })

    $('#progExit').click(function(){
        progressCleanup()
        allowAPICalls = false
        //TODO: CANCEL ACTUAL API CALLS/PROMISES
    })

	$('#downloadCSV').click(function(){
        let data = hot.getData()
        let cleanData = []
        let csv

        //get data from table and remove all empty rows
        data.forEach(function(r, i) {
            if(!hot.isEmptyRow(i)) {
                cleanData.push(r)
            }
        })
       
        //add header row to file
        cleanData.unshift(hot.getColHeader())
        
        //convert array to csv and export
        csv = toCSV(cleanData)
        filename = $('#connectionsSelect').selectize()[0].selectize.getValue() + ' ^ ' + $('#uploadSelection').selectize()[0].selectize.getValue() + ' ^ ' + new Date().YYYYMMDDHHMMSS()
        $('#export_file').attr('nwsaveas',filename)
		saveFile('#export_file', csv)
    })

    $('#importToAPI').click(async function(){
        showLoadingModal('Building request data for WFD.')
        setTimeout(()=>{
            if (hot) {            
                var limitError
                ut = QLIPTypes.findIt(hot['uploadType'])
                ut.skip = false
                ad = ut.toJSON(ut)
    
                //LIMIT LOGIC
                if (ut.hasOwnProperty('limitCheck')) {
                    limitError = ut.limitCheck(hot.getData())
                }
                else if (ut.hasOwnProperty('limit')) {
                    if (hot.countRows() - 1 > ut.limit) {
                        limitError = "You are attempting to import too many records (Limit = " + ut.limit +")"
                    }
                }
    
                if (hot.countRows() - 1 == 0) limitError = "You have not entered any data to import."
                
                //BEGIN MAIN IMPORT
                if (limitError){
                    $.notifyBar({ cssClass: "error",delay:10000,close:true,waitingForClose:false,position:"bottom",html: limitError})
                    showLoadingModal()
                }		   
                else 
                {
                    environmentType = QLIPTypes.getEnvironmentTypeByName($('#wfd_environmentType').val())
                    if (environmentType) {
                        if (environmentType.enableImportWarning) {
                            $.confirm({
                                title: 'Confirm',
                                content: 'Are you sure you want to import data to this ' + environmentType.name + ' environment?',
                                type: 'red',
                                confirmButton: 'Yes',
                                useBootstrap:false,
                                boxWidth:'50%',
                                backgroundDismiss: true,
                                buttons: {
                                    yes: {
                                        text: 'Yes',
                                        btnClass: 'btn-dark',
                                        keys: ['enter'],
                                        action: function(){
                                            importToAPI(ut,ad)
                                        }
                                    },
                                    cancel: {}
                                }
                            });   
                        }
                    }
                    else importToAPI(ut,ad)
                }   
            } 
        },10) //SO MODAL WILL SHOW
    })

    async function importToAPI(ut,ad) {
        showLoadingModal()
        if (ut.removeExisting) {
            showLoadingModal('Gathering data from Dimensions.')
            curRes = await iwfdAPI.doExistingCalls(ut)
            QLIPTypes.removeIfExists(ut,curRes,ad)
        }
        if (ut.findExisting)            {
            showLoadingModal('Gathering data from Dimensions.')
            curRes = await iwfdAPI.doExistingCalls(ut)
            QLIPTypes.updateIfExists(ut,curRes,ad)
        }
        
       //MAIN IMPORT
       showLoadingModal()

       $('.progress').addClass('active');
       $('#progressModal').modal({
           backdrop: 'static',
           keyboard: true
       });
       
       $('#progressModal').modal('show');
       
        responses = await iwfdAPI.doAPICalls(ad,ut)
        allowAPICalls = true
        hot.promiseResponses = responses
        histSave(responses)
        console.log('Post Import (app->importToAPI)',responses)
    }

    $('#exportFromAPI').click(async ()=>{
        //GET EXPORT TYPE
        so = $('#uploadSelection').selectize()[0].selectize;
        so = so.options[so.items]

        //PERFORM SOME VALIDATION HERE BASED ON SELECTIZE VALUE ("EXPORT TYPE/promptValidation"))

        //GET FORM DATA
        fdata = getExportPrompts()
        errorMsg = ""
        if (so.promptValidation) errorMsg = so.promptValidation(fdata,so)
      
        //IF FORM IS GOOD
        if (!errorMsg)
        {
            $('#exportPrompt_error_message').hide()
        
            res = await iwfdAPI.doExportAPICalls(fdata,so)
            tdata = so.requestToOutput(res,fdata)
            QLIPTypes.postExportRenderTableData(tdata)
            histSave(tdata)
        }
        else $('#exportPrompt_error_message').show().html(errorMsg)       
    })
    
    //FORGEROCK LOGIN HANDLE/GET TOKEN
    $('#forgeRockLogin').on('load', function() {     
        $('#forgeRockLogin').show()
        iFrameURL = document.getElementById("forgeRockLogin").contentWindow.location.href
        console.log('frame onload',iFrameURL)
        if (iFrameURL.indexOf('?support=true') > -1) {
            console.log('login page')
            setTimeout(function(){
                let url = $('#wfd_url').val()
                let clientId = $('#wfd_clientId').val()
                let originUrl = window.frames[0].location.origin
                $('#forgeRockLogin').attr('src',originUrl + '/accessToken?clientId=' + clientId)
            },500)
        }

        if (iFrameURL.indexOf('/accessToken?clientId=') > -1) {
            $('#forgeRockLogin').hide()
            if ($('#forgeRockLogin').contents().find("html").html().indexOf('Invalid Request') > -1)
            {
                $('#iframeModal').modal('hide')
                $.confirm({
                    title: 'Confirm',
                    content: 'Login was unsuccessful, please validate your clientID',
                    type: 'red',
                    confirmButton: 'Yes',
                    useBootstrap:false,
                    boxWidth:'50%',
                    backgroundDismiss: true,
                    buttons: {
                        cancel: {
                            text: 'Ok'
                        }
                    }
                });  
            }
            else 
            {
                access_data.token = JSON.parse($('#forgeRockLogin').contents().find("html").find('pre').html()).accessToken
                console.log('got token',access_data.token)
                $('#iframeModal').modal('hide')
                showQLIPTypes()
            }
        }
    });

	$('#loadCSV').click(function(){
        chooseFile('#fileDialog')
	})

    $('#connectionForm').find('input').keypress(function(e){
        if (e.which == 13) $('#wfd_connect').click()
    })

     $('#add_connection_button').click(function(){
        addWFDConnection()
    })

    $('#connectionModal').on('shown.bs.modal', function () {
        if ($('#wfd_alias').val() != '') $('#wfd_passwd').focus();
        else $('#wfd_alias').focus();    
    })

    $('#deleteConnection').click(function(){
        $.confirm({
            title: 'Confirm',
            content: 'Are you sure you want to delete this connection',
            type: 'red',
            confirmButton: 'Yes',
            useBootstrap:false,
            boxWidth:'50%',
            backgroundDismiss: true,
            buttons: {
                yes: {
                    text: 'Yes',
                    btnClass: 'btn-dark',
                    keys: ['enter'],
                    action: function(){
                        so = getSelectizeOption('#connectionsSelect')
                        if (so) 
                        {
                            addUpdateConnection(so,true)
                            $("#connectionModal").modal("hide");
                        }
                    }
                },
                cancel: {}
            }
        });      
    })

    $('#showTypeDescription').click(()=>{
        ty = getSelectizeOption('#uploadSelection')
        if (ty) {
            if (ty.descHTML) {
                $('#typeDescriptionModal').find('.modal-title').html(ty.type_name)
                //console.log(ty.descHTML)
                $('#typeDescriptionModal').find('.modal-body').html(ty.descHTML)
                $('#typeDescriptionModal').modal('show');
            }
        }
    })

    //====================================================================
    //      HELPERS
    //====================================================================
    function progressCleanup() {
        $("[id^='progShow']").hide()
        $("[id^='progCount']").html('0')
        $('#progExit').show()
        $("[id^='progBar']").width(0)
        $('.progress').removeClass('active');
        $('#progressModal').modal('hide');
        $('#progErrorCount').text('')
    }

    function saveFile(name, data) {
        var chooser = document.querySelector(name);
        chooser.addEventListener("change", function(evt) {
            //console.log(this.value); // get your file name
            fs.writeFileSync(this.value + ".csv", data, function(err) {
                if(err) {
                    alert("error"+err);
                }
            });
        }, false);
        
        chooser.click();  
    }

    function chooseFile(name) {
        var chooser = $(name);
        chooser.unbind('change');
        chooser.change(function(evt) {
            showLoadingModal('Reading from source file.')
            fname = this.value
            if (fname) {
                //get data from selected file and convert into an Array of Arrays for HOT
                let data = fs.readFileSync(fname,'utf8')
                let dt_arr = CSVToArray(data,",")

                var adj = 0
                adj = 1

                if (hot.getColHeader()[0] == 'Import Status') {
                    $(dt_arr).each(function(k,v){
                        dt_arr[k].unshift('')
                    })
                }

                //console.log(adj,hot.countCols() - adj,dt_arr[0].length)


                //TODO: check if headers match, then remove it
                dt_arr.shift()
                var errorMessage = ''
                if (hot){
                    //FILE HAS DATA
                    if (dt_arr.length > 0){
                        
                        //COLUMN NUMBERS MATCH
                        if (hot.countCols() == dt_arr[0].length) {
                            //load the data into the table and render it

                            dt_arr.forEach(function(element){
                                if (element.length == dt_arr[0].length){
                                }
                                else{
                                    element.pop();
                                }
                            });
                                               
                            //FIX FOR HOT (documented online)
                            setTimeout(function() {
                                hot.loadData(dt_arr)
                                showLoadingModal()
                                //hot.loadData(dt_arr)
                                //hot.render() 
                                }, 300) 
                        }
                        //COLUMNS DON"T MATCH
                        else 
                        {
                            showLoadingModal()
                            $.alert({
                                useBootstrap: false,
                                columnClass: 'medium',
                                title: 'Mismatch Columns',
                                type: 'blue',
                                content: "Imported columns are greater than destination columns. Please Verify Data."
                            })                                
                        }							  
                    }
                    //NO DATA IN FILE
                    else 
                    {
                        $.alert({
                            useBootstrap: false,
                            title: 'File Load Error',
                            type: 'red',
                            content: 'There is no data in the file that was selected.'
                        })
                    }
                }
            }
            $(name).val('')
        });
        chooser.trigger('click');  
    }

    function findLargestArray(objs,key) {
        largest = 0
        objs.forEach(function(v,k){
            if (v) {
                if (Array.isArray(v[key])) {
                    cur = v[key].length
                    if (cur > largest) largest = cur
                }
            }
        })
        return largest
    }
});

