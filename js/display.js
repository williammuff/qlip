function addWFDConnection() {
    //TODO-connections?
    $('#wfd_alias').val('')
    $('#wfd_url').val('')
    //$('#wfd_tenantId').val('')
    $('#wfd_uname').val('')
    //$('#wfd_passwd').val('')
    $('#wfd_clientId').val('')
    $('#wfd_clientSecret').val('')
    $('#wfd_appKey').val('')
    $('#connection_error_message').css('display','none')
    $('#connectionModal').modal()
}

function showLoadingModal(msg) {
    if (msg) {
        $('#content').css('opacity', '1');
        $('#content').loadingModal('destroy');
        $('#content').loadingModal({text: msg});
        $('#content').loadingModal('show')
    }
    else $('#content').loadingModal('hide');
}

function showQLIPTypes(){
    //hide the connection modal and show the table
    $("#connectionModal").modal("hide");
    $('#uploadSelectionBox').css('visibility','visible')
    $('body').css("background-image", "url('')");

    //DISPLAY DROPDOWN SLEECTION FOR UPLOAD TYPE
    renderQLIPTypes()

    //gather connection values and update Connections
    ur = $('#wfd_url').val()
    ps = b64EncodeUnicode($('#wfd_passwd').val())
}

var preUTVal;
function renderQLIPTypes()
{
    var selectize = $('#uploadSelection').get(0).selectize;
    if (selectize) selectize.destroy()
    //$('.tables').css('visibility','visible')

    var ut_imports = QLIPTypes.imports
    ut_imports.forEach((i)=>{ 
        if (i.type_name.indexOf('Delete') > -1) i.type_name = i.type_name
        else i.type_name = i.type_name.replace(' - Import','') + ' - Import'
        i.type = 'Import'
        //if (i.type_name.indexOf(' - ' + i.type) == -1) i.type_name = i.type_name + ' - ' + i.type;
    })
    var ut_exports = QLIPTypes.exports
    ut_exports.forEach((e)=>{ 
        e.type_name = e.type_name.replace(' - Export','') + ' - Export'
        e.type = 'Export'
        //if (e.type_name.indexOf(' - ' + e.type) == -1) e.type_name = e.type_name + ' - ' + e.type
    })
    
    options = []
    ut_imports.forEach((i)=>{options.push(i)})
    ut_exports.forEach((e)=>{options.push(e)})
    options = options.filter((o)=>{
        if (o.hasOwnProperty('visible')) {
            if (!o.vislble) return false
            else return true
        }
        return true
    })

    environmentType = $('#wfd_environmentType').val()
    if (environmentType) environmentType = QLIPTypes.getEnvironmentTypeByName(environmentType)
    if (environmentType) {
        options.forEach((o,k)=>{
            if (o.excludeEnvironmentTypes) {
                if (o.excludeEnvironmentTypes.indexOf(environmentType.name) > -1) o.skip = true
            }
        })
    }

    options = options.filter(o => o.skip != true)

    options = options.sort(function(a, b) {
        var textA = a.type_name.toUpperCase();
        var textB = b.type_name.toUpperCase();
        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
    /*
    options = options.sort((a,b) => {
        if (a.type_name < b.type_name) return -1
        if (a.type_name > b.type_name) return 1
        return 0
    })
    */
    
    $('#uploadSelection').selectize({
        persist: false,
        //openOnLoad: true,
        maxItems: 1,
        //allowEmptyOption:false,
        valueField: 'type_name',
        labelField: 'type_name',
        searchField: ['type_name'],
        options:options,
        render: {
            item: function(item, escape) {
                return '<div>' +
                    '<span class="name">' + escape(item.type_name) + '</span>' +
                '</div>';
            },
            option: function(item, escape) {
                return '<div>' +
                    '<span class="label">' + escape(item.type_name) + '</span>' +
                '</div>';
            }
        },
        onInitialize : function(){
                this['currentValue'] = this.getValue()
            },
        onChange: function (value) {},
    
        onDropdownOpen: (value)=>{
            so = getSelectizeOption('#uploadSelection')
            if (so) preUTVal = so.type_name
        },
        onDropdownClose: function (value,b,c) {
            sl = this
            $('.otherNav').css('visibility','visible')
            value = sl.getValue()

            if (value) {
                if (sl.getValue().indexOf(' - Export') > -1) {
                    QLIPTypes.onSelectionChange(QLIPTypes.findIt(value))
                    $('#importToAPI').hide()
                    $('#loadCSV').hide()
                }
                else if (gl_table_edited)
                {
                   disregardChanges(function(didConfirm) {
                        gl_table_edited = false;
                        
                        if (didConfirm) {
                            selectize['currentValue'] = sl.getValue()
                            QLIPTypes.onSelectionChange(QLIPTypes.findIt(value))
                        }
                        else {
                            sl.setValue(selectize['currentValue'])
                        }
                   })
                }
                else if (hot)
                {
                    selectize['currentValue'] = sl.getValue()
                    if (value != hot['uploadType']) QLIPTypes.onSelectionChange(QLIPTypes.findIt(value))
                }
                else {
                    selectize['currentValue'] = sl.getValue()
                    QLIPTypes.onSelectionChange(QLIPTypes.findIt(value))
                }
    
                //ADD BUTTONS BACK
                if (sl.getValue().indexOf(' - Export') == -1) {
                    $('#importToAPI').show()
                    $('#loadCSV').show()
                }
            }
            else if (preUTVal) {
                var utselectize = $('#uploadSelection').get(0).selectize;
                utselectize.setValue(preUTVal)
                console.log('nothing was selected',preUTVal)
            }
        }
        
    });
    var selectize = $('#uploadSelection').get(0).selectize;
    selectize.focus();   
}

function errorCodeRenderer(instance, td, row, col, prop, value, cellProperties) {

    Handsontable.renderers.TextRenderer.apply(this, arguments);
    //console.log(row,value,cellProperties)
    if (value) {
        if (value.indexOf('Successfully Imported') > -1 || value.indexOf('Allowable Error') > -1) {
            //console.log('renderer',hot.promiseResponses.find(x => x.rnum === row))
            td.style.background = '#99ffdd'
            if (value.indexOf('Successfully Imported') > -1) td.innerHTML = value.replace('|', '')
        }
        else if (value.indexOf('Already Exists') > -1) {
            td.style.background = '#6fbbd3'
        }
        else {
            td.style.background = '#fdb9b9'
            eData = ["", ""]
            if (value) eData = value.split('|')
            td.innerHTML = '<div title="' + eData[1] + '">' + eData[0] + '</div>';
        }
    }
}

function errorHandsonUpdate(keepSuccessRows) {
    
    var promiseResponses = hot.promiseResponses
    var columns = hot.getSettings().columns;
    var colHeaders = hot.getSettings().colHeaders;
    var dater = hot.getData()

    hasErrorColumn = true

    if (colHeaders[0] !== 'Import Status') {
        hasErrorColumn = false
        colHeaders.unshift('Import Status')
        //columns.unshift({label: "Error Message", type: "autocomplete", source:['Successfully Imported'], strict: true})
        columns.unshift({label: "Import Status",renderer:errorCodeRenderer})
    }

    var finalDater = []
    $(dater).each(function(k,v){
        if (!v.every(x => x === null)) { //CHECK ROW FOR BLANK
            row = v
            //console.log(row,promiseResponses)
            eMessage = 'Successfully Imported|'

            //var indResp = promiseResponses.find(x => x.rnum === k)
            
            var indResp = findRowInResponses(k,promiseResponses)
    
            if (indResp)
            {
                if (indResp.error) eMessage = indResp.error.errorCode + '|' + indResp.error.message
                if (indResp.__exists) eMessage = 'Record Already Exists in WFD'
                if (indResp.allow_error) eMessage = 'Allowable Error' + '|' + indResp.error.message 
                
                if (hasErrorColumn){
                    v.shift() //IF ERROR COL PRESENT REMOVE FIRST COLUMN (as we will be adding back)
                }
                row.unshift(eMessage)
                if (keepSuccessRows) finalDater.push(row) 
                else if (eMessage.indexOf('Successfully Imported') == -1 && eMessage.indexOf('Already Exists') == -1) finalDater.push(row) 
            }
        }
    })
    
    //console.log(finalDater)
    hot.updateSettings(
      {
        colHeaders: colHeaders,
        columns: columns,
        data : finalDater
      }
    )
    hot.validateCells()
}

function findRowInResponses(rnum,responses,check_type){
    if (!check_type) check_type = ""
    var retVal = ""
    responses.forEach((r)=>{
        if (retVal == "") {
            r.final.forEach((f)=>{
                if (f.check_type == 'skipped') {
                    f.skipped.forEach((t)=>{
                        if (t.__rnum == rnum && retVal == "") retVal = t
                    })
                }
                else {
                    f.tdata.forEach((t)=>{
                        if (t.__rnum == rnum && retVal == "") retVal = t
                    })
                }  
            })
            
            if (r.exists) {   
                r.exists.forEach((t)=>{
                    t.__exists = true
                    if (t.__rnum == rnum && retVal == "") retVal = t
                })
            }
        }
    })

    if (retVal.__ignore && !check_type) {
        retVal = findRowInResponses(rnum,responses,'skipped')
        if (retVal.__from_rnum || String(retVal.__from_rnum) == "0") { //ZERO GETS YOU EVERYTIME
            retVal = findRowInResponses(retVal.__from_rnum,responses)
        }
    } 
    
    return retVal
}

function disregardChanges(cb)
{
     $.confirm({
                title: 'Disregard Changes',
                type: 'red',
                content: 'You have unsaved changes that will be lost. Are you sure?',
                backgroundDismiss: true,
                useBootstrap:false,
                boxWidth:'50%',
                //scrollToPreviousElement:false,
                buttons: {
                        yes: {
                            text: 'Yes',
                            btnClass: 'btn-dark',
                            keys: ['enter'],
                            action: function(){
                                gl_table_edited = false;
                                cb(true)
                            }
                        },
                        cancel: function () {
                            gl_table_edited = false
                            cb(false)
                            //var selectize = $('#uploadSelection').get(0).selectize
                            //if (selectize) selectize.setValue(hot['uploadType'])
                            //gl_table_edited = true
                        }
                    }
    })
}

function changeConnectionVisuals(){
    $('#data_table').css('visibility','hidden')
    $('#connection_error_message').hide()
    $('.otherNav').css('visibility','hidden')
    $('.tables').css('visibility','hidden')
}

function showHistory()
{
    ut = getSelectizeOption('#uploadSelection')
    if (ut) 
    {
        historyTitle = ut.type_name + ' - Backups'
        $('#backupLookupTableTitle').html(historyTitle)
        curl = $('#wfd_url').val()
        
        barr = getBackupHistory(ut.type_name)
            
        fbarr = []
        $(barr).each(function(k,v){
            tobj = {}
            tobj['name'] = v
            tobj['value'] = v
            fbarr.push(tobj)
        })

        var selectize = $('#backupListing').get(0).selectize;
        if (selectize) selectize.destroy()
        $('#backupListing').selectize({
            maxItems: 1,
            options: fbarr,
            valueField: 'name',
            labelField: 'name',
            searchField: 'name',
            render: {
                item: function(item, escape) {
                    return backupFileToPrettyHTML(item.name)
                },
                option: function(item, escape) {
                    return backupFileToPrettyHTML(item.name)
                }
            }
        })
        $('#historyModal').modal()
    }
}

function wfdUrlToFolder(wurl) {
    //matches = wurl.match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i);
    //dm = matches && matches[1]
    dm = wurl.split('\/')
    dm = dm[2]
    return path.normalize(dm)
}

function historyBuildJSON(responses,ut) {
    resArr = []
    cdata = []
    if (responses.hasOwnProperty('data')) {
        responses.headers.forEach((h)=>{cdata.push({data:h,name:h,visible:true})})
        responses.data.forEach((d,k)=>{resArr.push(d)})
    }
    else {
        cdata = ut.cdata
        responses[0].final.forEach((r)=>{
            r.tdata.forEach((t)=>{
                resArr.push(t)
            })
        })
    }
    
    return {cdata:cdata,tdata:resArr}
}

function histSave(responses) {
    ut = getSelectizeOption('#uploadSelection')
    if (hot && ut) {
        //jdata = {'cdata':ut.cdata,'tdata':historyBuildJSON(responses)}
        jdata = historyBuildJSON(responses,ut)

        url = $('#wfd_url').val()
        out_folder = driveHistFolder(url)
        out_fname = driveHistFname(ut.type_name)

        if (out_fname)
        {
            fs.writeFileSync(out_folder + '\\' + out_fname, JSON.stringify(jdata), function(err) {
                if(err) {
                    alert("Failed To Output History File");
                }
            });
            purgeHistory(url)
            $.notifyBar({ cssClass: "",delay:1000,close:true,waitingForClose:false,position:"bottom",html: "A backup was taken" });
        }
    }
}

function driveHistFolder(url)
{
    ofold = hist_path + '//' + wfdUrlToFolder(url)
    if (!fs.existsSync(ofold)) fs.mkdirSync(ofold)
    return ofold
}

function driveHistFname(type)
{
    var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
    var ds = (new Date(Date.now() - tzoffset)).toISOString().replace(/[^0-9]/g, "");
    ds = ds.substring(0,4) + '_' + ds.substring(4,6) + '_' + ds.substring(6,8) + '_' + ds.substring(8,10) + '_' + ds.substring(10,12) + '_' + ds.substring(12,14)
    
    hfile = ds + '__' + type + '.json'
    return hfile
}

function purgeHistory(wurl)
{
    barr = getBackupHistory('')
    var d = new Date();
    var daysBack = 14
    d.setDate(d.getDate() - daysBack)
    d.setHours(0)
    d.setMinutes(0)
    d.setSeconds(0)
    c_url = wfdUrlToFolder(wurl)
    $(barr).each(function(k,v){
        raw_dt = v.substring(0,10).replace('_','-').replace('_','-')
        var fd = new Date(raw_dt)
        fd.setHours(0)
        fd.setMinutes(0)
        fd.setSeconds(0)
        if (fd < d) fs.unlinkSync(hist_path + '\\' + c_url + '\\' + v)
    })		
}

function getBackupHistory(type)
{
    $('#backupListing').html('')
    bkp_arr = []
    c_url = $('#wfd_url').val()
    url_hist_fold = wfdUrlToFolder(c_url)
    full_hist_path = hist_path + '//' + url_hist_fold

    if (fs.existsSync(full_hist_path)) {
        files = fs.readdirSync(full_hist_path)

        files.forEach(function(f) {
            if(f.indexOf("__" + type) > -1 || type == "") bkp_arr.push(f)
        });
    }

    bkp_arr = bkp_arr.sort()
    bkp_arr = bkp_arr.reverse()
    return bkp_arr
}

function backupFileToPrettyHTML(fname)
{
    p = fname.split('__')
    d = p[0].split('_')
    pretty_date = d[1] + '/' + d[2] + '/' + d[0]
    pretty_time = d[3] + ':' + d[4] + ':' + d[5]
    
    
    jdtm = new Date(d[0],d[1] - 1,d[2],d[3],d[4],d[5])
    cdtm = new Date()

    sresult = cdtm - jdtm
    var how_long_ago;
    dDays = Math.floor(sresult / (1000*60*60*24))
    dHrs = Math.floor(sresult / (1000*60*60))
    dMins = Math.floor(sresult / (1000*60))
    dSecs = Math.floor(sresult / (1000))

    if (dSecs >= 1) how_long_ago = dSecs + ' Seconds Ago'
    if (dMins >= 1) how_long_ago = dMins + ' Minutes Ago'
    if (dHrs >= 1) how_long_ago = dHrs + ' Hours Ago'
    if (dDays >= 1) how_long_ago = dDays + ' Days Ago'

    //list_name = p[1].replace('DWLOAD','Download').replace("UPLOAD_BKP","Upload System Backup").replace('UPLOAD','Upload')
    return '<div>' + pretty_date + ' ' + pretty_time + ' (' + how_long_ago + ')' + '</div>';
}


function loadHistory(ho)
{
    cols = []
    output = []
    if (ho.tdata.length > 0) {
        if (ho.tdata[0].hasOwnProperty('__id')) {
            ho.tdata.forEach((t)=>{
                Object.keys(t).forEach((k)=>{
                    if (k.indexOf('__') > -1 && k != '__id') delete t[k]
                })
            })
            ho.cdata.push({ visible: true,data: 'Imported ID',name: '__id'})
        }
    }
    ho.cdata.forEach((c)=>{cols.push(c.name)})
    ho.tdata.forEach((t)=>{
        tmpArr = []
        Object.keys(t).forEach((k)=>{
            tmpArr.push(t[k])
        })
        /*
        cols.forEach((c)=>{
            if (t[c]) tmpArr.push(t[c])
            else tmpArr.push("")
        })
        */
        output.push(tmpArr)
    })

    QLIPTypes.importRender(ho,output)
    //dt_arr = CSVToArray(ldata,",")
    //mconfig = findConfigInTable(dt_arr)
    //dt_obj = objToDataTable(dt_arr)
    //buildHandsOnTable(dt_obj,mconfig)
    $.notifyBar({ cssClass: "",delay:3000,close:true,waitingForClose:false,position:"bottom",html: "Table Successfully Loaded Into Editor" });
}
