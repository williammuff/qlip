function getSelectizeOption(ident,all_flg)
{
    var selectize = $(ident).get(0).selectize
    var current = selectize.getValue()
    var options = selectize.options
    var option = selectize.options[current]
    if (all_flg) return options
    else return option
}

function limitCheck(v){
    var limit = 0
    if (v.hasOwnProperty('limit')) limit = v.limit
    return limit
}

function ifnull(o,k) {
    //console.log('ifnull',o,k,o.hasOwnProperty(k))
    if (o.hasOwnProperty(k)) {
        v = o[k]
        if (v === null) v = ''
        if (!(v)) v = ''
        return v
    }
    else return ''
}

function removeImagefromResponse(rs)
{
    //WHY JQUERY...WHY MUST YOU RENDER THE DOM OBJECT!!!! (just load it, don't snag it)
    rs = rs.replace(/<img[^>]*>/g,"");
    rs = rs.replace(/<svg[^>]*>/g,"");
    return rs
}

function batchArray(arr,qty)
  {
      finalArr = []
      tmp_arr = []
      $(arr).each(function(k,v){
          tmp_arr.push(v)
          if ((k+1)%qty == 0 || (k+1) == arr.length) {
              finalArr.push(tmp_arr)
              tmp_arr = []
          }
      })
      return finalArr
  }

function findConfigInTable(ld)
{
    var mco = {}
    {
        so = getSelectizeOption('#lookupTablesSelect')
        $(ld[0]).each(function(k,v){
            config_obj = getConfigParams(v,so.table_name)
            if(config_obj)
            {
                getConfigList(config_obj,v,function(r){
                    mco[k] = r
                })	
            }
        })
    }
    return mco
}

function getConfigParams(friendly_name,tname)
{
    var robj;
    
    //HARD CHECK (using tnameLike as well)
    $(WFC_API_JSON.root.Category).each(function(k,v){
        //RULE LEVEL
        $(v.Rule).each(function(kk,vv){
            //ENTRY LEVEL - VERIFY TNAMELIKE EXISTS
            if (typeof vv['_tnameLike'] != 'undefined') {
                //VALIDATE TNAME MATCH
                if (tname.indexOf(vv['_tnameLike']) > -1) {
                    //LOOP THROUGH FRIENDLY's FOR MATCH
                    $(vv._friendly).each(function(kkk,vvv){
                        if (vvv == friendly_name) robj = vv 
                    })
                }
            }
        })
    })

    //SECONDARY CHECK IF NO TABLE MATCH IS FOUND
    if (!(robj)) {
        $(WFC_API_JSON.root.Category).each(function(k,v){
            $(v.Rule).each(function(kk,vv){
                $(vv._friendly).each(function(kkk,vvv){
                    if (vvv == friendly_name) robj = vv 
                })
            })
        })
    }
    return robj;
}

function objToDataTable(lto)
{
    master = [];
    columns = [];
    data = [];
    if (lto)
    {
        $(lto[0]).each(function(k,v){
            if (v == "") v = " "
            columns.push({"title":v,"data":v})
        })
        
        $(lto).each(function(k,v){
            row = {}
            
            if (k > 0)
            {
                
                $(v).each(function(kk,vv){
                    if (kk + 1 <= columns.length) row[columns[kk].title] = vv
                })
                data.push(row)
            }
        })
        
        master = { "data" : data, "columns" : columns}
    }

    return master
}

function stringToDate(_date,_format,_delimiter)
{
            var formatLowerCase=_format.toLowerCase();
            var formatItems=formatLowerCase.split(_delimiter);
            var dateItems=_date.split(_delimiter);
            var monthIndex=formatItems.indexOf("mm");
            var dayIndex=formatItems.indexOf("dd");
            var yearIndex=formatItems.indexOf("yyyy");
            var month=parseInt(dateItems[monthIndex]);
            month-=1;
            var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
            return formatedDate;
}

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function arraytoCSV(csv_arr,delim)
{
    dl = ','
    if (delim) dl = delim
    master_csv = ''
    $(csv_arr).each(function(k,v){
        $(v).each(function(kk,vv){
            if (vv == null) vv = ""
            cell = vv.replace('"','""')
            if (cell.indexOf(dl) > -1) cell = '"' + cell + '"'
            master_csv = master_csv + cell
            if(!(v.length == kk + 1)) master_csv = master_csv + dl
        })
        if (!(csv_arr.length == k + 1)) master_csv = master_csv + '\n'
    })
    return master_csv
}

function CSVToArray(strData, strDelimiter)
{
    // Check to see if the delimiter is defined. If not,
    // then default to comma.
    strDelimiter = (strDelimiter || ",");
    // Create a regular expression to parse the CSV values.
    var objPattern = new RegExp((
        // Delimiters.
    "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
        // Quoted fields.
    "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
        // Standard fields.
    "([^\"\\" + strDelimiter + "\\r\\n]*))"), "gi");
    // Create an array to hold our data. Give the array
    // a default empty first row.
    var arrData = [[]];
    // Create an array to hold our individual pattern
    // matching groups.
    var arrMatches = null;
    // Keep looping over the regular expression matches
    // until we can no longer find a match.
    while (arrMatches = objPattern.exec(strData)) {
        // Get the delimiter that was found.
        var strMatchedDelimiter = arrMatches[1];
        // Check to see if the given delimiter has a length
        // (is not the start of string) and if it matches
        // field delimiter. If id does not, then we know
        // that this delimiter is a row delimiter.
        if (strMatchedDelimiter.length && (strMatchedDelimiter != strDelimiter)) {
            // Since we have reached a new row of data,
            // add an empty row to our data array.
            arrData.push([]);
        }
        // Now that we have our delimiter out of the way,
        // let's check to see which kind of value we
        // captured (quoted or unquoted).
        if (arrMatches[2]) {
            // We found a quoted value. When we capture
            // this value, unescape any double quotes.
            var strMatchedValue = arrMatches[2].replace(
                    new RegExp("\"\"", "g"), "\"");
        } else {
            // We found a non-quoted value.
            var strMatchedValue = arrMatches[3];
        }
        // Now that we have our value string, let's add
        // it to the data array.
        arrData[arrData.length - 1].push(strMatchedValue);
    }
    // Return the parsed data.
    return (arrData);
}

function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

function validateAndCleanURL(wfc_url)
{
    wfc_url_regex = new RegExp('(http|https):\/\/[^\/]+\/[A-Za-z0-1]*')
    slash_pos = getPosition(wfc_url,'/',4)
    if (slash_pos) wfc_url = wfc_url.substring(0,slash_pos)
    if (/(http|https):\/\/[^\/]+\/[A-Za-z0-1]+/.test(wfc_url)) return wfc_url
    else return ''
}

function getPosition(string, subString, index) {
   return string.split(subString, index).join(subString).length;
}

function compareArrays(array1, array2){
  return (array1.join('-') == array2.join('-'));
}

function checkKey(obj, level,  ...rest) {
    if (obj === undefined) return false
    if (rest.length == 0 && obj.hasOwnProperty(level)) return true
    return checkKey(obj[level], ...rest)
  }

function filterTables(ltd){
    flt_arr = getFilters()
    if (flt_arr.length > 0)
    {
        tmp_obj = []
        $(ltd).each(function(k,v){
            if (flt_arr.indexOf(v.table_name) > -1) tmp_obj.push(v)
        })
        ltd = tmp_obj
    }
    return ltd
}

function b64EncodeUnicode(str) {
    // first we use encodeURIComponent to get percent-encoded UTF-8,
    // then we convert the percent encodings into raw bytes which
    // can be fed into btoa.
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
        function toSolidBytes(match, p1) {
            return String.fromCharCode('0x' + p1);
    }));
}

function b64DecodeUnicode(str) {
    // Going backwards: from bytestream, to percent-encoding, to original string.
    return decodeURIComponent(atob(str).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
}

  function toCSV(array,delim) {
    if (!delim) delim = ','
    //return array.map(joinRow).join('\n')
    return array.map((row)=>{
        return row.map(escapeCell).join(delim)
    }).join('\n')
  }
  
  function joinRow(row,delim) {
    return row.map(escapeCell).join(delim)
  }
  
  function escapeCell(x) {
    return /,|"/.test(x)
      ? '"' + x + '"'
      : x
  }


Object.defineProperty(Date.prototype, 'YYYYMMDDHHMMSS', {
    value: function() {
        function pad2(n) {  // always returns a string
            return (n < 10 ? '0' : '') + n;
        }

        return this.getFullYear() +
               pad2(this.getMonth() + 1) + 
               pad2(this.getDate()) +
               pad2(this.getHours()) +
               pad2(this.getMinutes()) +
               pad2(this.getSeconds());
    }
});

function getDomain() {

    let url = $('#wfd_url').val()
    let cleanUrl = url.replace('https://','')
    return cleanUrl
}

function isFunction(thing) {
    var getType = {}
    return thing && 
           getType.toString.call(thing) === '[object Function]'
}

function _setDefaults(opt) {
    if(!opt) opt = {};
    if(!opt.hasOwnProperty('propertyHere')) { 
        //if it has the property already you can do things 
    }

    return opt
}

function dateAdjust(d) { 
    var rval = ''
    var match = (/([0-9]{1,2})\/([0-9]{1,2})\/(\d{4})/).exec(d);
    if (match) {
        if (match.length == 4) {
            mn = ('00' + match[1]).slice(-2)
            dy = ('00' + match[2]).slice(-2)
            yr = match[3] 
            rval = yr + '-' + mn + '-' + dy
        }
    }
    else rval = d
    return rval
}

function copyToClipboard(str) {
    const el = document.createElement('textarea');
    el.value = str;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
}

function getExportPrompts(){
    let tmpObj = {}
    $('#exportForm').find('input').each((k,v)=>{
        id = $(v).attr('id')
        if (id) {
            id = id.replace('export__','')
            value = $(v).val()
            text = $(v).text()
            tmpObj[id] = {}
            tmpObj[id].value = value
            tmpObj[id].text = text
        }
    })
    $('#exportForm').find('select').each((k,v)=>{
        id = $(v).attr('id')
        if (id) {
            id = id.replace('export__','')
            value = $(v).val()
            text = $(v).text()
            tmpObj[id] = {}
            tmpObj[id].value = value
            tmpObj[id].text = text
        }
    })
    return tmpObj
}



function arrayToObject(arr) {
   retObj = {}
    arr.forEach((v)=>{
        retObj[v.name] = v.value
   })
   return retObj
}

function right(str, chr)
{
  return str.slice(str.length-chr,str.length);
}

function left(String, Length) { if (String == null) return (false); return String.substr(0, Length); }

function stringToTime(strtm){
	strtm = strtm.replace(':','')
  strtm = '0000' + strtm
	strtm = right(strtm,4)
  strtm = strtm.substring(0,2) + ':' + strtm.substring(2,4)
  return strtm
}