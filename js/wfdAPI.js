
function wfdAPIgetParams() {
    let formArr = $('#connectionForm').serializeArray()
    let paramObj = {}
    
    $(formArr).each(function(k,v){ 
        paramObj[v['name']] = v['value'] 
    })
    return paramObj
}

function _getKronosDefaultXHRSettings(method, url)
{
    let xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    
    xhr.open(method, url);
    xhr.setRequestHeader("content-type", "application/json");
    xhr.setRequestHeader("cache-control", "no-cache");

    return xhr
}

progressInfo = {}

class wfdAPI {

    refreshToken() {
        return new Promise((resolve, reject) => {
            let password = $('#wfd_passwd').val()
            if (!password) {
                let url = $('#wfd_url').val()
                let clientId = $('#wfd_clientId').val()

                let tokenUrl = url + '/accessToken?clientId=' + clientId
                $.get(tokenUrl).then((tokenData)=>{
                    access_data.token = tokenData.accessToken
                    resolve(access_data)
                })
            }
            else this.getOAuthToken('').then(()=>{
                resolve(access_data)
            }).catch((e)=>{
                $.alert({
                    useBootstrap: false,
                    columnClass: 'medium',
                    title: 'Token Error',
                    type: 'red',
                    content: "The token being used to authenticate to the WFD API layer has expired. <br>QLIP attempted to refresh this token, but the refresh failed."
                })     
            })
        })
    }

    getOAuthToken(token) {
        return new Promise((resolve, reject) => {
            let scope = {errorColor:null,errorCode:null,errorMessage: null}
            let oAuthParams = wfdAPIgetParams()
            let authUrl = oAuthParams.wfd_url + '/api/authentication/access_token'
            let appKey = $('#wfd_appKey').val()

            let params =
            'username=' + oAuthParams['wfd_uname'] +
            '&password=' + oAuthParams['wfd_passwd'] + 
            '&client_id=' + oAuthParams['wfd_clientId'] + 
            '&client_secret=' + oAuthParams['wfd_clientSecret'] + 
            '&grant_type=password' + 
            '&auth_chain=OAuthLdapService'

            //console.log('getOAuthToken',oAuthParams,authUrl,appKey,params)
            
            $.ajax({
                async:true, //ONLY ASYNC FALSE (not sure how to get around this as i have to have the config data to init the data table)
                url: authUrl,
                data:params,
                method: "POST",
                headers: {
                    "appkey": appKey,
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Cache-Control": "no-cache"
                },
                success : function(response){
                    if (response.error != null) {
                        scope.errorColor = 'red'
                        scope.errorCode = response.error
                        scope.errorMessage = response.error_description
                        reject(scope)
                    }
                    else {
                        access_data.token = response.access_token
                        access_data.appkey = appKey
                        scope.accessToken = response.access_token
                        scope.appKey = appKey
                        resolve(scope)
                    }     
                },
                error : function(request,status,error){
                    //console.log(request)
                    let eMessage = "Please validate your connection credentials."
                    if (request.responseJSON) {
                        //be great if we could keep consistent responses...(IN THE SAME API) #vent
                        if (request.responseJSON.hasOwnProperty('error')) {
                            eMessage = request.responseJSON.error + ' - ' + request.responseJSON.error_description
                        }
                        else eMessage = request.responseJSON.errorCode + ' - ' + request.responseJSON.message
                    }
                    scope.errorColor = 'red'
                    scope.errorCode = status
                    scope.errorMessage = eMessage
                    reject(scope)
                }
            })
        })
    }

    getAPI(dsObj,scope,skipParse) {
        return new Promise((resolve, reject) => {
            let x = this

            if (dsObj) {
                let post_type = 'GET'
                let post_data = ''
                if (dsObj.datasource.hasOwnProperty('pdata')) {
                    post_type = 'POST'
                    post_data = JSON.stringify(dsObj.datasource.pdata)
                }
                
                $.ajax({
                    async:true, 
                    url: $('#wfd_url').val() + '/api' + dsObj.datasource.apiurl,
                    type: post_type,
                    headers: {
                        "Authorization": access_data.token,
                        "appkey": access_data.appkey,
                        "Content-Type": "application/json",
                        "Cache-Control": "no-cache"
                    },
                    data: post_data,
                    success : function(r){
                        let dataArr = []
                        if (skipParse) resolve(r)
                        else {
                            $(r).each(function(k,v){
                                if (v.hasOwnProperty(dsObj.datasource.tag)) dataArr.push(v[dsObj.datasource.tag])
                                else dataArr.push(v)
                            })    
                            dsObj.list = dataArr
                            resolve(dsObj)
                        }
                    },
                    error : function(xhr){
                        let dataArr = []
                        dsObj.list = dataArr
                        /*
                        try {
                            console.log('getAPI',xhr.status)
                            let actRes = JSON.parse(xhr.responseJSON)
                            
                            if (actRes.message.indexOf("UnAuthorized. Authentication Failed.") > -1) {
                                //console.log('getAPI getOAuth',xhr)
                                x.refreshToken().then(()=>{x.getAPI(dsObj,scope,skipParse)})
                            }
                        }
                        catch(e) {}
                        */

                        try {
                            if (xhr.status == 401) x.refreshToken().then(()=>{
                                x.getAPI(dsObj,scope,skipParse).then((r)=> resolve(r))
                            })
                        }
                        catch(e) {}
                        //resolve(dsObj)
                    }
                })
            }
            else resolve()
        })
    }

    //====================================================================================
    // NEW WORK
    //====================================================================================
    batchArray(arr,qty) {
        let finalArr = []
        let tmp_arr = []
        arr.forEach(function(v,k){
            tmp_arr.push(v)
            if ((k+1)%qty == 0 || (k+1) == arr.length) {
                finalArr.push(tmp_arr)
                tmp_arr = []
            }
        })
        return finalArr
    }

    wrapApi (aw,v) {
        if (Array.isArray(aw)) return v
        
        let returnKey
        let returnObj = Object.assign({}, aw);
        Object.keys(aw).forEach((k)=>{
            if (Array.isArray(aw[k])) returnKey = k
        })

        returnObj[returnKey] = v
        return returnObj
    }

    async doExistingCalls(ut,reqs) {
        let apiCalls = []
        let existRequests = ut.removeExisting
        if (!existRequests) existRequests = ut.findExisting

        //IF SINGLE RECORD FOR TO ARRAY
        existRequests.new_datasource = existRequests.datasource
        if (!Array.isArray(existRequests.new_datasource)) existRequests.new_datasource = [existRequests.new_datasource]

        let finalObj = {}
        //LOOP THROUGH existingRequests
        existRequests.new_datasource.forEach((er)=>{
            //SET THREAD COUNT/BATCH COUNT if not provided
            
            if (!er.hasOwnProperty('apiBatchQty')) er.apiBatchQty = 1
            
            //ATTEMPT TO BATCH CALLS (GET/DATA/ETC)
            if (er.hasOwnProperty('apiBatchOn')) {
                let toBatch = QLIPTypes.exportHelpers.getObjectDotNotation(er.pdata,er.apiBatchOn)
                let batched = this.batchArray(toBatch,er.apiBatchQty)
                
                batched.forEach((b)=>{
                    let tmpAPIReq = JSON.parse(JSON.stringify(er.pdata))
                    QLIPTypes.exportHelpers.setObjectDotNotation(tmpAPIReq,er.apiBatchOn,b)
                    apiCalls.push({url:er.apiUrl,pdata:tmpAPIReq,type:er.type})
                })
            }
            else apiCalls.push({url:er.apiUrl,pdata:er.pdata,type:er.type})
            finalObj[er.type] = []
        })
        
        //SET THREAD COUNT
        if (!existRequests.hasOwnProperty('apiThreads')) existRequests.apiThreads = 1

        //MAKE THE CALLS
        let responseData = await this.threadPromise(apiCalls,this.apiRequest,existRequests.apiThreads)

        //ASSEMBLE BACK RESPONSES (may need custom function at this point)
        responseData.forEach((rd)=>{
            rd.forEach((rr)=>{
                if (rr.request.response) {
                    if (Array.isArray(rr.request.response)) {
                        rr.request.response.forEach((ir)=>{
                            if (ut.updateRemoveExistingResponse) ir = ut.updateRemoveExistingResponse(ir)
                            if (!Array.isArray(ir)) ir = [ir]
                            ir.forEach((ii)=>{finalObj[rr.type].push(ii)})
                            
                        })
                    }
                }
            })          
        })

        //console.log('1',apiCalls)
        //console.log('2',responseData)
        //console.log('3',finalObj)

        return finalObj

    }

    async doExportAPICalls(fdata,exportType) {
        let pdata = {}
        let apiReq
        let apiCalls = []
        let exports =  []
        let exportResponse = {}
        let exportResponses = []

        Array.prototype.asyncForEach = async function(callback, thisArg) {
            thisArg = thisArg || this
            for (let i = 0, l = this.length; i !== l; ++i) {
                await callback.call(thisArg, this[i], i, this)
            }
        }

        Object.keys(fdata).forEach((f)=>{
            pdata[f] = fdata[f].value
        })
        
        let aRequest = await exportType.apiRequest(pdata)
        if (!Array.isArray(aRequest)) {
            exports.push({
                apiRequest:exportType.apiRequest,
                apiBatchOn:exportType.apiBatchOn,
                apiBatchQty:exportType.apiBatchQty,
                apiUrl:exportType.apiUrl
            })
        }
        else exports = aRequest

        await exports.asyncForEach(async (request)=>{
            apiCalls = []
            exportResponse = []

            //BATCH QTY IF NOT PROVIDED
            if (!request.hasOwnProperty('apiBatchQty')) request.apiBatchQty = 1
                 
            //MULTI NOW
            apiReq = await request.apiRequest(pdata)

            if (request.hasOwnProperty('apiBatchOn') && request.apiBatchQty != 1) {
                let toBatch = QLIPTypes.exportHelpers.getObjectDotNotation(apiReq,request.apiBatchOn)
                let batched = this.batchArray(toBatch,request.apiBatchQty)
                this.exportTotalBatchCount = batched.length
                this.exportTotalProccessed = 0
                
                batched.forEach((b)=>{
                    let tmpAPIReq = JSON.parse(JSON.stringify(apiReq))
                    QLIPTypes.exportHelpers.setObjectDotNotation(tmpAPIReq,request.apiBatchOn,b)
                    apiCalls.push({url:request.apiUrl,pdata:tmpAPIReq})
                })
            }
            else {
                let tmpar = {url:request.apiUrl,pdata:apiReq}
                if (!tmpar.pdata) delete tmpar.pdata
                apiCalls.push(tmpar)
            }

            //SET THREAD COUNT
            if (!request.hasOwnProperty('apiThreads')) request.apiThreads = 1

            //RUN THREADS
            this.progressExportVisuals()
            console.log('Pre Export (wfdAPI->doExportAPICalls->apiCalls)',apiCalls)
            exportResponse = await this.threadPromise(apiCalls,this.apiRequest,request.apiThreads,this.processExportResponse)
            if (request.hasOwnProperty('apiType')) exportResponse.apiType = request.apiType
            console.log('Post Export (wfdAPI->doExportAPICalls) Responses)',exportResponse)
            
            exportResponses.push(exportResponse)
        })

        //PROCESS RESPONSES
        let finalArr = []
        exportResponses.forEach((er) => {
            er.forEach((r)=>{
                r.forEach((rr)=>{
                    rr.request.response.forEach((rrr)=>{
                        if (er.hasOwnProperty('apiType')) {
                            if (!finalArr[er.apiType]) finalArr[er.apiType] = []
                            finalArr[er.apiType].push(rrr)
                        }
                        else finalArr.push(rrr)
                    })
                })
            })
        })
        
        //console.log('exportAPIRespones',finalArr)
        console.log('Post Export (wfdAPI->doExportAPICalls) Final)',finalArr)
        return finalArr
    }

    async doAPICalls(apiRequestData,uploadType) {
        let apiCalls = []
        let tData = []
        let skipped = []
        let finalArr = []

        Array.prototype.asyncForEach = async function(callback, thisArg) {
            thisArg = thisArg || this
            for (let i = 0, l = this.length; i !== l; ++i) {
                await callback.call(thisArg, this[i], i, this)
            }
        }

        progressInfo.skipped = 0

        //PREP CALLS
        await apiRequestData.asyncForEach(async (atype)=>{
            //CREATE TDATA ARR
            atype.tdata = []
            atype.tmp_calls = []
            atype.final = []
            atype.exists = []
            atype.calls.forEach((c)=>{
                if (c.skip) {
                    c.tdata.__skip == true
                    atype.skipped.push(c.tdata)
                }
                else if (c.__exists) {
                    c.tdata.__exists == true
                    atype.exists.push(c.tdata)
                    progressInfo.skipped = progressInfo.skipped + 1
                }
                else {
                    atype.tdata.push(c.tdata)
                    atype.tmp_calls.push(c.api)
                }
            })

            //New to deal with POS type transactions where you need to group things together in a multi (then batch)
            if (atype.hasOwnProperty('groupFN')) {
                let groups = atype.groupFN(atype.tdata)
                let gArr = []
                //groups object
                groups.forEach((g)=>{ gArr.push({ginfo:g,tmp_calls:[],tdata:[]})})

                //associate tdata to group (by all fields) *batch by group
                atype.calls.forEach((c)=>{
                    let t = c.tdata
                    gArr.forEach((g)=>{
                        let gkeys = Object.keys(g.ginfo)
                        let invalid = false
                        if (!invalid) {
                            gkeys.forEach((gk)=>{
                                if (g.ginfo[gk] != t[gk]) invalid = true
                            })
                        }
                        if (invalid == false) {
                            g.tmp_calls.push(c.api)
                            g.tdata.push(c.tdata)
                        }
                    })
                })

                //further batch (by qty) & create calls
                //atype.final = []
                gArr.forEach((g)=>{
                    g.tmp_calls = this.batchArray(g.tmp_calls,atype.apiBatchQty)
                    g.tdata = this.batchArray(g.tdata,atype.apiBatchQty)
                    g.tmp_calls.forEach((calls,k)=>{
                        atype.final.push({pdata:atype.apiWrap(g.ginfo,calls),tdata:g.tdata[k],url:atype.apiUrl,method:atype.apiMethod})
                    })
                })
                atype.gArr = gArr  //for visiblity         
            }
            else { //STD BATCHING
                //BATCH ARRAY && TDATA (need tdata for syncing of respones per batch to table data)
                atype.tmp_calls = this.batchArray(atype.tmp_calls,atype.apiBatchQty)
                atype.tdata = this.batchArray(atype.tdata,atype.apiBatchQty)
                
                //ADD FINAL CALLS TO API-REQUEST FORMAT
                atype.tmp_calls.forEach((f,k)=>{
                    if (atype.apiWrap) {
                        //if (atype.apiBatchQty == 1) f.forEach((ff)=>{ff = this.wrapApi(atype.apiWrap,ff)})
                        //else f = this.wrapApi(atype.apiWrap,f)
                        f = this.wrapApi(atype.apiWrap,f)
                    }
                    //else if (atype.apiBatchQty == 1) f = f[0]
                    if (atype.apiBatchQty == 1) f = f[0]
                    
                    let fobj = {url:atype.apiUrl,pdata:f,tdata:atype.tdata[k]}
                    if (atype.apiMethod) fobj.method = atype.apiMethod
                    //if (atype.apiMethod == 'DELETE') delete atype.apiMethod
                    atype.final.push(fobj)
                    
                })
            }
        })

        console.log('Pre Import (wfdAPI->doAPICalls)',apiRequestData)
        progressInfo.total = progressInfo.skipped
        progressInfo.processed = progressInfo.skipped

        apiRequestData.forEach((rd)=>{
            rd.final.forEach((f)=>{
                f.tdata.forEach((t)=>{
                    if (!t.__ignore) progressInfo.total = progressInfo.total + 1
                })
            })
        })
        
        progressInfo.errors = 0
        progressInfo.pct = 0

        this.progressVisuals()
    
        //THREAD EACH API TYPE
        let responseData
        let responseDataArr = []
        let rawSuccessResponses = []
        
        await apiRequestData.asyncForEach(async (atype,k)=>{
            if (atype.updateKeyValue) this.attemptToUpdateKeyValues(atype,rawSuccessResponses)
            responseData = await this.threadPromise(atype.final,this.apiRequest,atype.apiThreads,this.processResponse)
            
            //KEEP TRACK OF SUCCESS RESPONSES
            responseData.forEach((r)=>{
                r.forEach((rr)=>{
                    if (rr.request.hasOwnProperty('response')) {
                        let responses = this.captureRawResponse(rr.request.response)
                        responses.forEach((ir)=>{rawSuccessResponses.push(ir)})
                    }
                })
            })

            //PROCESS RESPONSES
            let finalArr = []

            /*
            console.log('responseData',responseData)
            //CHECK FOR ALLOWABLE ERRORS
            if (atype.allowableErrors) {
                atype.allowableErrors.forEach((e)=>{
                    finalArr.forEach((f)=>{
                        if (f.hasOwnProperty('error')) {
                            if (f.error.errorCode == e.errorCode) f.allow_error = true
                        }
                    })
                })
            }
            */
        
            atype.results = responseData
            responseDataArr.push(atype)
            //console.log('responseData',responseData)
            //console.log('rawSuccessResponses',rawSuccessResponses)
            //if (k == 1 || 1==1) console.log('atype',atype) 
        })
        return responseDataArr
    }

    // Async threading function
    async threadPromise(data,promiseFunc,threadCount,pctFunc) {  
        let threadsData = []
        for (let i = 0; i < data.length; i += threadCount) { 
            let pct = Math.round(((i + 1)/(data.length)) * 100)
            //if (debug) console.log('Percentage',pct,'(' + (i + 1) + '/' + (data.length) + ')')  
            
            let x = this
            const requests = data.slice(i, i + threadCount).map((d) => {
                return promiseFunc(d,pctFunc,x)
                //if (pctFunc) return promiseFunc(d,pctFunc,x)
                //else return promiseFunc(d)
            })
            
            let threadData = await Promise.all(requests)
            threadsData.push(threadData)
        }
        return threadsData
    }

    processResponse(res,x) {
        //console.log('processResponse',res)
        if (res) {
            //if (res.request.httpcode == 207 || res.request.httpcode == 400 || res.request.httpcode == 404) res = x.captureErrors(res) //console.log('PARTIAL SUCCESS')}
            if (res.request.httpcode == 207 || res.request.httpcode >= 400) res = x.captureErrors(res) //console.log('PARTIAL SUCCESS')}
            else x.captureIDs(res)
        }
        x.progressVisuals(res)
        return res
    }

    processExportResponse(res,x) {
        //d.forEach((res)=>{
            if (res) {
                //if (res.request.httpcode == 207 || res.request.httpcode == 400 || res.request.httpcode == 404) res = x.captureErrors(res) //console.log('PARTIAL SUCCESS')}
                if (res.request.httpcode == 207 || res.request.httpcode >= 400) res = x.captureErrors(res) //console.log('PARTIAL SUCCESS')}                
            }
        //})
        
        x.exportTotalProccessed = x.exportTotalProccessed + 1
        let pct = x.exportTotalProccessed/x.exportTotalBatchCount
        
        x.progressExportVisuals(pct)
        return res
    }

    attemptToUpdateKeyValues(atype,successes) {
        atype.final.forEach((a,k)=>{
            let backupObj = Object.assign({},a)
            if (Array.isArray(a.pdata)) {
                let tdata,b
                a.pdata.forEach((b,kk)=>{
                    //console.log(b)
                    tdata = a.tdata[kk]
                    b = atype.updateKeyValue(successes,tdata,b)
                    if (!b) {
                        b = {}
                        a.tdata[kk]._ignore = true
                    }
                })
            }
            else {
                a.pdata = atype.updateKeyValue(successes,a.tdata[0],a.pdata)
                if (!a.pdata) {
                    a.pdata = {}
                    a.tdata[0].__ignore = true
                }
            }
        })

    }

    apiRequest(params,pctFunc,y) {
        let url = params.url
        let pdata
        
        if (params.hasOwnProperty('pdata')) {
            pdata = params.pdata
            if (Array.isArray(pdata)) {
                pdata = pdata.filter(item => JSON.stringify(item) !== "{}")
                if (JSON.stringify(pdata) == '[]') pdata = ""
            }
            else if (JSON.stringify(pdata) == '{}') pdata = ""
            if (!pdata) params.__skip = true
        }
        let transaction = {}
        let x = this

        return new Promise((resolve, reject) => {
            
            //SET METHOD (GET OR POST) **ADDING DELETE ; thinking of UPDATE/PUT/etc
            let method = 'GET'

            if (params.method) method = params.method
            else if (pdata) method = 'POST'

            if (method == 'DELETE') pdata = ""

            //GET CONNECTION DATA
            let connDetails = wfdAPIgetParams()

            //SET REQ OPTIONS
            let reqOptions = {
                async:true,
                url: connDetails['wfd_url'] +'/api' + url,
                type: method,
                async: true,
                headers: {
                    "Authorization": access_data.token,
                    "appkey": access_data.appkey,
                    "Content-Type": "application/json",
                    "Cache-Control": "no-cache"
                },
                data: JSON.stringify(pdata)
            }
            if (!pdata) delete reqOptions.data

            transaction.tdata = params.tdata
            if (params.type) transaction.type = params.type
            transaction.request = {}
            transaction.request.options = reqOptions

            reqOptions.success = function(response,status,xhr){
                transaction.request.edtm = new Date()
                transaction.request.response = response
                transaction.request.status = status
                transaction.request.httpcode = xhr.status
                if (transaction.request.options.hasOwnProperty('data')) transaction.request.body = JSON.parse(transaction.request.options.data)
                if (pctFunc) pctFunc(transaction,y)
                resolve(transaction)
            }
            reqOptions.error = function(xhr,status,error){
                transaction.request.edtm = new Date()
                transaction.request.error = error
                transaction.request.status = status
                transaction.request.httpcode = xhr.status
                transaction.request.response = xhr.responseJSON
                if (transaction.request.options.hasOwnProperty('data')) transaction.request.body = JSON.parse(transaction.request.options.data)
                try {
                    if (transaction.request.response) {
                        //let actRes = JSON.parse(transaction.request.response)                    
                        let actRes = transaction.request.response
                        if (actRes.message.indexOf('UnAuthorized') > -1) {
                            if (transaction.tdata) progressInfo.processed = progressInfo.processed - transaction.tdata.length
                            y.refreshToken().then(()=>{y.apiRequest(params,pctFunc,y)})    
                        }
                    }
                    //pctFunc(transaction,y)
                }
                catch(e) {console.log('apiRequest - catch1',e)}
                if (pctFunc) pctFunc(transaction,y)                
                resolve(transaction)
            }

            transaction.request.sdtm = new Date()
            
            //CHECK FOR CANCEL OR SKIP
            if (allowAPICalls == false) resolve()
            else if (params.__skip) resolve(transaction)
            else $.ajax(reqOptions)
        })
    }

    captureIDs(fullres) {
        let res = fullres.request.response
        if (Array.isArray(res)) {
            if (fullres.tdata.length == res.length) {
                res.forEach((r,k)=>{
                    //console.log('r',r)
                    if (r.id) fullres.tdata[k].__id = r.id
                })
            }
        }
        else if (res) {
            if (res.id) fullres.__id = res.id
        } 
    }

    captureErrors(r) {
        let mpResponse = r.request.response
        r.error_cnt = 0
        
        if (!r.request.response) {
            r.tdata[0].error = {}
            r.tdata[0].error.errorCode = r.request.httpcode 
            r.tdata[0].error.message = 'A ([httpcode] error occured.)'.replace('[httpcode]',r.request.httpcode)
            r.error_cnt = r.error_cnt + 1
        }
        
        else if (r.request.response.hasOwnProperty('details')) {
            let mpErrArr = r.request.response.details['error-offsets']
            mpErrArr.forEach((i)=>{
                
                if (r.request.response.details.results[i].error.hasOwnProperty('details')) {
                    //GET SINGLE ERROR    
                    if (r.request.response.details.results[i].error.details.errors) {
                        r.tdata[i].error = r.request.response.details.results[i].error.details.errors[0]
                        r.error_cnt = r.error_cnt + 1
                    }
                    else {
                        r.tdata[i].error = r.request.response.details.results[i].error
                        r.error_cnt = r.error_cnt + 1
                    }
                }
                else {
                    r.tdata[i].error = r.request.response.details.results[i].error
                    r.error_cnt = r.error_cnt + 1
                }
            })
        }
        else if (r.request.response.hasOwnProperty('errorCode')) {
            r.tdata.forEach((rc)=>{
                rc.error = r.request.response
                r.error_cnt = r.error_cnt + 1
            })
        }
        return r
    }

    captureRawResponse(r) {
        let finalArr = []
        if (r) {
            if (Array.isArray(r)) r.forEach((ir)=>{finalArr.push(ir)})
            else if (r.hasOwnProperty('details')) {
                r.details.results.forEach((ir)=>{
                    if (ir.hasOwnProperty('success')) finalArr.push(ir.success)
                })
            }
        }
        finalArr.push(r)
        return finalArr
    }

    progressExportVisuals(pct) {
        if (pct) {
            let progWidth = $('#exportprogressModalContainer').width()
            $('#exportProgressBar').css('width',pct * 100.00 + '%');
            if (pct == 1) $('#exportprogressModalContainer').removeClass('glowBase')
        }
        else $('#exportprogressModalContainer').addClass('glowBase')  
    }

    progressVisuals(v) {
        //if (btch) {
            //btch.forEach((v)=>{
                if (v) {
                    if (v.hasOwnProperty('error_cnt')) progressInfo.errors = progressInfo.errors + v.error_cnt
                    v.tdata.forEach((s)=>{
                        if (s.__ignore) progressInfo.skipped = progressInfo.skipped + 1
                    })
                    progressInfo.processed = progressInfo.processed + v.tdata.length   
                }
            //})
        //}
        else $('#progressModalContainer').addClass('glowBase')
        progressInfo.pct = ((progressInfo.processed)/progressInfo.total) * 100.00

        let sCount = progressInfo.processed
        let eCount = progressInfo.errors
        let tCount = progressInfo.total
        let skpCount = progressInfo.skipped
        let pct = progressInfo.pct

        $('#progCountTotal').html(progressInfo.total)
        $('#progCountSkip').html(progressInfo.skipped)

        let svar = ''
        if (eCount > 1) svar = 's'
        if (eCount > 0) {
            $('#progCountErrors').html(eCount)
            $('#progErrorCount').text("(" + eCount + ' Error' + svar + ")")
        }
        
        let errPcnt = eCount / tCount
        let sccPcnt = (sCount - eCount - skpCount) / tCount
        let skpPcnt = skpCount / tCount
        let totPcnt = pct/100.00
        
        let progWidth = ($('#progressModalContainer').width() * totPcnt)

        //$('#progBarSkipped').width((progWidth * skpPcnt) - 1);
        //$('#progBarSuccess').width((progWidth * sccPcnt) - 1);
        //$('#progBarError').width(progWidth * errPcnt);

        $('#progBarSkipped').css('width',100.00 * skpPcnt + '%');
        $('#progBarSuccess').css('width',100.00 * sccPcnt + '%');
        $('#progBarError').css('width',100.00 * errPcnt + '%');
        $('#progCountProcessed').html((sCount))

        if (totPcnt == 1) {
            $('#progressModalContainer').removeClass('glowBase')
            $('#progExit').hide()  
            $('#progShowAll').show()
            if (eCount == 0) {
                //$('#progShowDismiss').show()
                //$('#progShowAll').hide()
            }
            else 
            $('#progShowErrors').show()
        }
    }

    getDataToObject(res) {
        finalArr = []
        res.data.children.forEach((d)=>{
          tmpObj = {}
        d.attributes.forEach((c)=>{
            ky = c.key
          if (c.hasOwnProperty('alias')) ky = c.alias
            tmpObj[ky] = c.value
        })
          finalArr.push(tmpObj)
      })
     return finalArr
    }
    
}

