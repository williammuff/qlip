var QLIPTypes = {
    toJSON: function(dObj) {
        APIArr = [];
        tdata_orig = hot.getData()
        tcols = hot.getColHeader()
        dObj.jdata = []
        dObj.kdata = []

        tdata = []
        if (tcols[0] == 'Import Status') {
            tcols.shift()
            $(tdata_orig).each(function(k, v) {
                v.shift();
                tdata.push(v);
            })
        } else tdata = tdata_orig

        //JDATA CONVERSION ARRAY TO KEY BASED OBJECTS
        if (dObj.hasOwnProperty('apiMap')) {
            $(tdata).each(function(k, v) {
                rec = {}
                if (!v.every(x => x === null) && !v.every(x => x === "")) { //CHECK ROW FOR BLANK
                    $(v).each(function(kk, vv) {
                        rowval = vv
                        ky = dObj.cdata.find(x => x['data'] === tcols[kk])
                        if (rowval) {
                            if (typeof rowval == 'string') rowval = rowval.replace(/(\r\n|\n|\r)/gm, "")
                        }
                        if (ky) rec[ky.name] = rowval
                    })
                    rec.__rnum = k
                    dObj.jdata.push({rnum:k,tdata:rec})
                    dObj.kdata.push(rec)
                }
            })
        }
    
        //API MAP
        dObj.jdata.forEach((r)=>{
            tmpApiMap = dObj.apiMap(r.tdata) 
            if (!Array.isArray(tmpApiMap)) tmpApiMap = [tmpApiMap]    
            tmpApiMap.forEach((aa,k)=>{
                if (!Array.isArray(aa)) aa = [aa]
                
                aa.forEach((a)=>{
                    tmpAPI = {}
                
                    //SET DATA POINTS IF NOT POPULATED IN ACTUAL apiMap Function (higher at root of upload)
                    if (a.hasOwnProperty('api')) tmpAPI.api = a.api
                    else tmpAPI.api = a

                    if (a.hasOwnProperty('apiUrl')) tmpAPI.apiUrl = a.apiUrl
                    else tmpAPI.apiUrl = dObj.apiUrl

                    if (a.hasOwnProperty('apiBatchQty')) tmpAPI.apiBatchQty = a.apiBatchQty
                    else if (dObj.hasOwnProperty('apiBatchQty')) tmpAPI.apiBatchQty = dObj.apiBatchQty
                    else tmpAPI.apiBatchQty = 1

                    if (a.hasOwnProperty('apiThreads')) tmpAPI.apiThreads = a.apiThreads
                    else if (dObj.hasOwnProperty('apiThreads')) tmpAPI.apiThreads = dObj.apiThreads
                    else tmpAPI.apiThreads = 1

                    if (a.hasOwnProperty('updateKeyValue')) tmpAPI.updateKeyValue = a.updateKeyValue            
                    else tmpAPI.updateKeyValue = ""

                    if (a.hasOwnProperty('apiWrap')) tmpAPI.apiWrap = a.apiWrap
                    else if (dObj.hasOwnProperty('apiWrap')) tmpAPI.apiWrap = dObj.apiWrap
                    else tmpAPI.apiWrap = ""

                    if (a.hasOwnProperty('allowableErrors')) tmpAPI.allowableErrors = a.allowableErrors
                    else tmpAPI.allowableErrors = ""

                    if (a.hasOwnProperty('apiType')) tmpAPI.apiType = a.apiType
                    else if (dObj.hasOwnProperty('apiType')) tmpAPI.apiType = dObj.apiType
                    else tmpAPI.apiType = ""
                    
                    if (a.hasOwnProperty('removeDuplicates')) tmpAPI.removeDuplicates = a.removeDuplicates
                    else tmpAPI.removeDuplicates = false

                    if (a.hasOwnProperty('apiMethod')) tmpAPI.apiMethod = a.apiMethod

                    returnObj = {'__rnum': r.rnum,'tdata': r.tdata,'api': tmpAPI.api,'apiUrl': tmpAPI.apiUrl,'apiBatchQty':tmpAPI.apiBatchQty,'apiThreads':tmpAPI.apiThreads,'apiWrap':tmpAPI.apiWrap,'updateKeyValue':tmpAPI.updateKeyValue,'sort':k,'allowableErrors':tmpAPI.allowableErrors,'apiType':tmpAPI.apiType,'removeDuplicates':tmpAPI.removeDuplicates,apiMethod:tmpAPI.apiMethod}
                    if (dObj.hasOwnProperty('groupFN')) returnObj.groupFN = dObj.groupFN
                    APIArr.push(returnObj)    
                })
            })
        })
        
        //REMOVE EXISTING
        //console.log('find existing',dObj.findExisting)
        if (dObj.hasOwnProperty('findExisting') && dObj.kdata) dObj.findExisting.datasource.pdata = dObj.findExisting.setDataSource(dObj.kdata,dObj.findExisting.datasource.pdata)
        if (dObj.hasOwnProperty('updateRemoveExisting') && dObj.kdata) dObj.updateRemoveExisting(dObj.kdata)
    
        //SKIP RECORDS THAT ARE LIKE (ex. 10 rows for leave case... only 1 for actual case..)
        apiFilter = []
        APIArr.forEach((a)=>{   
            if (a.removeDuplicates)  {
                s = {__rnum:a.__rnum,json:btoa(JSON.stringify(a.api))}
                match = apiFilter.find(m => m.json === btoa(JSON.stringify(a.api)))            
                if (match) {
                    a.tdata.__from_rnum = match.__rnum
                    a.skip = true
                } 
                else a.skip = false
                apiFilter.push(s)
            }
        })
        APIArrCopy = Object.assign([],APIArr)
        //lodash unique based on specific props of objects
        tmp = []
        uniqApiUrls = []
        APIArr.forEach((a)=>{
            Object.keys(a.tdata).forEach((ky)=>{
                a.apiUrl = a.apiUrl.replace('$.' + ky,a.tdata[ky])
            })

            tmpObj = {apiUrl:a.apiUrl,apiBatchQty:a.apiBatchQty,apiThreads:a.apiThreads,apiWrap:a.apiWrap,updateKeyValue:a.updateKeyValue,sort:a.sort,allowableErrors:a.allowableErrors,apiType:a.apiType,apiMethod:a.apiMethod}
            if (a.hasOwnProperty('groupFN')) tmpObj.groupFN = a.groupFN
            if (!tmpObj.apiWrap) delete tmpObj.apiWrap
            if (tmp.indexOf(JSON.stringify(tmpObj)) == -1) uniqApiUrls.push(tmpObj)
            tmp.push(JSON.stringify(tmpObj))
            delete a.apiBatchQty
            delete a.apiThreads
            delete a.sort
            delete a.updateKeyValue
            delete a.removeDuplicates
        })
    
        finalAPIArr = []
        uniqApiUrls.forEach((au)=>{
            tmpObj = Object.assign({},au)
            tmpObj.calls = []
            tmpObj.skipped = []
            APIArrCopy.forEach((a)=>{
                if (a.apiUrl == au.apiUrl) {
                    //delete a.apiUrl
                    if (a.skip) tmpObj.skipped.push(a)
                    else tmpObj.calls.push(a)
                    delete a.skip
                }
            })
            finalAPIArr.push(tmpObj)
        })
        console.log('Pre Import (QLIPTypes->tojson)',finalAPIArr)
        return finalAPIArr;
    },
    removeIfExists: (ut,responses,apiCalls)=>{
        if (responses) {  
            if (ut.removeExisting.checkExists) {
                apiCalls.forEach((v)=>{
                    v.calls.forEach((vv)=>{
                        if (ut.removeExisting.checkExists(vv,responses[vv.apiType],vv.apiType)) {
                            vv.__exists = true
                        }
                    })
                })
            }
        }
    },
    updateIfExists: (ut,responses,apiCalls)=>{
        if (responses) {  
            if (ut.findExisting.updateExists) {
                apiCalls.forEach((v)=>{
                    v.calls.forEach((vv)=>{
                       vv = ut.findExisting.updateExists(vv,responses[vv.apiType],v.calls,vv.apiType)
                    })
                })
            }
        }
        
        //console.log('ut',ut)
        //console.log('responses',responses)
        //console.log('apiCalls',apiCalls)
    },
    findIt: function(v) {
        sterm = v
        ut = this.imports.find(x => x.type_name.indexOf(sterm) == 0)
        if (!ut) ut = this.exports.find(x => x.type_name.indexOf(sterm) == 0)
        if (Object.keys(ut).indexOf('toJSON') == -1) ut.toJSON = this.toJSON
        return ut;
    },
    onSelectionChange: function(dObj) {
        if (dObj.type == 'Import') return this.importRender(dObj)
        else if (dObj.type == 'Export') return this.exportRender(dObj)
    },
    generateSelectize: function(prompt,et){

        fieldName = 'export__' + prompt.name
        formHTML = '<div class="form-group"><label>'+prompt.label+'</label>[PROMPT]</div>'
        selectHTML = '<select name="'+prompt.name+'" id="'+fieldName+'" placeholder="Please select a '+prompt.label+'"></select>'
        formHTML = formHTML.replace('[PROMPT]',selectHTML)
        $('#exportForm').append(formHTML)

        prompt.list = prompt.list.sort((a,b) => a.name - b.name)

        $('#' + fieldName).selectize({
            persist: false,
            //openOnLoad: true,
            maxItems: 1,
            valueField: 'id',
            labelField: 'name',
            searchField: ['name'],
            options: prompt.list,
            onChange: (a)=>{
                this.onChangerequireToShow(et)
            }
        });

    },
    generateTextField: (prompt)=>{
        fieldName = 'export__' + prompt.name
        formHTML = '<div id="'+fieldName+'_formgroup" class="form-group"><label>'+prompt.label+'</label>[PROMPT]</div>'
        selectHTML = '<input class="form-control input-txt" name="'+prompt.name+'" id="'+fieldName+'" placeholder="[PLACEHOLDER]">'
        rpv = ""
        if (prompt.info) rpv = prompt.info
        selectHTML = selectHTML.replace('[PLACEHOLDER]',rpv)
        formHTML = formHTML.replace('[PROMPT]',selectHTML)
        //console.log(formHTML)
        $('#exportForm').append(formHTML)
    },
    generatePikday: (prompt)=>{
        fieldName = 'export__' + prompt.name
        formHTML = '<div id="'+fieldName+'_formgroup" class="form-group"><label>'+prompt.label+'</label>[PROMPT]</div>'
        selectHTML = '<input class="form-control" name="'+prompt.name+'" id="'+fieldName+'" placeholder="YYYY-MM-DD" readonly></select>'
        formHTML = formHTML.replace('[PROMPT]',selectHTML)

        $('#exportForm').append(formHTML)

        datePicker = new Pikaday(
        {
            field: document.getElementById(fieldName),
            format : "YYYY-MM-DD",
            firstDay: 1    
        });
        $('#' + fieldName).data('datepicker', datePicker);

    },
    onChangerequireToShow: (et)=>{
        //FUNCTION TO MAKE EVERYTHING ENABLED
        $('#exportForm').find('.form-group').each((k,v)=>{
            $(v).css('display','block')
        })

        //DISABLE FIELDS IF APPLICABLE
        et.prompts.forEach((p)=>{
            if (p.hasOwnProperty('requireToShow')) {
                checkField = $('#export__' + p.requireToShow.name).val()
                checkFieldInt = parseInt(checkField)
                //console.log(checkField,p.requireToShow.values,p.requireToShow.values.indexOf(checkField),checkField)
                if (p.requireToShow.values.indexOf(checkFieldInt) == -1) {
                    field = 'export__' + p.name + '_formgroup'
                    $('#' + field).hide()
                }
            }
        })
    },
    async exportRender(dObj) {
        try {if (hot) hot.destroy()} catch (e) {}
        
        let iwfdAPI = new wfdAPI()
        
        //UPDATE MODAL TITLE
        $('#exportModalTitle').html(dObj.type_name)
        $('#exportProgressBar').width(0)
        $('#exportForm').html('')

        showLoadingModal('Logging into Dimensions to gather config data')

        //GET DATASOURCE if API and SET LIST for hard coded lists
        if (dObj.prompts) {
            await Promise.all(dObj.prompts.map(async (prompt) => {
                if (prompt.hasOwnProperty('datasource')) {
                    let ds = prompt.datasource
                    if (prompt.datasource.hasOwnProperty('apiurl')) {
                        ds = await iwfdAPI.getAPI(prompt)
                        ds = ds.list[0][prompt.datasource.wrap]
                    }
                    prompt.list = ds
                } 
            }));
    
            //GENERATE FORM FIELDS
            dObj.prompts.forEach((prompt)=>{
                //GENERATE SELECTIZE.......
                if (prompt.hasOwnProperty('list')) this.generateSelectize(prompt,dObj)
                else if (prompt.type == 'date') this.generatePikday(prompt)
                else this.generateTextField(prompt)
            })
            if (dObj.hasOwnProperty('promptModalAdjust')) dObj.promptModalAdjust()
        }

        showLoadingModal()
        if (dObj.prompts) this.onChangerequireToShow(dObj)
        $('#exportModal').modal()
        if (!dObj.prompts) $('#exportFromAPI').click()
    },
    importRender: (dObj,hist_data)=> {
        if (typeof dObj == 'undefined') dObj = null

        let dt = document.getElementById('data_table')
        let cdata = dObj.cdata
        let column_headers = [];
        let column_config = [];
        let iwfdAPI = new wfdAPI()

        apiPromiseChain = []
        showLoadingModal('Logging into Dimensions to gather config data')

        $(cdata).each(function(k, v) {
            if (v['visible']) {
                cconfig = {
                    label: v['data']
                }
                if (v.hasOwnProperty('datasource')) {
                    if (Array.isArray(v.datasource)) {
                        cconfig.type = 'dropdown'
                        cconfig.source = v.datasource
                    } else apiPromiseChain.push(iwfdAPI.getAPI(v))
                }
                column_headers.push(v['data']);
                column_config.push(cconfig)
            }
        })

        Promise.all(apiPromiseChain).then(function(promiseArr) {
            $(promiseArr).each(function(k, v) {
                $(column_config).each(function(kk, vv) {
                    if (vv['label'] == v['data']) {
                        column_config[kk] = {
                            label: v['data'],
                            type: 'autocomplete',
                            source: v.list,
                            strict: true
                        }
                    }
                })
            })

            //tmp_obj = { type: 'autocomplete', source:tmp_config_obj, strict:true} 

            tData = []
            if (hist_data) tData = hist_data
            else if (dObj.hasOwnProperty('data') && autoConnectionTestMode) tData = dObj['data']

            showLoadingModal()
            try {if (hot) hot.destroy()}
            catch (e) {}

            hot = new Handsontable(dt, {
                startRows: 30,
                rowHeaders: true,
                contextMenu: true,
                stretchH: 'all',
                copyRowsLimit: 5000,
                autoWrapRow: true,
                autoWrapCol: true,
                autoInsertRow: true,
                manualColumnResize: true,
                data: tData,
                columns: column_config,
                colHeaders: column_headers,
                minSpareRows: 1,
                trimWhitespace: true,
                wordWrap: false,
                trimDropdown: true,
                cdata: cdata,
                beforePaste: function(data, coords) {
                    for (i = 0; i<data.length; i++){
                        for(j = 0; j<data[i].length; j++){
                            data[i][j] = data[i][j].trim(); //.replace(/\r\n/g,"");
                        }
                    }
                //console.log(data);
                },
                afterChange: function(change, source) {
                    //console.log('handsOnTable - After Change',change,source)
                    if (hot && change) {
                        //HAS ANYTHING CHANGED (global VAR)
                        odata = hot['original_data']
                        cdata = this.getData()
                        if (change != null && source != 'loadData') {
                            if (!compareArrays(odata, cdata)) gl_table_edited = true
                        }
                        return;
                    }
                }
            })

            hot.validateCells()

            so = getSelectizeOption('#uploadSelection')
            if (so) hot['uploadType'] = so.type_name
            hot['original_data'] = hot.getData()

            hot.updateSettings({
                height: $(window).height() - $("#data_table").offset().top
            })

            $(window).resize(function() {
                if (hot) {
                    hot.updateSettings({
                        height: $(window).height() - $("#data_table").offset().top
                    })
                }
            });
        })
        
    },
    errorColumnVisuals() {
        if (hot) {
            cname = hot.getColHeader(0)
            if (cname == 'Import Status') {
                $('#data_table').find('tr').each((k,v)=>{
                    cell = $(v).find('.htNoWrap').eq(0)
                    if (cell.length > 0) {
                        cellcontents = cell.html()
                        cellcontents = cellcontents.split('|')
                        if (cellcontents.length > 1) {
                            updHTML = '<div title="' + cellcontents[1] + '">' + cellcontents[0] + '</div>';
                            cell.html(updHTML)
                        }                        
                    }
                })
                var x = []; 
                hot.getColHeader().forEach((v,k)=>{ 
                    width = hot.getColWidth(k)
                    x.push(width)
                 })
                 x[0] = 90
                 hot.updateSettings({colWidths:x})
            }
        }
    },
    postExportRenderTableData: (tdata)=>{

        try {if (hot) hot.destroy()}
        catch (e) {}

        let dt = document.getElementById('data_table')

        //cdata
        //tdata
        let column_config = []
        let column_headers = []
        $(tdata.headers).each(function(k, v) {
            column_headers.push(v);
            column_config.push({label: v})
        })
        let table_data = []
        tdata.data.forEach((r)=>{
            line = []
            Object.keys(r).forEach((c)=>{line.push(r[c])})
            table_data.push(line)
        })

        hot = new Handsontable(dt, {
            startRows: 30,
            dropdownMenu: ['filter_by_condition','filter_by_value'],
            filters: true,
            rowHeaders: true,
            contextMenu: true,
            stretchH: 'all',
            copyRowsLimit: 5000,
            autoWrapRow: true,
            autoWrapCol: true,
            autoInsertRow: true,
            manualColumnResize: true,
            data: table_data,
            columns: column_config,
            colHeaders: column_headers,
            minSpareRows: 1,
            trimWhitespace: true,
            wordWrap: false,
            trimDropdown: true,
            //cdata: cdata,
            beforePaste: function(data, coords) {
                for (i = 0; i<data.length; i++){
                    for(j = 0; j<data[i].length; j++){
                        data[i][j] = data[i][j].trim(); //.replace(/\r\n/g,"");
                    }
                }
            //console.log(data);
            }
        })

        $('#exportModal').modal('hide')

        hot.updateSettings({
            height: $(window).height() - $("#data_table").offset().top
        })

        $(window).resize(function() {
            if (hot) {
                hot.updateSettings({
                    height: $(window).height() - $("#data_table").offset().top
                })
            }
        });
    },
    findEnvironmentTypeByUrl: (url)=>{
        var retVal 
        eTypes = QLIPTypes.environmentTypes
        eTypes.forEach((e)=>{
            if (!retVal) {
                if (e.urlCheck(url)) retVal =  e
            }
        })
        return retVal
    },
    getEnvironmentTypeByName: (enm)=>{
        return QLIPTypes.environmentTypes.find(e => e.name == enm)
    },
    environmentTypes: [
        {
            name:'Production',
            urlCheck: (url)=>{
                if (
                    (url.indexOf('prd.mykronos') > -1) 
                    && 
                    (
                        (!(url.indexOf('uat') > -1)) &&
                        (!(url.indexOf('dev') > -1)) &&
                        (!(url.indexOf('trn') > -1)) &&
                        (!(url.indexOf('tst') > -1)) &&
                        (!(url.indexOf('edu') > -1)) &&
                        (!(url.indexOf('stg') > -1))
                    ) //|| 1==1 //WAS FOR TESTING
                ) return true
                else return false
            },
            enableImportWarning:true
        }
    ],

    //===============================================================================================================================  
    //      UPLOAD TYPE DEFINITIONS
    //===============================================================================================================================
    imports: [
        { //ACCRUAL BAL
            type_name: 'Accrual Balance',
            //excludeEnvironmentTypes: ['Production'],
            /*
            allowableErrors: [{
                errorCode: 'WTK-170000',
                errorMessage: 'The Cost Center has previously been loaded'
            }],
            */
            cdata: [
                
                {visible: true,data: 'Transaction Type',name: 'trans_type',datasource:['Reset','Update']},
                {visible: true,data: 'Person Number',name: 'employee'},
                {visible: true,data: 'Effective Date',name: 'effectiveDate'},
                {visible: true,data: 'Accrual Code',name: 'accrualCode',datasource: {apiurl: '/v1/timekeeping/setup/accrual_codes',tag: 'name'}},
                {visible: true,data: 'Amount',name: 'amount'},
                {visible: true,data: 'Probation Amount',name: 'probationAmount'}
            ],
            apiMap: function(data) {
                request = {}
                if (data.trans_type.toUpperCase() == 'RESET')
                {
                    request.apiUrl = '/v1/timekeeping/accruals/resets'
                    request.apiWrap = {"accrualResets":[]}
                    request.apiBatchQty = 5
                    request.apiThreads = 5
                    request.api = 
                    {
                        "accrualReset": {
                            "accrualCode": {
                            "qualifier": data.accrualCode
                            },
                            "amount": data.amount,
                            //"amountType": "AMOUNTTYPE",
                            "effectiveDate": dateAdjust(data.effectiveDate),
                            "employee": {
                            "qualifier": data.employee
                            },
                            "probationAmount": data.probationAmount
                        }
                    }
                    if (!data.probationAmount) delete request.api.accrualReset.probationAmount
                    //if (data.probationAmount == 'True') delete x.accrualReset.amount
                    //else delete x.accrualReset.probationAmount
                }

                if (data.trans_type.toUpperCase() == 'UPDATE') {
                    request.apiUrl = '/v1/timekeeping/accruals/updates'
                    request.apiWrap = {"accrualUpdates":[]}
                    request.apiBatchQty = 5
                    request.apiThreads = 5
                    request.api = 
                    {
                        "accrualCode": {
                            "qualifier": data.accrualCode
                        },
                        "amount": data.amount,
                        //"amountType": "AMOUNTTYPE",
                        "effectiveDate": dateAdjust(data.effectiveDate),
                        "employee": {
                            "qualifier": data.employee
                        }
                    }
                }
            
                return request
            },
            data : [
                ['Update',"20326","2020-02-06",'Vacation Leave','10','9.5']
            ],
            descHTML: `
            <p>This import is capable of performing accrual balance resets and updates.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                  <th>Optional</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
             <tr>
                <th scope="row">Transaction Type</th><td>Text</td><td>Reset,Update</td><td></td></tr>   
                <tr><th>Person Number</th><td>Text</td><td>12345</td><td></td></tr>
                <tr><th>Effective Date</th><td>Date</td><td>2020-01-01</td><td></td></tr>
                <tr><th>Accrual Code</th><td>Text</td><td>12345</td><td></td></tr>
                <tr><th>Amount</th><td>Decimal</td><td>8.5</td><td>Y</td></tr>
                <tr><th>Probation Amount</th><td>Decimal</td><td>8.5</td><td>Y</td></tr>
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            <p>* Amount is required when doing an Accrual Update.</p>
            <p>* You must provide either Amount or Probation amount, but you cannot provide both </p>
            <p>* Accrual Codes are validated against WFD.</p>
            `
        },
        {
            type_name: 'Person',
            //visible: true,
            apiType: 'empData',
            allowableErrors: [{
                errorCode: 'WCO-101520',
                errorColor: 'blue',
                errorMessage: 'The employee has already been loaded'
            }],
            'cdata': 
            [ 
                //Basic Information 
                    { visible: true, data: 'Person Number*', name: 'personNum' },
                    { visible: true, data: 'First Name', name: 'firstName' },
                    { visible: true, data: 'Last Name*', name: 'lastName' },
                    { visible: true, data: 'Short Name', name: 'shortName' },
                    { visible: true, data: 'Middle Initial', name: 'middleInitial' },
                    { visible: true, data: 'Hire Date', name: 'hireDate' },
                    { visible: true, data: 'Employment Status', name: 'empStatus', datasource: ['Active','Inactive','Terminated'] },
                    { visible: true, data: 'Employment Status Eff Date', name: 'empStatusEffDate'},
                //USER INFORMATION 
                    { visible: true, data: 'User Account Status', name: 'userStatus', datasource: ['Active','Inactive','Terminated'] },
                    { visible: true, data: 'User Account Status Eff Date', name: 'userStatusEffDate'},
                    { visible: true, data: 'Authentication Type', name: 'authenticationTypeName', datasource: ['Basic','Federated'] },
                    { visible: true, data: 'Username', name: 'userName' },
                    { visible: true, data: 'Password', name: 'userPassword' },
                    { visible: true, data: 'Logon Profile', name: 'logonProfile' },
                    { visible: true, data: 'MFA Required?', name: 'mfaRequired', datasource: ['true','false'] },
                //Personal 
                    { visible: true, data: 'Analytics Type', name: 'analyticsLaborTypeName',datasource:['Full Time','Part Time','Per Diem','Pool'] },
                    { visible: true, data: 'Analytics Type Eff Date', name: 'analyticsLaborTypeEffDate' },
                    { visible: true, data: 'Phone Label', name: 'phoneLabel' },
                    { visible: true, data: 'Phone Number', name: 'phoneNumber' },
                    { visible: true, data: 'Phone SMS?', name: 'isSMSSwitch', datasource: ['true','false']},
                    { visible: true, data: 'State', name: 'state' },
                    { visible: true, data: 'City', name: 'city' },
                    { visible: true, data: 'Zip Code', name: 'postalCode' },
                    { visible: true, data: 'Country', name: 'country' },
                    { visible: true, data: 'Worker Type', name: 'workerType' },
                    { visible: true, data: 'Email', name: 'email' },
                //Scheduling Information 
                    { visible: true, data: 'Expected Daily Hours', name: 'dailyHours' },
                    { visible: true, data: 'Expected Weekly Hours', name: 'weeklyHours' },
                    { visible: true, data: 'Expected PP Hours', name: 'payperiodHours' },
                    { visible: true, data: 'Schedule Group', name: 'scheduleGroup' },
                //Common Profiles 
                    { visible: true, data: 'Display Profile', name: 'preferenceProfileName', datasource: { apiurl: '/v1/commons/display_profiles', tag: 'name' } },
                    { visible: true, data: 'Function Access Profile', name: 'accessProfileName', datasource: { apiurl: '/v1/commons/function_access_profiles', tag: 'name' } },
                    { visible: true, data: 'Time Zone', name: 'timezone', datasource: ['Afghanistan', 'Alaskan', 'Almaty', 'Arabian', 'Arizona', 'Atlantic', 'AUS_Central', 'Azores', 'Baghdad', 'Baku', 'Bangkok', 'Brisbane', 'Canada_Central', 'Cape_Verde_Islands', 'Central', 'Central_America', 'Central_Asia', 'Central_Australia', 'Central_Pacific', 'Chihuahua', 'China', 'Czech', 'E._Europe', 'E._South_America', 'Eastern', 'Egypt', 'Ekaterinburg', 'FIJI', 'GFT', 'GMT', 'Greenland', 'Greenwich', 'Hawaiian', 'Helsinki', 'India', 'Indiana(East)', 'Iran', 'Irkutsk', 'Israel', 'Kathmandu', 'Krasnoyarsk', 'Kuala_Lumpur', 'Lisbon_Warsaw', 'Lord_Howe_Island', 'Mexico', 'Mid-Atlantic', 'Mountain', 'Nairobi', 'New_Zealand', 'Newfoundland', 'Pacific', 'Perth', 'Rangoon', 'Romance', 'Russian', 'SA_Eastern', 'SA_Pacific', 'SA_Western', 'Samoa', 'Santiago', 'Saudi_Arabia', 'Seoul', 'South_Africa', 'Sri_Jayawardenepura', 'Sydney', 'Taipei', 'Tasmania', 'Tokyo', 'UTC', 'Vladivostok', 'W._Europe', 'West_Asia', 'West_Central_Africa', 'West_Pacific', 'Yakutsk'] },
                    { visible: true, data: 'Locale', name: 'localePolicyName' },
                    { visible: true, data: 'Notification Profile', name: 'notProfileName' },
                //Timekeeping Information 
                    { visible: true, data: 'Pay Rule*', name: 'payRule', datasource: { apiurl: '/v1/timekeeping/setup/payrules', tag: 'name' } },
                    { visible: true, data: 'Pay Rule Eff Date', name: 'payRuleEffectiveDate' },
                    { visible: true, data: 'Primary Job*', name: 'primaryJob' },
                    { visible: true, data: 'Labor Category', name: 'laborCategory' },
                    { visible: true, data: 'Primary Labor Eff Date', name: 'primlabEffDate' },
                    { visible: true, data: 'Employment Term', name: 'employmentTerm', datasource: { apiurl: '/v1/timekeeping/setup/employment_terms', tag: 'name' } },
                    { visible: true, data: 'Employment Eff Date', name: 'empTermstartDate' },
                    { visible: true, data: 'Accrual Profile', name: 'accrualProfile', datasource: { apiurl: '/v1/timekeeping/setup/accrual_profiles', tag: 'name' } },
                    { visible: true, data: 'Accrual Profile Eff Date', name: 'accrualProfileEffDate' },
                    { visible: true, data: 'Full Time %', name: 'fullTimePct' },
                    { visible: true, data: 'Full Time Hours', name: 'fullTimeHours' },
                    { visible: true, data: 'Full Time Employee Hours', name: 'employeeHours' },
                    { visible: true, data: 'Full Time Eq. Eff Date', name: 'fteEffDate' },
                    { visible: true, data: 'Wage Rate', name: 'wageRate' },
                    { visible: true, data: 'Wage Eff Date', name: 'wageRateEffDate' },
                    { visible: true, data: 'Wage Currency', name: 'employeeCurrency'  },
                    { visible: true, data: 'Reports To ID', name: 'supervisorID' },
                    { visible: true, data: 'Device Group', name: 'deviceGroupName', datasource: { apiurl: '/v1/commons/device_groups', tag: 'name' } },
                    { visible: true, data: 'Badge Number', name: 'badgeNumber' },
                    { visible: true, data: 'Badge Number Eff Date', name: 'badgeNumberEffDate' },
                //Work Items 
                    { visible: true, data: 'Work Profile Name', name: 'workProfileName' },
                    { visible: true, data: 'Default Activity', name: 'defaultActivityName' },
                    { visible: true, data: 'Idle Activity', name: 'idleActivityName' },
                    { visible: true, data: 'Paid Activity', name: 'paidActivityName' },
                    { visible: true, data: 'Unpaid Activity', name: 'unpaidActivityName' },
                //Access
                    { visible: true, data: 'Time Entry Method', name: 'timeEntryTypeName', datasource: ['Time Stamp & Hourly View', 'Time Stamp', 'Hourly View'] },
                    { visible: true, data: 'Time Entry Eff Date', name: 'timeEntryTypeEffectiveDate' },
                    { visible: true, data: 'Employee Pay Code Profile', name: 'professionalPayCodeName' },
                    { visible: true, data: 'Employee Work Rule Profile', name: 'professionalWorkRuleName' },
                    { visible: true, data: 'Employee Labor Category Profile', name: 'employeeLaborCategoryProfileName' },
                    { visible: true, data: 'Labor Category Manager Additions', name: 'mgrEmplLaborCategoryProfileName' },                
                    { visible: true, data: 'GDAP', name: 'gdapName' },
                    { visible: true, data: 'GDAP Eff Date', name: 'gdapEffDt' },
                    { visible: true, data: 'Person Access Eff Date', name: 'empMgrProfEffDate' },
                    { visible: true, data: 'PA - Employee JTS', name: 'professionalTransferOrganizationSetName' },
                    { visible: true, data: 'PA - JTS Manager Additions', name: 'empMgrTransferOrganizationSetName' },
                    { visible: true, data: 'PA - Organisational Set', name: 'organisationalSet', datasource: { apiurl: '/v1/commons/location_sets/multi_read', tag: 'name', pdata: { "where": { "allDetails": false, "context": "ORG", "date": "1900-01-01", "types": { "ids": [1, 2, 3] } } } } },
                    { visible: true, data: 'PA - Employee Group', name: 'employeeGroup', datasource: { apiurl: '/v1/commons/employee_groups', tag: 'name' } },
                    { visible: true, data: 'PA - Home Hyperfind', name: 'homeHyperFindQueryName' },
                    { visible: true, data: 'Manager Work Rule', name: 'managerWorkRule' },
                    { visible: true, data: 'Manager Pay Code Edit', name: 'managerPCE', datasource: { apiurl: '/v1/timekeeping/setup/pay_codes/data_access_profiles', tag: 'name' } },
                    { visible: true, data: 'Manager Pay Code View', name: 'managerPCV', datasource: { apiurl: '/v1/timekeeping/setup/pay_codes/data_access_profiles', tag: 'name' } },
                    { visible: true, data: 'Manager Labor Category Profile', name: 'managerLCP', datasource: { apiurl: '/v1/commons/labor_category_profiles', tag: 'name' } },
                    { visible: true, data: 'Pattern Template Profile', name: 'schedulePatternName' },
                    { visible: true, data: 'Shift Template Profile', name: 'shiftCodeName' },
                    { visible: true, data: 'Schedule Group Profile', name: 'groupScheduleName' },
                    { visible: true, data: 'Report Profile', name: 'reportName' },
                    { visible: true, data: 'Forecasting Category Profile', name: 'forecastingCategoryProfileName' },
                    { visible: true, data: 'Delegate Profile', name: 'delegateProfileName' },
                    { visible: true, data: 'Manager Currency Preference', name: 'currencyCode' }, //IS THIS ACTUALLY MANAGER
                //LICENSES 
                    { visible: true, data: 'Timekeeping License', name: 'TimekeepingLicense', datasource: ['Hourly Timekeeping','Salaried Timekeeping'] },
                    { visible: true, data: 'Scheduling License', name: 'SchedulingLicense', datasource: ['Scheduling','Advanced Scheduling','Advanced Planning and Scheduling','TeleStaff','Optimized Scheduling with Forecasting'] },
                    { visible: true, data: 'WAM License', name: 'WAMLicense', datasource: ['Accruals & Leave','Absence','Accruals'] },
                    { visible: true, data: 'Analytics License', name: 'AnalyticsLicense', datasource: ['Analytics','Analytics & Healthcare Analytics'] },
                    { visible: true, data: 'Work License', name: 'WorkLicense', datasource: ['Work']},
                    { visible: true, data: 'Employee License', name: 'EmployeeLicense', datasource:['Employee']},
                    { visible: true, data: 'Manager License', name: 'ManagerLicense', datasource: ['Manager'] },
                //CUSTOM DATA AND DATES 
                    { visible: true, data: 'Custom Data Names', name: 'cdataNames' },
                    { visible: true, data: 'Custom Data Values', name: 'cdataValues' },
                    { visible: true, data: 'Custom Date Names', name: 'cdateNames' },
                    { visible: true, data: 'Custom Date Values', name: 'cdateValues' },
                //OVERRIDE
                    { visible: true, data: 'Override Effective Date', name: 'masterEffDate'}    
            ],

            apiMap: function(data) {
                request = {}
                request.apiUrl = '/v1/commons/persons/multi_upsert'
                request.apiWrap = []
                request.apiBatchQty = 5
                request.apiThreads = 5

                //LICENSES
                if (!data.TimekeepingLicense) { data.TimekeepingLicense = 'Hourly Timekeeping' }
                var LicenseMap = []
                if (CheckEmpty(data.TimekeepingLicense) == true){LicenseMap.push(data.TimekeepingLicense)}
                if (CheckEmpty(data.SchedulingLicense) == true){LicenseMap.push(data.SchedulingLicense)}
                if (CheckEmpty(data.WAMLicense) == true){LicenseMap.push(data.WAMLicense)}
                if (CheckEmpty(data.AnalyticsLicense) == true){LicenseMap.push(data.AnalyticsLicense)}
                if (CheckEmpty(data.WorkLicense) == true){LicenseMap.push(data.WorkLicense)}
                if (CheckEmpty(data.EmployeeLicense) == true){LicenseMap.push(data.EmployeeLicense)}
                if (CheckEmpty(data.ManagerLicense) == true){LicenseMap.push(data.ManagerLicense)}

                LicenseMap = LicenseMap
                    .filter(function (e) {
                        return e;
                    })
                    .map(function (y) {
                        return {
                            "activeFlag": true,
                            "licenseTypeName": y
                        }
                    })

                //CUSTOM DATA
                cdata = []
                if (data.cdataNames) {
                    if (data.cdataNames.indexOf('|') > - 1 && data.cdataValues.indexOf('|') > - 1) {
                        names = data.cdataNames.split('|')
                        vals = data.cdataValues.split('|')
    
                        names.forEach((v,k) => {
                            cdata.push(
                                {
                                    "customDataTypeName": v,
                                    "text": vals[k]
                                }
                            )
                        });
    
                    }
                }

                 //CUSTOM DATES
                 cdates = []
                 if (data.cdateNames) {
                    if (data.cdateNames.indexOf('|') > - 1 && data.cdateValues.indexOf('|') > - 1) {
                        names = data.cdateNames.split('|')
                        vals = data.cdateValues.split('|')
    
                        names.forEach((v,k) => {
                           cdates.push(
                                {
                                    "customDateTypeName": v,
                                    "text": vals[k]
                                }
                            )
                        });
                    }
                }               

                function piDateLogic(data,key) {
                    if (data.masterEffDate) return dateAdjust(data.masterEffDate)
                    else if (data[key]) return dateAdjust(data[key])
                    else return dateAdjust(data.hireDate)
                }

                function piCurrentDate(date) {
                    if (date) return dateAdjust(date)
                    else return new Date().toISOString().split('T')[0]
                }

                function stringToBool(v) {
                    if (v.toUpperCase() == 'TRUE') return true
                    else return false
                }

                x = [{
                    "personIdentity": {
                        "personNumber": data.personNum
                    },
                    "gdapAssignments": [{
                        gdapName: data.gdapName,
                        defaultSwitch:true,
                        role: "MANAGER_ROLE",
                        effectiveDate: piCurrentDate(data.gdapEffDt),
                        expirationDate: "3000-01-01"
                    }],
                    "jobAssignment": {
                        "scheduleGroupName": data.scheduleGroup,
                        "jobAssignmentDetails": {
                            "payRuleEffectiveDate": piDateLogic(data,'payRuleEffectiveDate'),
                            "payRuleName": data.payRule,
                            "timeZoneName": data.timezone,
                            "deviceGroupName": data.deviceGroupName,
                            "workerTypeName": data.workerType,
                            "supervisorPersonNumber": data.supervisorID

                        },
                        /*
                        "employmentTermAssignments": [{
                            "startDate": piDateLogic(data,'empTermstartDate'), "endDate": "3000-01-01",
                            "name": data.employmentTerm

                        }],
                        */
                        "baseWageRates": [{
                            "effectiveDate": piDateLogic(data,'wageRateEffDate'), "expirationDate": "3000-01-01",
                            "hourlyRate": parseInt(data.wageRate, 10)
                        }],

                        "primaryLaborAccounts": [{
                            "effectiveDate": piDateLogic(data,'primlabEffDate'), "expirationDate": "3000-01-01",
                            "organizationPath": data.primaryJob,
                            "laborCategoryName": data.laborCategory
                        }]
                    },
                    "personInformation": {
                        "employeeCurrencyAssignment": { "currencyCode": data.employeeCurrency },


                        "telephoneNumbers": [
                            {
                                "contactTypeName": data.phoneLabel, "isSMSSwitch": stringToBool(data.isSMSSwitch), "phoneNumber": data.phoneNumber
                            }

                        ],
                        "personAuthenticationTypes": [{ "authenticationTypeName": data.authenticationTypeName, "activeFlag": true }],
                        "customDateList": cdates,
                        "postalAddresses": [
                            {
                                city: data.city, 
                                contactTypeName: "Home",
                                state: data.state, 
                                postalCode: data.postalCode,
                                country: data.country
                            }
                        ],
                        "expectedHoursList": [
                            {
                                "timePeriodTypeName": "WEEKLY", "quantity": data.weeklyHours
                            },
                            {
                                "timePeriodTypeName": "DAILY", "quantity": data.dailyHours
                            }
                            /*
                            {
                                "timePeriodTypeName": "PAY_PERIOD", "quantity": data.payperiodHours
                            }
                            */

                        ],
                        "emailAddresses": [
                            {
                                "contactTypeName": "Work", "address": data.email
                            }
                        ],
                        "customDataList": cdata,
                        "accessAssignment": {
                            "accessProfileName": data.accessProfileName,
                            "timeEntryTypeName": data.timeEntryTypeName,
                            //"timeEntryTypeEffectiveDate": new Date().toISOString().split('T')[0],
                            "timeEntryTypeEffectiveDate": piDateLogic(data,'timeEntryTypeEffectiveDate'),
                            "preferenceProfileName": data.preferenceProfileName,
                            "notificationProfileName": data.notProfileName,
                            "localePolicyName": data.localePolicyName,
                            "managerWorkRuleName": data.managerWorkRule,
                            "managerViewPayCodeName": data.managerPCV,
                            "managerPayCodeName": data.managerPCE,
                            "managerLaborCategoryProfileName": data.managerLCP,
                            "professionalPayCodeName": data.employeePCE,
                            "professionalWorkRuleName": data.professionalWorkRule,
                            "employeeLaborCategoryProfileName": data.employeeLCP,
                            "schedulePatternName": data.schedulePatternName,
                            "shiftCodeName": data.shiftCodeName,
                            "reportName": data.reportName,
                            "groupScheduleName": data.groupScheduleName,
                            "forecastingCategoryProfileName": data.forecastingCategoryProfileName,
                            "mgrEmplLaborCategoryProfileName": data.mgrEmplLaborCategoryProfileName,
                            "delegateProfileName": data.delegateProfileName

                        },
                        "personAccessAssignments": [{
                            "effectiveDate": piDateLogic(data,'empMgrProfEffDate'), "expirationDate": "3000-01-01",
                            "managerEmployeeGroupName": data.employeeGroup,
                            "managerTransferOrganizationSetName": data.organisationalSet,
                            "empMgrTransferOrganizationSetName": data.empMgrTransferOrganizationSetName,
                            "professionalTransferOrganizationSetName": data.professionalTransferOrganizationSetName,
                            "homeHyperFindQueryName": data.homeHyperFindQueryName
                        }],
                        "analyticsLaborTypeList": [
                            {
                              "analyticsLaborTypeName": data.analyticsLaborTypeName,
                              "effectiveDate": data.analyticsLaborTypeEffDate
                            }
                          ],
                        "person": {
                            "personNumber": data.personNum,
                            "lastName": data.lastName,
                            "hireDate": dateAdjust(data.hireDate),
                            "middleInitial": data.middleInitial,
                            "shortName": data.shortName,
                            "birthDate": data.birthDate,
                            "firstName": data.firstName,
                            "accrualProfileName": data.accrualProfile,
                            "accrualProfileEffectiveDate": piDateLogic(data,'accrualProfileEffDate'),
                            "fullTimeEquivalencies": [{
                                "effectiveDate": piDateLogic(data,'fteEffDate'), "expirationDate": "3000-01-01",
                                "employeeStandardHours": data.employeeHours,
                                "fullTimeStandardHours": data.fullTimeHours,
                                "fullTimePercentage": parseInt(data.fullTimePct, 10)
                            }]
                        },
                        "personLicenseTypes": LicenseMap,
                        "userAccountStatusList": [{
                            "effectiveDate": piDateLogic(data,'userStatusEffDate'),
                            "userAccountStatusName": data.userStatus
                        }],
                        "employmentStatusList": [{
                            "effectiveDate": piDateLogic(data,'empStatusEffDate'), "expirationDate": "3000-01-01",
                            "employmentStatusName": data.empStatus

                        }],
                        "badgeAssignments": [{
                            "badgeNumber": data.badgeNumber,
                            "effectiveDate": piCurrentDate(data.badgeNumberEffDate)
                        }],
                        "analyticsLaborTypeList": [
                            {
                                "analyticsLaborTypeName": data.analyticsLaborTypeName,
                                "effectiveDate": piDateLogic(data,'analyticsLaborTypeEffDate'), "expirationDate": "3000-01-01"
                            }
                        ],
                        "workEmployee": {
                            "defaultActivityName": data.defaultActivityName,
                            "idleActivityName": data.idleActivityName,
                            "paidActivityName": data.paidActivityName,
                            "profileName": data.workProfileName,
                            "unpaidActivityName": data.unpaidActivityName
                        }
                    },

                    "user": {
                        "userAccount": {
                            "logonProfileName": data.logonProfileName,
                            "userName": data.userName,
                            "userPassword": data.userPassword,
                            "mfaRequired": stringToBool(data.mfaRequired)
                        },
                        "userCurrency": { "currencyCode": data.currencyCode }
                    }
                }]

                var today = new Date();
                var todayDate = today.toISOString().substr(0, 10)
                
                function CheckEmpty(param) {
                    //console.log(param)
                    if (param == '' || param == null || param == undefined) {;return false }
                    else return true
                }
                function RemoveNullFromArray(array) {
                    return array.filter(function(e) {
                        return e;
                    });
                    }

                //GDAP
                if (CheckEmpty(data.gdapName) == false) { delete x[0].gdapAssignments}
                //SCHEDULEGROUP
                if (CheckEmpty(data.scheduleGroup) == false) { delete x[0].jobAssignment.scheduleGroupName}
                //JOBASSIGNMENTDETAILS
                if (CheckEmpty(data.payRule) == false) { delete x[0].jobAssignment.jobAssignmentDetails.payRuleName; delete x[0].jobAssignment.jobAssignmentDetails.payRuleEffectiveDate  }
                if (CheckEmpty(data.timeZoneName) == false) { delete x[0].jobAssignment.jobAssignmentDetails.timeZoneName}
                if (CheckEmpty(data.deviceGroupName) == false) { delete x[0].jobAssignment.jobAssignmentDetails.deviceGroupName}
                if (CheckEmpty(data.workerType) == false) { delete x[0].jobAssignment.jobAssignmentDetails.workerTypeName}
                if (CheckEmpty(data.supervisorPersonNumber) == false) { delete x[0].jobAssignment.jobAssignmentDetails.supervisorPersonNumber}
                if (Object.keys(x[0].jobAssignment.jobAssignmentDetails).length == 0) { delete x[0].jobAssignment.jobAssignmentDetails }
                //ETERMS
                if (CheckEmpty(data.employmentTerm) == false) { delete x[0].jobAssignment.employmentTermAssignments }
                //BASEWAGERATES
                if (CheckEmpty(data.wageRate) == false) { delete x[0].jobAssignment.baseWageRates }
                //PRIMARYLABORACCTS
                if (CheckEmpty(data.primaryJob) == false) { delete x[0].jobAssignment.primaryLaborAccounts[0].organizationPath }
                if (CheckEmpty(data.laborCategory) == false) { delete x[0].jobAssignment.primaryLaborAccounts[0].laborCategoryName }
                if (Object.keys(x[0].jobAssignment.primaryLaborAccounts[0]).length == 2) { delete x[0].jobAssignment.primaryLaborAccounts }
                //EMPCURENCY
                if (CheckEmpty(data.employeeCurrency) == false) { delete x[0].personInformation.employeeCurrencyAssignment }
                //MOBILE
                if (CheckEmpty(data.phoneNumber) == false) { delete x[0].personInformation.telephoneNumbers }
                //telephoneNumbers
                if (CheckEmpty(data.authenticationTypeName) == false) { delete x[0].personInformation.personAuthenticationTypes }
                //CUSTOMDATE
                if (CheckEmpty(data.customDateTypeName) == false) { delete x[0].personInformation.customDateList }
                //POSTALADDRESS
                if (CheckEmpty(data.city) == false) { delete x[0].personInformation.postalAddresses[0].city }
                if (CheckEmpty(data.state) == false) { delete x[0].personInformation.postalAddresses[0].state }
                if (CheckEmpty(data.postalCode) == false) { delete x[0].personInformation.postalAddresses[0].postalCode }
                if (CheckEmpty(data.country) == false) { delete x[0].personInformation.postalAddresses[0].country }
                if (Object.keys(x[0].personInformation.postalAddresses[0]).length == 0) { delete x[0].personInformation.postalAddresses }
                //EXPECTEDHOURSLIST
                if (CheckEmpty(data.dailyHours) == false) { delete x[0].personInformation.expectedHoursList[1] }
                if (CheckEmpty(data.weeklyHours) == false) { delete x[0].personInformation.expectedHoursList[0] }
                if (CheckEmpty(data.payperiodHours) == false) { delete x[0].personInformation.expectedHoursList[2] }
                if (RemoveNullFromArray(x[0].personInformation.expectedHoursList).length === 0) { delete x[0].personInformation.expectedHoursList }
                //EMAIL
                if (CheckEmpty(data.email) == false) { delete x[0].personInformation.emailAddresses }
                //CUSTOMDATA
                if (cdata.length == 0) { delete x[0].personInformation.customDataList }
                //ACCESSASSIGNMENTS
                if (CheckEmpty(data.accessProfileName) == false) { delete x[0].personInformation.accessAssignment.accessProfileName }
                if (CheckEmpty(data.timeEntryTypeName) == false) { delete x[0].personInformation.accessAssignment.timeEntryTypeName; delete x[0].personInformation.accessAssignment.timeEntryTypeEffectiveDate }
                if (CheckEmpty(data.preferenceProfileName) == false) { delete x[0].personInformation.accessAssignment.preferenceProfileName }
                if (CheckEmpty(data.notProfileName) == false) { delete x[0].personInformation.accessAssignment.notificationProfileName }
                if (CheckEmpty(data.localePolicyName) == false) { delete x[0].personInformation.accessAssignment.localePolicyName }
                if (CheckEmpty(data.managerWorkRuleName) == false) { delete x[0].personInformation.accessAssignment.managerWorkRuleName }
                if (CheckEmpty(data.managerPCV) == false) { delete x[0].personInformation.accessAssignment.managerViewPayCodeName }
                if (CheckEmpty(data.employeePCE) == false) { delete x[0].personInformation.accessAssignment.professionalPayCodeName }
                if (CheckEmpty(data.managerLCP) == false) { delete x[0].personInformation.accessAssignment.managerLaborCategoryProfileName }
                if (CheckEmpty(data.employeePCE) == false) { delete x[0].personInformation.accessAssignment.professionalPayCodeName }
                if (CheckEmpty(data.professionalWorkRule) == false) { delete x[0].personInformation.accessAssignment.professionalWorkRuleName }
                if (CheckEmpty(data.employeeLCP) == false) { delete x[0].personInformation.accessAssignment.employeeLaborCategoryProfileName }
                if (CheckEmpty(data.schedulePatternName) == false) { delete x[0].personInformation.accessAssignment.schedulePatternName }
                if (CheckEmpty(data.shiftCodeName) == false) { delete x[0].personInformation.accessAssignment.shiftCodeName }
                if (CheckEmpty(data.reportName) == false) { delete x[0].personInformation.accessAssignment.reportName }
                if (CheckEmpty(data.groupScheduleName) == false) { delete x[0].personInformation.accessAssignment.groupScheduleName }
                if (CheckEmpty(data.forecastingCategoryProfileName) == false) { delete x[0].personInformation.accessAssignment.forecastingCategoryProfileName }
                if (CheckEmpty(data.mgrEmplLaborCategoryProfileName) == false) { delete x[0].personInformation.accessAssignment.mgrEmplLaborCategoryProfileName }
                if (CheckEmpty(data.delegateProfileName) == false) { delete x[0].personInformation.accessAssignment.delegateProfileName }
                if (Object.keys(x[0].personInformation.accessAssignment).length == 0) delete x[0].personInformation.accessAssignment
                //PERSONACCESSASSIGNMENTS
                if (CheckEmpty(data.employeeGroup) == false) { delete x[0].personInformation.personAccessAssignments[0].managerEmployeeGroupName }
                if (CheckEmpty(data.organisationalSet) == false) { delete x[0].personInformation.personAccessAssignments[0].managerTransferOrganizationSetName }
                if (CheckEmpty(data.empMgrTransferOrganizationSetName) == false) { delete x[0].personInformation.personAccessAssignments[0].empMgrTransferOrganizationSetName }
                if (CheckEmpty(data.professionalTransferOrganizationSetName) == false) { delete x[0].personInformation.personAccessAssignments[0].professionalTransferOrganizationSetName }
                if (CheckEmpty(data.homeHyperFindQueryName) == false) { delete x[0].personInformation.personAccessAssignments[0].homeHyperFindQueryName }
                if (Object.keys(x[0].personInformation.personAccessAssignments[0]).length == 2) delete x[0].personInformation.personAccessAssignments
                //WFAN TYPE
                if (CheckEmpty(data.analyticsLaborTypeName) == false) { delete x[0].personInformation.analyticsLaborTypeList}                        
                //PERSONDATA
                if (CheckEmpty(data.personNum) == false) { delete x[0].personInformation.person.personNumber }
                if (CheckEmpty(data.lastName) == false) { delete x[0].personInformation.person.lastName }
                if (CheckEmpty(data.middleInitial) == false) { delete x[0].personInformation.person.middleInitial }
                if (CheckEmpty(data.shortName) == false) { delete x[0].personInformation.person.shortName }
                if (CheckEmpty(data.birthDate) == false) { delete x[0].personInformation.person.birthDate }
                if (CheckEmpty(data.firstName) == false) { delete x[0].personInformation.person.firstName }
                if (CheckEmpty(data.accrualProfile) == false) { delete x[0].personInformation.person.accrualProfile; delete x[0].personInformation.person.accrualProfileEffectiveDate }
                if (CheckEmpty(data.employeeHours) == false) { delete x[0].personInformation.person.fullTimeEquivalencies[0].employeeStandardHours }
                if (CheckEmpty(data.fullTimeHours) == false) { delete x[0].personInformation.person.fullTimeEquivalencies[0].fullTimeStandardHours }
                if (CheckEmpty(data.fullTimePct) == false) { delete x[0].personInformation.person.fullTimeEquivalencies[0].fullTimePercentage }
                if (Object.keys(x[0].personInformation.person.fullTimeEquivalencies[0]).length == 2) delete x[0].personInformation.person.fullTimeEquivalencies
                if (Object.keys(x[0].personInformation.person).length === 0) delete x[0].personInformation.person
                //USERACCOUNTSTATUS
                if (CheckEmpty(data.userName) == false) { delete x[0].personInformation.userAccountStatusList }
                //BADGES
                if (CheckEmpty(data.badgeNumber) == false) { delete x[0].personInformation.badgeAssignments }
                //ANALYTICS TYPE
                if (CheckEmpty(data.analyticsLaborTypeName) == false) { delete x[0].personInformation.analyticsLaborTypeList }
                //WORKDATA
                if (CheckEmpty(data.defaultActivityName) == false) { delete x[0].personInformation.workEmployee.defaultActivityName }
                if (CheckEmpty(data.idleActivityName) == false) { delete x[0].personInformation.workEmployee.idleActivityName }
                if (CheckEmpty(data.paidActivityName) == false) { delete x[0].personInformation.workEmployee.paidActivityName }
                if (CheckEmpty(data.workProfileName) == false) { delete x[0].personInformation.workEmployee.profileName }
                if (CheckEmpty(data.unpaidActivityName) == false) { delete x[0].personInformation.workEmployee.unpaidActivityName }
                if (Object.keys(x[0].personInformation.workEmployee).length === 0) { delete x[0].personInformation.workEmployee }
                //USERDATA
                if (CheckEmpty(data.currencyCode) == false) { delete x[0].user.userCurrency }
                if (CheckEmpty(data.userName) == false) { delete x[0].user.userAccount.userName }
                if (CheckEmpty(data.userPassword) == false) { delete x[0].user.userAccount.userPassword }
                if (CheckEmpty(data.mfaRequired) == false) { delete x[0].user.userAccount.mfaRequired }
                if (CheckEmpty(data.logonProfileName) == false) { delete x[0].user.userAccount.logonProfileName }
                if (Object.keys(x[0].user.userAccount).length === 0) { delete x[0].user.userAccount }
                if (Object.keys(x[0].user).length === 0) delete x[0].user
  
                request.api =  x[0]
                return request
            },
            findExisting: 
            {
                setDataSource:(da,ds)=>{
                    empList = []
                    $(da).each(function(k,v){
                        console.log(v)
                        empList.push(v.personNum)
                    })

                    try {
                        ds.where.employees.values = empList
                    }
                    catch(e){}
                    return ds
                },
                datasource: {
                    type:'empData',
                    apiUrl: 'v1/commons/persons/base_persons/multi_read/',
                    pdata: {
                        "where": {
                            "employees": {
                                "key": "personnumber",
                                "values": []
                            },
                            "snapshotDate": new Date().toISOString().split('T')[0]
                        }
                    },
                    apiBatchOn: 'where.employees.values',
                    apiBatchQty: 2500
                },
                updateExists: function(o,existRes,calls,apiType) { 
                    //if (apiType == 'leaveCases') {
                        extMatch = false
                        $(existRes).each(function(k,res){
                            $(calls).each((k,call)=>{
                                if (res.personNumber == call.api.personIdentity.personNumber) {
                                    try {
                                        delete call.api.user.userAccount.userPassword
                                    }
                                    catch(e) {}
                                    
                                }
                                //call.api.leaveCase.id = res.id
                            })
                        })
                    //}
                    //console.log('o',o)
                    //console.log('existRes',existRes)
                    //console.log('calls',calls)
                    //console.log('apiType',apiType)
                }
            },
            data: [
                ['ABC123','FirstName','Last Name','Short Name','D','8/11/2020','Active','8/12/2020','Active','8/13/2020','Basic','ABC123','Password1!','Default','TRUE','Full Time','8/14/2020','Phone 1','3178888888','TRUE','IN','Indianapolis','46259','US','Full Time','abc123@kronos.com','8','40','80','Kyle Test','Super Access','Super Access','Eastern','American English','All','Full Time Bi-Weekly','8/15/2020','Corporate/IS','dfsdfasf,,Shift 1,','08/11/2020','Administrative Employees','8/17/2020','Basic Accruals','8/18/2020','50','40','40','8/19/2020','99.99','8/20/2020','USD','10031','All Devices','1238898','8/20/2020','Default Profile','ActivityNew23','ActivityNew23','ActivityNew23','ActivityNew23','Time Stamp & Hourly View','8/21/2020','Retail Pay Codes','All Work Rules','GTest1','GTest2','All Access','8/22/2020','8/23/2020','All Department Groups','All Department Groups','All Department Groups','All Department Groups','ABC','All Work Rules','All Pay Codes','Manufacturing Pay Codes','KAD Test','All Pattern Templates','All Shift Templates','All Schedule Groups','All Reports','All Forecast Categories','Retail Managers Delegate Profile','USD','Hourly Timekeeping','Scheduling','Accruals & Leave','Analytics','Work','Employee','Manager','Only Custom Data Field|Seniority','A|B','Rehire Date|Service Date','2020-01-01|2020-02-02','']
            ],
            descHTML: `
            <p>This imports personality data.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
              <tr><th scope="row">Person Number*</th><td>Text</td><td></td></tr>
              <tr><th>First Name</th><td>Text</td><td></td></tr>
              <tr><th>Last Name*</th><td>Text</td><td></td></tr>
              <tr><th>Short Name</th><td>Text</td><td></td></tr>
              <tr><th>Middle Initial</th><td>Text</td><td></td></tr>
              <tr><th>Hire Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Employment Status</th><td>Text</td><td></td></tr>
              <tr><th>Employment Status Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>User Account Status</th><td>Text</td><td></td></tr>
              <tr><th>User Account Status Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Authentication Type</th><td>Text</td><td></td></tr>
              <tr><th>Username</th><td>Text</td><td></td></tr>
              <tr><th>Password</th><td>Text</td><td></td></tr>
              <tr><th>Logon Profile</th><td>Text</td><td></td></tr>
              <tr><th>MFA Required?</th><td>Text</td><td></td></tr>
              <tr><th>Analytics Type</th><td>Text</td><td></td></tr>
              <tr><th>Analytics Type Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Phone Label</th><td>Text</td><td></td></tr>
              <tr><th>Phone Number</th><td>Text</td><td></td></tr>
              <tr><th>Phone SMS?</th><td>Text</td><td></td></tr>
              <tr><th>State</th><td>Text</td><td></td></tr>
              <tr><th>City</th><td>Text</td><td></td></tr>
              <tr><th>Zip Code</th><td>Text</td><td></td></tr>
              <tr><th>Country</th><td>Text</td><td></td></tr>
              <tr><th>Worker Type</th><td>Text</td><td></td></tr>
              <tr><th>Email</th><td>Text</td><td></td></tr>
              <tr><th>Expected Daily Hours</th><td>Text</td><td></td></tr>
              <tr><th>Expected Weekly Hours</th><td>Text</td><td></td></tr>
              <tr><th>Expected PP Hours</th><td>Text</td><td></td></tr>
              <tr><th>Schedule Group</th><td>Text</td><td></td></tr>
              <tr><th>Display Profile</th><td>Text</td><td></td></tr>
              <tr><th>Function Access Profile</th><td>Text</td><td></td></tr>
              <tr><th>Time Zone</th><td>Text</td><td></td></tr>
              <tr><th>Locale</th><td>Text</td><td></td></tr>
              <tr><th>Notification Profile</th><td>Text</td><td></td></tr>
              <tr><th>Pay Rule*</th><td>Text</td><td></td></tr>
              <tr><th>Pay Rule Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Primary Job*</th><td>Text</td><td></td></tr>
              <tr><th>Labor Category</th><td>Text</td><td></td></tr>
              <tr><th>Primary Labor Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Employment Term</th><td>Text</td><td></td></tr>
              <tr><th>Employment Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Accrual Profile</th><td>Text</td><td></td></tr>
              <tr><th>Accrual Profile Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Full Time %</th><td>Text</td><td></td></tr>
              <tr><th>Full Time Hours</th><td>Text</td><td></td></tr>
              <tr><th>Full Time Employee Hours</th><td>Text</td><td></td></tr>
              <tr><th>Full Time Eq. Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Wage Rate</th><td>Text</td><td></td></tr>
              <tr><th>Wage Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Wage Currency</th><td>Text</td><td></td></tr>
              <tr><th>Reports To ID</th><td>Text</td><td></td></tr>
              <tr><th>Device Group</th><td>Text</td><td></td></tr>
              <tr><th>Badge Number</th><td>Text</td><td></td></tr>
              <tr><th>Badge Number Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Work Profile Name</th><td>Text</td><td></td></tr>
              <tr><th>Default Activity</th><td>Text</td><td></td></tr>
              <tr><th>Idle Activity</th><td>Text</td><td></td></tr>
              <tr><th>Paid Activity</th><td>Text</td><td></td></tr>
              <tr><th>Unpaid Activity</th><td>Text</td><td></td></tr>
              <tr><th>Time Entry Method</th><td>Text</td><td></td></tr>
              <tr><th>Time Entry Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Employee Pay Code Profile</th><td>Text</td><td></td></tr>
              <tr><th>Employee Work Rule Profile</th><td>Text</td><td></td></tr>
              <tr><th>Employee Labor Category Profile</th><td>Text</td><td></td></tr>
              <tr><th>Labor Category Manager Additions</th><td>Text</td><td></td></tr>
              <tr><th>GDAP</th><td>Text</td><td></td></tr>
              <tr><th>GDAP Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Person Access Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>PA - Employee JTS</th><td>Text</td><td></td></tr>
              <tr><th>PA - JTS Manager Additions</th><td>Text</td><td></td></tr>
              <tr><th>PA - Organisational Set</th><td>Text</td><td></td></tr>
              <tr><th>PA - Employee Group</th><td>Text</td><td></td></tr>
              <tr><th>PA - Home Hyperfind</th><td>Text</td><td></td></tr>
              <tr><th>Manager Work Rule</th><td>Text</td><td></td></tr>
              <tr><th>Manager Pay Code Edit</th><td>Text</td><td></td></tr>
              <tr><th>Manager Pay Code View</th><td>Text</td><td></td></tr>
              <tr><th>Manager Labor Category Profile</th><td>Text</td><td></td></tr>
              <tr><th>Pattern Template Profile</th><td>Text</td><td></td></tr>
              <tr><th>Shift Template Profile</th><td>Text</td><td></td></tr>
              <tr><th>Schedule Group Profile</th><td>Text</td><td></td></tr>
              <tr><th>Report Profile</th><td>Text</td><td></td></tr>
              <tr><th>Forecasting Category Profile</th><td>Text</td><td></td></tr>
              <tr><th>Delegate Profile</th><td>Text</td><td></td></tr>
              <tr><th>Manager Currency Preference</th><td>Text</td><td></td></tr>
              <tr><th>Timekeeping License</th><td>Text</td><td></td></tr>
              <tr><th>Scheduling License</th><td>Text</td><td></td></tr>
              <tr><th>WAM License</th><td>Text</td><td></td></tr>
              <tr><th>Analytics License</th><td>Text</td><td></td></tr>
              <tr><th>Work License</th><td>Text</td><td></td></tr>
              <tr><th>Employee License</th><td>Text</td><td></td></tr>
              <tr><th>Manager License</th><td>Text</td><td></td></tr>
              <tr><th>Custom Data Names</th><td>Text</td><td>CName1|CName2|..</td></tr>
              <tr><th>Custom Data Values</th><td>Text</td><td>CValue1|CValue2|..</td></tr>
              <tr><th>Custom Date Names</th><td>Text</td><td>CDate1|Cdate2|...</td></tr>
              <tr><th>Custom Date Values</th><td>Text</td><td>CValue1|CValue2|..</td></tr>
              <tr><th>Override Effective Date</th><td>Date</td><td>2020-01-01</td></tr>
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            <p>* The following date logic will occur for all effective dated fields (Override Effective Date -> Actual Date Provided -> Hire Date).</p>
            `
		},
        { //ATTN BAL
            type_name: 'Attendance Balance',
            apiThreads: 3,
            apiBatchQty: 75,
            /*
            allowableErrors: [{
                errorCode: 'WTK-170000',
                errorMessage: 'The Cost Center has previously been loaded'
            }],
            */
            cdata: [
                {visible: true,data: 'Transaction Type',name: 'trans_type',datasource:['Reset','Update']},
                {visible: true,data: 'Person Number',name: 'employee'},
                {visible: true,data: 'Effective Date',name: 'effDt'},
                {visible: true,data: 'Balance Type',name: 'balanceDefinition',/*datasource: {apiurl: '/v1/timekeeping/setup/accrual_codes',tag: 'name'}*/},
                {visible: true,data: 'Amount',name: 'amount'}
            ],
            apiWrap: [],
            apiMap: function(data) {
                tmpAPIArr = []

                if (data.trans_type.toUpperCase() == 'RESET') {
                    attnBalReset = {}
                    attnBalReset.apiUrl = '/v1/attendance/balance_resets/multi_update'
                    attnBalReset.apiWrap = []
                    attnBalReset.api = 
                    {
                        "amount": data.amount,
                        "balanceDefinition": {"qualifier": data.balanceDefinition},
                        "employees": {
                          "qualifiers": [data.employee],
                        },
                        //"isManagerRole": false,
                        "resetDateTime": dateAdjust(data.effDt) + 'T00:00:00'
                      }
                    tmpAPIArr.push(attnBalReset)
                }
                else if (data.trans_type.toUpperCase() == 'UPDATE') {
                    attnBalUpdate = {}
                    attnBalUpdate.apiUrl = '/v1/attendance/balance_adjustments/multi_update'
                    attnBalUpdate.apiWrap = []
                    attnBalUpdate.api = 
                    {
                        "amount": data.amount,
                        "balanceDefinition": {
                          "qualifier": data.balanceDefinition
                        },
                        "employees": {
                          "qualifiers": [data.employee]
                        },
                        'updateDateTime':dateAdjust(data.effDt) + 'T00:00:00'
                    }
                    tmpAPIArr.push(attnBalUpdate)
                }
                
                return tmpAPIArr
            },
            data : [
                ["Reset","20326","2020-02-06",'Occurrences','10']
            ],
            descHTML: `
            <p>This import is capable of performing attendance balance resets and attendance balance updates.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                  <th>Optional</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
                <tr><th scope="row">Transaction Type</th><td>Text</td><td>Reset,Update</td><td></td></tr>
                <tr><th>Person Number</th><td>Text</td><td>12345</td><td></td></tr>
                <tr><th>Effective Date</th><td>Date</td><td>2020-01-01</td><td></td></tr>
                <tr><th>Balance Type</th><td>Text</td><td>12345</td><td></td></tr>
                <tr><th>Amount</th><td>Decimal</td><td>8.5</td><td>Y</td></tr>
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            `
        },
        { //ATTN EVENT 
            type_name: 'Attendance Event',
            apiUrl: '/v1/attendance/events/multi_create',
            apiThreads: 3,
            apiBatchQty: 75,
            /*
            allowableErrors: [{
                errorCode: 'WTK-170000',
                errorMessage: 'The Cost Center has previously been loaded'
            }],
            */
            cdata: [
                {visible: true,data: 'Person Number',name: 'employee'}, ///api//v1/attendance/events/types
                {visible: true,data: 'Event Date',name: 'applyDate'},
                {visible: true,data: 'Event Time',name: 'eventTime'},
                {visible: true,data: 'Event Name',name: 'eventName'},
                {visible: true,data: 'Event Type',name: 'eventType',datasource: {apiurl: '/v1/attendance/events/types',tag: 'name'}},
                {visible: true,data: 'Amount',name: 'amount'}
            ],
            apiWrap: [],
            apiMap: function(data) {
            var x = 
                {
                    "amount": data.amount * 3600.00,
                    //"name": "NAME",
                    //"userCurrency": "USERCURRENCY",
                    //"updatedByUserId": 1,
                    /*"timeZoneId": 1,
                    "ruleDefinition": {
                        "id": 1,
                        "qualifier": "QUALIFIER"
                    },
                    "policyDefinition": {
                        "detailLabel": "DETAILLABEL",
                        "id": 1,
                        "qualifier": "QUALIFIER"
                    },
                    "pointTransaction": {
                        "applyDateTime": "APPLYDATETIME",
                        "balanceChange": 1.1,
                        "decimalPlaceNum": 1.1,
                        "employee": {
                        "id": 1,
                        "qualifier": "QUALIFIER"
                        },
                        "id": 1,
                        "name": "NAME",
                        "type": {
                        "displayName": "DISPLAYNAME",
                        "name": "NAME"
                        }
                    },
                    "isSystem": false,
                    */
                    "applyDate": dateAdjust(data.applyDate),
                    /*
                    "incidentType": "INCIDENTTYPE",
                    "incidentDate": "2020-01-25",
                    "id": 1,
                    */
                    "eventTime": stringToTime(data.eventTime),
                    "eventDefinition": {
                        /*
                        "active": false,
                        "paycode": {
                        "id": 1,
                        "qualifier": "QUALIFIER"
                        },
                        "upperThresholdDayAmount": 1.1,
                        "upperThresholdAmount": 1,
                        */
                        "type": {
                            //"displayName": "DISPLAYNAME",
                            "name": data.eventType
                        },
                        /* --!!!!!
                        "shortName": "SHORTNAME",
                        */
                        "qualifier": data.eventName,
                        /*
                        "punchType": {
                        "id": 1,
                        "qualifier": "QUALIFIER"
                        /*
                        },
                        "paycodeAmountType": {
                        "id": 1,
                        "qualifier": "QUALIFIER"
                        },
                        "lowerThresholdDayAmount": 1.1,
                        "amountUnit": {
                        "id": 1,
                        "qualifier": "QUALIFIER"
                        },
                        "lowerThresholdAmount": 1,
                        "id": 1,
                        "group": {
                        "id": 1,
                        "qualifier": "QUALIFIER"
                        },
                        "extendedData": "EXTENDEDDATA",
                        "exceptionType": {
                        "id": 1,
                        "qualifier": "QUALIFIER"
                        },
                        "displayName": "DISPLAYNAME",
                        "dayBased": false,
                        "comment": {
                        "id": 1,
                        "text": "TEXT"
                        },
                        "usedType": {
                        "id": 1,
                        "qualifier": "QUALIFIER"
                        }
                        */
                    },
                    //"employeeCurrency": "EMPLOYEECURRENCY",
                    "employee": {
                        "qualifier": data.employee
                    },
                    //"dayAmount": 1.1,
                    //"wageLiability": 1.1
                }

                //else delete x.accrualReset.probationAmount
                return x
            },
            data : [
                ["20326",'2020-02-13','08:00',"Tardy Excused","BASIC",23.5]
            ],
            descHTML: `
            <p>This import is capable of importing attendance events.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                  <th>Optional</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
             <tr>
                <th scope="row">Person Number</th><td>Text</td><td>12345</td><td></td></tr>
                <tr><th>Event Date</th><td>Date</td><td>2020-01-01</td><td></td></tr>                
                <tr><th>Event Time</th><td>Time</td><td>08:00</td><td></td></tr>                
                <tr><th>Event Name</th><td>Text</td><td>Tardy Excused</td><td></td></tr>   
                <tr><th>Event Type</th><td>Text</td><td>Basic,Pattern,Combined,...</td><td></td></tr>   
                <tr><th>Amount</th><td>Decimal</td><td>8.5</td><td></td></tr>
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            <p>* Event Names and Types are validated against WFD.</p>
            `
        },
        { //LEAVE CASE
            type_name: 'Leave Case',
            apiUrl: '/v1/leave/leave_cases/multi_create',
            apiThreads: 4,
            apiBatchQty: 50,
            /*
            allowableErrors: [{
                errorCode: 'WTK-170000',
                errorMessage: 'The Cost Center has previously been loaded'
            }],
            */
            cdata: [
                {visible: true,data:'Person Number',name: 'pnum'},
                {visible: true,data:'Frequency Type',name: 'freq_type',datasource:['Continuous','Intermittent']},
                {visible: true,data:'Start Date',name: 'sdt'},
                {visible: true,data:'End Date',name: 'edt'},
                {visible: true,data:'Request Date',name: 'rdt'},
                {visible: true,data:'Case Code',name: 'case_code'},
                {visible: true,data:'Category Name',name: 'cat_name',datasource:{apiurl:'/v1/leave/setup/leave_categories',tag:'name'}},
                {visible: true,data:'Reason', name: 'reason',datasource:{apiurl:'/v1/leave/setup/leave_reasons',tag:'name'}},
                //{visible: true,data:'Leave Rule',name: 'leave_rule'},  //SEPERATE API //NEED API
                {visible: true,data:'Case Status',name: 'case_status',datasource:['Open','Closed']},
                {visible: true,data:'Case Approval Name',name: 'case_approval'},
                {visible: true,data:'External ID',name: 'extid'},
                
                //{visible: true,data:'User Defined Field',name: 'udf_name',datasource:{apiurl:'/v1/leave/custom_fields',tag:'name'}}, //SEPERATE API
                //{visible: true,data:'Note or User Defined Field Value',name: 'note_udf_val'}, //SEPERATE API
                //{visible: true,data:'Note Effective Date',name: 'note_effdt'} //SEPERATE API
                
            ],
            /*
            apiPreGroup: (tdata)=>{
                tmpfinalArr = []
                finalArr = []
                tdata.forEach((r)=>{
                    r = r.filter(function(value, index, arr){ return index <= 11;});
                    r = JSON.stringify(r)
                    if (tmpfinalArr.indexOf(r) == -1) tmpfinalArr.push(r)
                })
                tmpfinalArr.forEach((f)=>{finalArr.push(JSON.parse(f))})
                return finalArr
            },
            */
            apiWrap: [],
            apiMap: function(data) {
                tmpAPIArr = []

                if (data.freq_type == 'Continuous') data.freq_type = 1
                else data.freq_type = 2

                //LEAVE CASE
                var leaveCase = {}
                leaveCase.apiWrap = []
                leaveCase.removeDuplicates = true
                leaveCase.api = 
                {
                    "approvalStatus": {
                      //"id": 1,
                      //"name": "NAME",
                      "qualifier": data.case_approval
                    },
                    "frequency": {
                      "id": data.freq_type,
                      //"name": data.freq_type,
                      //"qualifier": data.freq_type
                    },
                    "startDate": dateAdjust(data.sdt),
                    "requestDate": dateAdjust(data.rdt),
                    "reason": {
                      //"id": 1,
                      //"name": "NAME",
                      "qualifier": data.reason
                    },
                    //"longCaseCode": "LONGCASECODE",
                    //"id": 1,
                    //"frequencyEffectiveDate": "2020-01-25",
                    "endDate": dateAdjust(data.edt),
                    "caseCode": data.case_code,
                    //"employeePhotoId": "EMPLOYEEPHOTOID",
                    //"employeeFullName": "EMPLOYEEFULLNAME",
                    "employee": {
                      //"fullName": "FULLNAME",
                      //"id": 1,
                      //"name": "NAME",
                      "qualifier": data.pnum
                    },
                    //"displayCaseCode": "DISPLAYCASECODE",
                    "code": data.extid, //EXTERNAL CODE
                    "category": {
                      //"id": 1,
                      //"name": "NAME",
                      "qualifier": data.cat_name
                    },
                    "status": {
                      //"id": 1,
                      //"name": "NAME",
                      "qualifier": data.case_status
                    }
                }

                if (!leaveCase.api.endDate) delete leaveCase.api.endDate

                tmpAPIArr.push(leaveCase)

                findLeaveId = (responses,data,api)=>{
                    var retID
                    responses.forEach((r)=>{
                        if (data.extid == r.id) retID = r.id
                        if (data.startDate = r.startDate && data.case_code == r.caseCode && data.pnum == r.employee.qualifier) retID = r.id
                    })
                    if (retID) { //1==0
                        if (api.hasOwnProperty('leaveCase')) api.leaveCase.id = retID
                        else api.id = retID
                        return api
                    }
                    else return
                }
            
                //LEAVE CASE - ASSIGN LEAVE RULE
                if (data.leave_rule) {
                    assignLeaveRule = {}
                    assignLeaveRule.apiUrl = '/v1/leave/leave_cases/case_rules'
                    assignLeaveRule.apiWrap = []
                    assignLeaveRule.apiBatchQty = 1
                    assignLeaveRule.apiThreads = 5
                    assignLeaveRule.updateKeyValue = findLeaveId
                    assignLeaveRule.api = 
                    {
                        "effectiveDate": dateAdjust(data.sdt),
                        "leaveCase": {
                          "id": "SHOULD BE UPDATED LATER",
                        },
                        "leaveRule": {
                          "qualifier": data.leave_rule
                        }                
                      }
                    //tmpAPIArr.push(assignLeaveRule)
                }

                //LEAVE CASE - USER DEFINED FIELD
                if (data.udf_name) {
                    leaveCaseCustomFields = {}
                    leaveCaseCustomFields.apiUrl = '/v1/leave/leave_cases/custom_fields/multi_update' //VERY CONFUSING.....not a multi api
                    //leaveCaseCustomFields.apiWrap = []
                    leaveCaseCustomFields.updateKeyValue = findLeaveId
                    leaveCaseCustomFields.apiBatchQty = 1
                    leaveCaseCustomFields.apiThreads = 5
                    leaveCaseCustomFields.api = 
                    {
                        "customFields": [
                            {
                                "label": data.udf_name,
                                "value": data.note_udf_val
                            }
                        ],
                        "leaveCase": {
                            "id": "SHOULD BE UPDATED LATER"
                        }
                    }
                    tmpAPIArr.push(leaveCaseCustomFields)
                }
                //LEAVE CASE NOTE
                else if (data.note_udf_val) {
                    leaveNotes = {}
                    leaveNotes.apiUrl = '/v1/leave/leave_cases/notes'
                    leaveNotes.updateKeyValue = findLeaveId
                    leaveNotes.apiBatchQty = 1
                    leaveNotes.apiThreads = 5
                    leaveNotes.api = 
                    {
                        "leaveCase": {
                            "id": "SHOULD BE UPDATED LATER"
                        },
                        "noteDate": dateAdjust(data.note_effdt),
                        "noteText": data.note_udf_val
                    }
                    if (!leaveNotes.api.noteDate) leaveNotes.api.noteDate = dateAdjust(data.sdt)
                    //tmpAPIArr.push(leaveNotes)
                }

                return tmpAPIArr
            },

            data : [
                ["20325","Continuous","1/20/2020","1/1/3000","1/20/2020","C100","Family","Spouse","Open","Pending","123"]
                //["20325","Continuous","1/20/2020","1/1/3000","1/20/2020","C100","Family","Spouse","Exigency","Open","Pending","123","","Note1",""],
                //["20325","Continuous","1/20/2020","1/1/3000","1/20/2020","C100","Family","Spouse","Exigency","Open","Pending","123","Physician name","A",""],
            ],
            descHTML: `
            <p>This import is capable of importing leave cases</p>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                  <th>Optional</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
                <tr><td scope="row">Person Number</td><td>Text</td><td>12345</td><td></td></tr>
                <tr><td>Frequency Type</td><td>Text</td><td>Continuous,Intermittent</td><td></td></tr>                   
                <tr><td>Start Date</td><td>Date</td><td>2020-01-01</td><td></td></tr>                   
                <tr><td>End Date</td><td>Date</td><td>2020-01-01</td><td></td></tr>                   
                <tr><td>Request Date</td><td>Date</td><td>2020-01-01</td><td></td></tr>                   
                <tr><td>Case Code</td><td>Text</td><td>C100</td><td></td></tr>
                <tr><td>Category Name</td><td>Text</td><td>Family</td><td></td></tr>
                <tr><td>Reason</td><td>Text</td><td>Spouse</td><td></td></tr>
                <tr><td>Case Status</td><td>Text</td><td>Open,Closed</td><td></td></tr>
                <tr><td>Case Approval Name</td><td>Text</td><td>Pending</td><td></td></tr>
                <tr><td>External ID</td><td>Number</td><td>456</td><td></td></tr>   
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            <p>* Frequency Type, Category Name, Reason, and Case Status are validated against WFD.</p>
            `
        },
        { 
            type_name: 'Leave Case Notes',
            apiType: 'leaveCases', //OPTIONAL only needed for update existing logic
            //apiUrl: '/v1/leave/leave_cases/notes',
            //apiThreads: 5,
            //apiBatchQty: 100,
            cdata: [
                {visible: true,data:'Person Number',name: 'pnum'},
                {visible: true,data:'Start Date',name: 'sdt'},
                {visible: true,data:'Case Code',name: 'case_code'},
                {visible: true,data:'External ID',name: 'extid'},
                {visible: true,data:'Note',name: 'note'}, //SEPERATE API
                {visible: true,data:'Note Effective Date',name: 'note_effdt'} //SEPERATE API
                
            ],
            apiMap: function(data) {  
                leaveNotes = {}
                leaveNotes.apiUrl = '/v1/leave/leave_cases/notes'
                leaveNotes.apiBatchQty = 1
                leaveNotes.apiThreads = 5
                leaveNotes.api = 
                {
                    "leaveCase": {
                        "id": "SHOULD BE UPDATED LATER"
                    },
                    "noteDate": dateAdjust(data.note_effdt),
                    "noteText": data.note
                }
                return leaveNotes    
            },
            findExisting: 
            {
                setDataSource:(da,ds)=>{
                    
                    da.forEach((d)=>{
                        d.__sdt = new Date(d.sdt)
                    })

                    let el = Enumerable.From(da)
                    .GroupBy("$.pnum", null,
                        function (key, g) {
                            return {
                            personnum: key,
                            min_effdt: g.Min("$.__sdt"),
                            max_effdt: g.Max("$.__sdt")
                        }
                    })
                    .ToArray();
    
                    let empList = []
                    $(el).each(function(k,v){empList.push(v.personnum)})
    
                    let mnmxEffDt = Enumerable.From(da)
                    .GroupBy("1", null,
                        function (key, g) {
                            return {
                            id: key,
                            mn_applydt: g.Min("$.__sdt"),
                            mx_applydt: g.Max("$.__sdt")
                        }
                    }).ToArray()
                    
                    try {
                        ds.dateRange.startDate = mnmxEffDt[0].mn_applydt.toISOString().split('T')[0]
                        ds.dateRange.endDate = mnmxEffDt[0].mx_applydt.toISOString().split('T')[0]
                        ds.employees.qualifiers = empList
                    }
                    catch(e){}
                    return ds
                },
                datasource: {
                    type:'leaveCases',
                    apiUrl: '/v1/leave/leave_cases/multi_read',
                    pdata: {
                        "dateRange": {
                          "endDate": "",
                          "startDate": "",
                        },
                        "employees": {
                          "qualifiers": [],
                        }
                    },
                    apiBatchOn: 'employees.qualifiers',
                    apiBatchQty: 100
                },
                updateExists: function(o,existRes,calls,apiType) { 
                    if (apiType == 'leaveCases') {
                        extMatch = false
                        $(existRes).each(function(k,res){
                            $(calls).each((k,call)=>{
                                if (res.id == call.tdata.extid) {
                                    extMatch = true
                                    call.api.leaveCase.id = res.id
                                }
                                else if (
                                    extMatch == false && 
                                    res.employee.qualifier == call.tdata.pnum &&
                                    res.startDate == dateAdjust(call.tdata.sdt) &&
                                    res.caseCode == call.tdata.case_code
                                ) call.api.leaveCase.id = res.id
                            })
                        })
                    }
                    //console.log('o',o)
                    //console.log('existRes',existRes)
                    //console.log('calls',calls)
                    //console.log('apiType',apiType)
                }
            },
            data : [
                ["20325","1/20/2020","C100","457",'Note','2020-03-10']
            ],
            descHTML: `
            <p>This import is capable of importing leave case nodes.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                  <th>Optional</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
                <tr><th scope="row">Person Number</th><td>Text</td><td>12345</td><td></td></tr>
                <tr><th>Start Date</th><td>Date</td><td>2020-01-01</td><td></td></tr>                   
                <tr><th>Case Code</th><td>Text</td><td>C100</td><td></td></tr>                   
                <tr><th>External ID</th><td>Number</td><td>456</td><td></td></tr>   
                <tr><th>Note</th><td>Text</td><td>This is a note.</td><td>Y</td></tr> 
                <tr><th>Note Effective Date</th><td>Date</td><td>2020-01-01</td><td></td></tr>                   
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            <p>* The case can be linked by either (Person Number, Start Date, and Case Code) or by specifying (Person Number and External ID).</p>
            `
        },
        { 
            type_name: 'Leave Case User Defined Fields',
            apiType: 'leaveCases', //OPTIONAL only needed for update existing logic
            //apiThreads: 5,
            //apiBatchQty: 100,
            cdata: [
                {visible: true,data:'Person Number',name: 'pnum'},
                {visible: true,data:'Start Date',name: 'sdt'},
                {visible: true,data:'Case Code',name: 'case_code'},
                {visible: true,data:'External ID',name: 'extid'},
                {visible: true,data:'User Defined Field',name: 'udf_name',datasource:{apiurl:'/v1/leave/custom_fields',tag:'name'}}, //SEPERATE API
                {visible: true,data:'User Defined Value',name: 'udf_val'} //SEPERATE API
                
            ],
            apiMap: function(data) {  
                leaveCaseCustomFields = {}
                leaveCaseCustomFields.apiUrl = '/v1/leave/leave_cases/custom_fields/multi_update' //VERY CONFUSING.....not a multi api
                leaveCaseCustomFields.apiBatchQty = 1
                leaveCaseCustomFields.apiThreads = 5
                leaveCaseCustomFields.api = 
                {
                    "customFields": [
                        {
                            "label": data.udf_name,
                            "value": data.udf_val
                        }
                    ],
                    "leaveCase": {
                        "id": "SHOULD BE UPDATED LATER"
                    }
                }
                return leaveCaseCustomFields
                
            },
            findExisting: 
            {
                setDataSource:(da,ds)=>{
                    
                    da.forEach((d)=>{
                        d.__sdt = new Date(d.sdt)
                    })

                    let el = Enumerable.From(da)
                    .GroupBy("$.pnum", null,
                        function (key, g) {
                            return {
                            personnum: key,
                            min_effdt: g.Min("$.__sdt"),
                            max_effdt: g.Max("$.__sdt")
                        }
                    })
                    .ToArray();
    
                    let empList = []
                    $(el).each(function(k,v){empList.push(v.personnum)})
    
                    let mnmxEffDt = Enumerable.From(da)
                    .GroupBy("1", null,
                        function (key, g) {
                            return {
                            id: key,
                            mn_applydt: g.Min("$.__sdt"),
                            mx_applydt: g.Max("$.__sdt")
                        }
                    }).ToArray()
                    
                    try {
                        ds.dateRange.startDate = mnmxEffDt[0].mn_applydt.toISOString().split('T')[0]
                        ds.dateRange.endDate = mnmxEffDt[0].mx_applydt.toISOString().split('T')[0]
                        ds.employees.qualifiers = empList
                    }
                    catch(e){}
                    return ds
                },
                datasource: {
                    type:'leaveCases',
                    apiUrl: '/v1/leave/leave_cases/multi_read',
                    pdata: {
                        "dateRange": {
                          "endDate": "",
                          "startDate": "",
                        },
                        "employees": {
                          "qualifiers": [],
                        }
                    },
                    apiBatchOn: 'employees.qualifiers',
                    apiBatchQty: 100
                },
                updateExists: function(o,existRes,calls,apiType) { 
                    if (apiType == 'leaveCases') {
                        extMatch = false
                        $(existRes).each(function(k,res){
                            $(calls).each((k,call)=>{
                                if (res.id == call.tdata.extid) {
                                    extMatch = true
                                    call.api.leaveCase.id = res.id
                                }
                                else if (
                                    extMatch == false && 
                                    res.employee.qualifier == call.tdata.pnum &&
                                    res.startDate == dateAdjust(call.tdata.sdt) &&
                                    res.caseCode == call.tdata.case_code
                                ) call.api.leaveCase.id = res.id
                            })
                        })
                    }
                    //console.log('o',o)
                    //console.log('existRes',existRes)
                    //console.log('calls',calls)
                    //console.log('apiType',apiType)
                }
            },
            data : [
                ["20325","1/20/2020","C100",'457','Physician name','ABC']
            ],
            descHTML: `
            <p>This import is capable of importing leave case user defined fields.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                  <th>Optional</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
                <tr><th scope="row">Person Number</th><td>Text</td><td>12345</td><td></td></tr>
                <tr><th>Start Date</th><td>Date</td><td>2020-01-01</td><td></td></tr>                   
                <tr><th>Case Code</th><td>Text</td><td>C100</td><td></td></tr>                   
                <tr><th>External ID</th><td>Number</td><td>456</td><td></td></tr>   
                <tr><th>User Defined Field</th><td>Text</td><td>This is a note.</td><td>Y</td></tr> 
                <tr><th>User Defined Value</th><td>Date</td><td>2020-01-01</td><td></td></tr>                   
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            <p>* The case can be linked by either (Person Number, Start Date, and Case Code) or by specifying (Person Number and External ID).</p>
            `
        },
        { 
            type_name: 'Leave Case Rule',
            apiType: 'leaveCases', //OPTIONAL only needed for update existing logic
            //apiUrl: '/v1/leave/leave_cases/notes',
            apiThreads: 5,
            apiBatchQty: 1,
            cdata: [
                {visible: true,data:'Person Number',name: 'pnum'},
                {visible: true,data:'Start Date',name: 'sdt'},
                {visible: true,data:'Case Code',name: 'case_code'},
                {visible: true,data:'External ID',name: 'extid'},
                {visible: true,data:'Leave Rule',name: 'leave_rule'}
            ],
            apiMap: function(data) {  
                assignLeaveRule = {}
                assignLeaveRule.apiUrl = '/v1/leave/leave_cases/case_rules'
                assignLeaveRule.apiWrap = []
                assignLeaveRule.apiBatchQty = 1
                assignLeaveRule.apiThreads = 5
                assignLeaveRule.api = 
                {
                    "effectiveDate": dateAdjust(data.sdt),
                    "leaveCase": {
                      "id": "SHOULD BE UPDATED LATER",
                    },
                    "leaveRule": {
                      "qualifier": data.leave_rule
                    }                
                  }
                return assignLeaveRule    
            },
            findExisting: 
            {
                setDataSource:(da,ds)=>{
                    
                    da.forEach((d)=>{
                        d.__sdt = new Date(d.sdt)
                    })

                    let el = Enumerable.From(da)
                    .GroupBy("$.pnum", null,
                        function (key, g) {
                            return {
                            personnum: key,
                            min_effdt: g.Min("$.__sdt"),
                            max_effdt: g.Max("$.__sdt")
                        }
                    })
                    .ToArray();
    
                    let empList = []
                    $(el).each(function(k,v){empList.push(v.personnum)})
    
                    let mnmxEffDt = Enumerable.From(da)
                    .GroupBy("1", null,
                        function (key, g) {
                            return {
                            id: key,
                            mn_applydt: g.Min("$.__sdt"),
                            mx_applydt: g.Max("$.__sdt")
                        }
                    }).ToArray()
                    
                    try {
                        ds.dateRange.startDate = mnmxEffDt[0].mn_applydt.toISOString().split('T')[0]
                        ds.dateRange.endDate = mnmxEffDt[0].mx_applydt.toISOString().split('T')[0]
                        ds.employees.qualifiers = empList
                    }
                    catch(e){}
                    return ds
                },
                datasource: {
                    type:'leaveCases',
                    apiUrl: '/v1/leave/leave_cases/multi_read',
                    pdata: {
                        "dateRange": {
                          "endDate": "",
                          "startDate": "",
                        },
                        "employees": {
                          "qualifiers": [],
                        }
                    },
                    apiBatchOn: 'employees.qualifiers',
                    apiBatchQty: 100
                },
                updateExists: function(o,existRes,calls,apiType) { 
                    if (apiType == 'leaveCases') {
                        extMatch = false
                        $(existRes).each(function(k,res){
                            $(calls).each((k,call)=>{
                                if (res.id == call.tdata.extid) {
                                    extMatch = true
                                    call.api.leaveCase.id = res.id
                                }
                                else if (
                                    extMatch == false && 
                                    res.employee.qualifier == call.tdata.pnum &&
                                    res.startDate == dateAdjust(call.tdata.sdt) &&
                                    res.caseCode == call.tdata.case_code
                                ) call.api.leaveCase.id = res.id
                            })
                        })
                    }
                    //console.log('o',o)
                    //console.log('existRes',existRes)
                    //console.log('calls',calls)
                    //console.log('apiType',apiType)
                }
            },
            data : [
                ["20325","1/20/2020","C100","457",'Leave Rule']
            ],
            descHTML: `
            <p>This import is capable of importing leave rule assignments.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                  <th>Optional</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
                <tr><th scope="row">Person Number</th><td>Text</td><td>12345</td><td></td></tr>
                <tr><th>Start Date</th><td>Date</td><td>2020-01-01</td><td></td></tr>                   
                <tr><th>Case Code</th><td>Text</td><td>C100</td><td></td></tr>                   
                <tr><th>External ID</th><td>Number</td><td>456</td><td></td></tr>   
                <tr><th>Leave Rule</th><td>Text</td><td>A valid leave rule in WFD</td><td>Y</td></tr> 
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            <p>* The case can be linked by either (Person Number, Start Date, and Case Code) or by specifying (Person Number and External ID).</p>
            `
        },
        { 
            type_name: 'Leave Case Eligibility Requirements',
            apiType: 'leaveCases', //OPTIONAL only needed for update existing logic
            apiThreads: 5,
            //visible:false,
            apiBatchQty: 1,
            cdata: [
                {visible: true,data:'Person Number',name: 'pnum'},
                {visible: true,data:'Start Date',name: 'sdt'},
                {visible: true,data:'Case Code',name: 'case_code'},
                {visible: true,data:'External ID',name: 'extid'},
                {visible: true,data:'Eligibility Requirement',name: 'eligibility_requirement'},
                {visible: true,data:'Should Be Bypassed',name: 'bypass',datasource:['Y','N']},
                {visible: true,data:'Should Be Recalculated',name: 'recalc',datasource:['Y','N']}
            ],
            apiMap: function(data) {  
                leaveEligReq = {}
                leaveEligReq.apiUrl = '/v1/leave/leave_cases/eligibility_requirements'
                leaveEligReq.apiWrap = []
                leaveEligReq.apiBatchQty = 1
                leaveEligReq.apiThreads = 5
                
                if (data.bypass == 'Y') data.bypass = true
                else data.bypass = false
                
                if (data.recalc == 'Y') data.recalc = true
                else data.recalc = false

                leaveEligReq.api = 
                {
                    "leaveCase": {"id": "SHOULD BE UPDATED LATER"},
                    "requirements": [
                      {
                        "name":data.eligibility_requirement,
                        "shouldBeBypassed": data.bypass,
                        "shouldBeRecalculated": data.recalc
                      }
                    ]
                }

                return leaveEligReq    
            },
            findExisting: 
            {
                setDataSource:(da,ds)=>{
                    
                    da.forEach((d)=>{
                        d.__sdt = new Date(d.sdt)
                    })

                    let el = Enumerable.From(da)
                    .GroupBy("$.pnum", null,
                        function (key, g) {
                            return {
                            personnum: key,
                            min_effdt: g.Min("$.__sdt"),
                            max_effdt: g.Max("$.__sdt")
                        }
                    })
                    .ToArray();
    
                    let empList = []
                    $(el).each(function(k,v){empList.push(v.personnum)})
    
                    let mnmxEffDt = Enumerable.From(da)
                    .GroupBy("1", null,
                        function (key, g) {
                            return {
                            id: key,
                            mn_applydt: g.Min("$.__sdt"),
                            mx_applydt: g.Max("$.__sdt")
                        }
                    }).ToArray()
                    
                    try {
                        ds.dateRange.startDate = mnmxEffDt[0].mn_applydt.toISOString().split('T')[0]
                        ds.dateRange.endDate = mnmxEffDt[0].mx_applydt.toISOString().split('T')[0]
                        ds.employees.qualifiers = empList
                    }
                    catch(e){}
                    return ds
                },
                datasource: {
                    type:'leaveCases',
                    apiUrl: '/v1/leave/leave_cases/multi_read',
                    pdata: {
                        "dateRange": {
                          "endDate": "",
                          "startDate": "",
                        },
                        "employees": {
                          "qualifiers": [],
                        }
                    },
                    apiBatchOn: 'employees.qualifiers',
                    apiBatchQty: 100
                },
                updateExists: function(o,existRes,calls,apiType) { 
                    if (apiType == 'leaveCases') {
                        extMatch = false
                        $(existRes).each(function(k,res){
                            $(calls).each((k,call)=>{
                                if (res.id == call.tdata.extid) {
                                    extMatch = true
                                    call.api.leaveCase.id = res.id
                                }
                                else if (
                                    extMatch == false && 
                                    res.employee.qualifier == call.tdata.pnum &&
                                    res.startDate == dateAdjust(call.tdata.sdt) &&
                                    res.caseCode == call.tdata.case_code
                                ) call.api.leaveCase.id = res.id
                            })
                        })
                    }
                    //console.log('o',o)
                    //console.log('existRes',existRes)
                    //console.log('calls',calls)
                    //console.log('apiType',apiType)
                }
            },
            data : [
                ["20325","1/20/2020","C100","457","Hours worked",'Y','N']
            ],
            descHTML: `
            <p>This import is capable of importing leave case eligibility requirements.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                  <th>Optional</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
                <tr><th scope="row">Person Number</th><td>Text</td><td>12345</td><td></td></tr>
                <tr><th>Start Date</th><td>Date</td><td>2020-01-01</td><td></td></tr>                   
                <tr><th>Case Code</th><td>Text</td><td>C100</td><td></td></tr>                   
                <tr><th>External ID</th><td>Number</td><td>456</td><td></td></tr>   
                <tr><th>Should Be Bypassed</th><td>Text</td><td>Y or N</td><td>Y</td></tr> 
                <tr><th>Should Be Recalculated</th><td>Text</td><td>Y or N</td><td>Y</td></tr> 
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            <p>* The case can be linked by either (Person Number, Start Date, and Case Code) or by specifying (Person Number and External ID).</p>
            `
        },
        {
            type_name: 'Pay Code Edit',
            apiType: 'payCodeEdits', //OPTIONAL only needed for remove existing logic
            apiUrl: '/v1/timekeeping/pay_code_edits/import',
            apiThreads: 4,
            apiBatchQty: 50,
            /*
            'allowableErrors': [{
                errorCode: 'WTK-120127"',
                errorColor: 'blue',
                errorMessage: 'The punch has previously been loaded'
            }],
            */
           cdata: [
                { visible: true,data: 'Person Number',name: 'personnum'},
                { visible: true,data: 'Apply Date',name: 'applydt'},
                { visible: true,data: 'Start Time',name: 'stm'},
                { visible: true,data: 'Paycode',name: 'paycode',datasource: {apiurl: '/v1/timekeeping/setup/pay_codes',tag: 'name'}}, 
                { visible: true,data: 'Amount',name: 'amount'},
                { visible: true,data: 'Type',name: 'type',datasource: ['Hour','Day','Money']},
                { visible: true,data: 'Job Path',name: 'jobpath'},
                //{ visible: true,data: 'Workrule',name: 'workrule'}, //NEEDAPI
                { visible: true,data: 'Cost Center',name: 'costcenter', datasource:{apiurl:'/v1/commons/cost_centers',tag:'name'}},
                { visible: true,data: 'Labor Categories',name: 'laborcats',datasource:{apiurl:'/v1/commons/labor_categories',tag:'name'}}
            ],
            apiWrap: {"payCodeEdits":[]},
            apiMap: function(d) {
                if (d.jobpath == null) d.jobpath = ""
                //if (d.workrule == null) d.workrule = ""
                if (d.costcenter == null) d.costcenter = ""
                if (d.laborcats == null) d.laborcats = ""
                tString = d.jobpath + ';' + '' + ';' + d.costcenter + ';' + d.laborcats

                if (d.type) d.type = d.type.toUpperCase()
                if (d.stm) d.stm = dateAdjust(d.applydt) + 'T' + stringToTime(d.stm)
                else d.stm = dateAdjust(d.applydt) + 'T00:00'

                var x = 
                {
                    "startDateTime": d.stm,
                    "amountType": d.type,
                    "paycode": {
                        "qualifier": d.paycode
                    },
                    "moneyAmount": "",
                    "applyDate": dateAdjust(d.applydt),
                    "employee": {
                        "qualifier": d.personnum
                    },
                    "durationInHours": "",
                    "durationInDays": "",
                    "transfer": {
                        "transferString": tString,
                        }
                }

                if (tString == ';;;') delete x.transfer
                //if (!x.startDateTime) delete x.startDateTime

                if (d.type == 'MONEY') x.moneyAmount = d.amount
                if (d.type == 'DAY') x.durationInDays = d.amount
                if (!d.type || d.type == 'HOUR') x.durationInHours = d.amount
                if (!x.amountType) x.amountType = "HOUR"

                if (!x.moneyAmount) delete x['moneyAmount']
                if (!x.durationInDays) delete x['durationInDays']
                if (!x.durationInHours) delete x['durationInHours']
                
                //TODO: CHECK TO SEE IF CERTAIN FIELDS ARE NULL not passing if null
                return x
            },
            removeExisting: 
            {
                datasource: {
                    type:'payCodeEdits',
                    apiUrl: '/v1/timekeeping/timecard/multi_read',
                    pdata: {
                        "select": ["PAYCODE_EDITS"],
                        "where": {
                            "dateRange": {
                            "endDate": "",
                            "startDate": ""
                            },
                            "employees": {
                            "qualifiers": []
                            }
                        }
                    },
                    apiBatchOn: 'where.employees.qualifiers',
                    apiBatchQty: 100
                },
                checkExists: function(o,existRes,apiType){                    
                    inExistArr = false
                    pce = o.api
                    if (apiType == 'payCodeEdits') {
                        $(existRes).each(function(k,v){
                            let amount                             
                            if (pce.amountType == 'MONEY') amount = pce.moneyAmount
                            if (pce.amountType == 'DAY') amount = pce.durationInDays
                            if (pce.amountType == 'HOUR') amount = pce.durationInHours
    
                            if (v.amountType == 'MONEY') v.amount = v.moneyAmount
                            if (v.amountType == 'DAY') v.amount = v.durationInDays
                            if (v.amountType == 'HOUR') v.amount = v.durationInHours
    
                            if (
                                dateAdjust(pce.applyDate) == v.applyDate &&
                                pce.employee.qualifier == v.employee.qualifier &&
                                pce.paycode.qualifier == v.paycode.qualifier &&
                                amount == v.amount
                            ) inExistArr = true
                            //console.log(inExistArr,pce.employee.qualifier + '|' + pce.applyDate + '|' + pce.paycode.qualifier + '|' + pce.amount + '||' + v.employee.qualifier + '|' + v.applyDate + '|' + v.paycode.qualifier + '|' + v.amount)
                        })
                    }
                    return inExistArr
                }
            },
            updateRemoveExisting: function(da) {
                da.forEach((d)=>{
                    d.__applydt = new Date(d.applydt)
                })

                let el = Enumerable.From(da)
                .GroupBy("$.personnum", null,
                    function (key, g) {
                        return {
                        personnum: key,
                        min_effdt: g.Min("$.__applydt"),
                        max_effdt: g.Max("$.__applydt")
                    }
                })
                .ToArray();

                let empList = []
                $(el).each(function(k,v){empList.push(v.personnum)})

                let mnmxEffDt = Enumerable.From(da)
                .GroupBy("1", null,
                    function (key, g) {
                        return {
                        id: key,
                        mn_applydt: g.Min("$.__applydt"),
                        mx_applydt: g.Max("$.__applydt")
                    }
                }).ToArray()
                
                try {
                    this.removeExisting.datasource.pdata.where.dateRange.startDate = mnmxEffDt[0].mn_applydt.toISOString().split('T')[0]
                    this.removeExisting.datasource.pdata.where.dateRange.endDate = mnmxEffDt[0].mx_applydt.toISOString().split('T')[0]
                    this.removeExisting.datasource.pdata.where.employees.qualifiers = empList
                }
                catch(e) {}
            },
            removeExistingResponse: "",
            updateRemoveExistingResponse: function(res){
                oArr = []
                $(res).each(function(k,v){
                    if (v.hasOwnProperty('payCodeEdits')) {
                        $(v.payCodeEdits).each(function(kk,vv){
                            oArr.push(vv)
                        })
                    }
                })
                return oArr
            },
           data : [
                ["20329","2019-09-11","08:00","Regular",1.00,""]
            ],
            descHTML: `
            <p>This import is capable of importing pay code edits.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                  <th>Optional</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
                <tr><th scope="row">Person Number</th><td>Text</td><td>12345</td><td></td></tr>
                <tr><th>Apply Date</th><td>Date</td><td>2020-01-01</td><td></td></tr>                   
                <tr><th>Start Time</th><td>Time</td><td>08:00</td><td></td></tr>                   
                <tr><th>Paycode</th><td>Text</td><td>Regular</td><td></td></tr>   
                <tr><th>Type</th><td>Text</td><td>Hour,Day,Money</td><td>Y</td></tr> 
                <tr><th>Job Path</th><td>Text</td><td>./Organization/path/to/job</td><td>Y</td></tr> 
                <tr><th>Workrule</th><td>Text</td><td>Non-Exempt Union</td><td>Y</td></tr> 
                <tr><th>Cost Center</th><td>Text</td><td>CC1</td><td>Y</td></tr> 
                <tr><th>Labor Categories</th><td>Text</td><td>Contract Code</td><td>Y</td></tr> 
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            <p>* Labor Categories, Cost Centers, Types, and Paycodes are validated against WFD.</p>
            `
        },
        {
            type_name: 'Punch',
            apiUrl: '/v1/timekeeping/punches/import',
            apiThreads: 4,
            apiBatchQty: 50,
           cdata: [
                { visible: true,data: 'Person Number*',name: 'personnum'},
                { visible: true,data: 'Punch DTM*',name: 'punchdtm',tooltip: 'YYYY-MM-DD HH:MM'},
                { visible: true,data: 'Override Type',name: 'override',datasource: ['In Punch','New Shift','Out Punch']}
            ],
            apiWrap: {"punches":[]},
            apiMap: function(d) {
                if(d.punchdtm != null)
				{
					d.punchdtm = d.punchdtm.replace(' ','T')
				}
                //tString = d.jobpath + ';' + d.workrule + ';' + d.costcenter + ';' + d.laborcats
                var x =    
                {
                "punchDtm": d.punchdtm,     
                "employee": {
                    "qualifier": d.personnum
                },
                "typeOverride": {
                    "id": ""
                }
                /*
                "transfer": {
                    "transferString": tString
                    }
                    */
                }
                    
                  
                  punchOverrideTypes = [
                      {vname:'In Punch',poid: 2},
                      {vname:'New Shift',poid: 3},
                      {vname:'Out Punch',poid: 4},
                  ]
                  let poid
                  $(punchOverrideTypes).each(function(k,v){
                        if (d.override == v.vname) poid = v.poid
                  })
                  
                  //if (tString == ';;;') delete x.punches[0].transfer
                  if (poid) x.typeOverride.id = poid
                  else delete x.typeOverride

                //if (x.color == '') delete x['color']
                //TODO: CHECK TO SEE IF CERTAIN FIELDS ARE NULL not passing if null
                return x
            },
        
           data : [
                ["20329","2019-09-11 08:00"],
            ],
            descHTML: `
            <p>This import is capable of importing punches.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                  <th>Optional</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
                <tr><th>Person Number</th><td>Text</td><td>12345</td><td></td></tr>
                <tr><th>Punch DTM</th><td>Datetime</td><td>2019-09-11 08:00</td><td></td></tr>                   
                <tr><th>Override Type</th><td>Text</td><td>In Punch</td><td></td></tr>                   
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            `
        },
        { //GENERIC DELETE
            type_name: 'Delete',
            apiThreads: 5,
            visible:false,
            apiBatchQty: 100,
            /*
            allowableErrors: [{
                errorCode: 'WTK-170000',
                errorMessage: 'The Cost Center has previously been loaded'
            }],
            */
            cdata: [
                {visible: true,data: 'Person Number',name: 'pnum'},
                {visible: true,data: 'Effective Date',name: 'effdt'},
                {visible: true,data: 'Imported ID',name: 'id'},
                {visible: true,data: 'Category',name: 'category',datasource:['Paycode Edit','Attendance Event']}
            ],
            apiWrap: [],
            apiMap: function(data) {
                tmpAPIArr = []

                if (data.category == 'Paycode Edit') {
                    pceDelete = {}
                    pceDelete.apiUrl = '/v1/timekeeping/pay_code_edits/multi_delete'
                    pceDelete.apiWrap = {'payCodeEdits':[]}
                    pceDelete.api = {
                        id:data.id,
                        employee:{qualifier:data.pnum},
                        applyDate:dateAdjust(data.effdt)
                    }
                    tmpAPIArr.push(pceDelete)
                }

                if (data.category == 'Attendance Event') {
                    attnDelete = {}
                    attnDelete.apiThreads = 5
                    attnDelete.apiBatchQty = 1
                    //attnDelete.apiUrl = '/v1/attendance/events/$.id'
                    attnDelete.apiUrl = '/v1/attendance/events/$.id/mark_deleted'
                    attnDelete.apiMethod = 'POST'
                    tmpAPIArr.push(attnDelete)
                }
                return tmpAPIArr
            },
            data : [
                //["111","2020-01-01","123","Paycode Edit"]
                ["111","2020-01-01","zzzz","Attendance Event"]
            ]
        },
        { //ATTN EVENT 
            type_name: 'POS',
            visible:false,
            apiUrl: '/v1/forecasting/actual_volume/import',
            apiThreads: 1,
            apiBatchQty: 250,
            /*
            allowableErrors: [{
                errorCode: 'WTK-170000',
                errorMessage: 'The Cost Center has previously been loaded'
            }],
            */
            cdata: [
                {visible: true,data: 'Site',name: 'site'},
                {visible: true,data: 'Effective Date',name: 'effdt'},
                {visible: true,data: 'Label',name: 'label'},
                {visible: true,data: 'Driver',name: 'driver'},
                {visible: true,data: 'S1',name: 'S1'},
                {visible: true,data: 'S2',name: 'S2'},
                {visible: true,data: 'S3',name: 'S3'},
                {visible: true,data: 'S4',name: 'S4'},
                {visible: true,data: 'S5',name: 'S5'},
                {visible: true,data: 'S6',name: 'S6'},
                {visible: true,data: 'S7',name: 'S7'},
                {visible: true,data: 'S8',name: 'S8'},
                {visible: true,data: 'S9',name: 'S9'},
                {visible: true,data: 'S10',name: 'S10'},
                {visible: true,data: 'S11',name: 'S11'},
                {visible: true,data: 'S12',name: 'S12'},
                {visible: true,data: 'S13',name: 'S13'},
                {visible: true,data: 'S14',name: 'S14'},
                {visible: true,data: 'S15',name: 'S15'},
                {visible: true,data: 'S16',name: 'S16'},
                {visible: true,data: 'S17',name: 'S17'},
                {visible: true,data: 'S18',name: 'S18'},
                {visible: true,data: 'S19',name: 'S19'},
                {visible: true,data: 'S20',name: 'S20'},
                {visible: true,data: 'S21',name: 'S21'},
                {visible: true,data: 'S22',name: 'S22'},
                {visible: true,data: 'S23',name: 'S23'},
                {visible: true,data: 'S24',name: 'S24'},
                {visible: true,data: 'S25',name: 'S25'},
                {visible: true,data: 'S26',name: 'S26'},
                {visible: true,data: 'S27',name: 'S27'},
                {visible: true,data: 'S28',name: 'S28'},
                {visible: true,data: 'S29',name: 'S29'},
                {visible: true,data: 'S30',name: 'S30'},
                {visible: true,data: 'S31',name: 'S31'},
                {visible: true,data: 'S32',name: 'S32'},
                {visible: true,data: 'S33',name: 'S33'},
                {visible: true,data: 'S34',name: 'S34'},
                {visible: true,data: 'S35',name: 'S35'},
                {visible: true,data: 'S36',name: 'S36'},
                {visible: true,data: 'S37',name: 'S37'},
                {visible: true,data: 'S38',name: 'S38'},
                {visible: true,data: 'S39',name: 'S39'},
                {visible: true,data: 'S40',name: 'S40'},
                {visible: true,data: 'S41',name: 'S41'},
                {visible: true,data: 'S42',name: 'S42'},
                {visible: true,data: 'S43',name: 'S43'},
                {visible: true,data: 'S44',name: 'S44'},
                {visible: true,data: 'S45',name: 'S45'},
                {visible: true,data: 'S46',name: 'S46'},
                {visible: true,data: 'S47',name: 'S47'},
                {visible: true,data: 'S48',name: 'S48'},
                {visible: true,data: 'S49',name: 'S49'},
                {visible: true,data: 'S50',name: 'S50'},
                {visible: true,data: 'S51',name: 'S51'},
                {visible: true,data: 'S52',name: 'S52'},
                {visible: true,data: 'S53',name: 'S53'},
                {visible: true,data: 'S54',name: 'S54'},
                {visible: true,data: 'S55',name: 'S55'},
                {visible: true,data: 'S56',name: 'S56'},
                {visible: true,data: 'S57',name: 'S57'},
                {visible: true,data: 'S58',name: 'S58'},
                {visible: true,data: 'S59',name: 'S59'},
                {visible: true,data: 'S60',name: 'S60'},
                {visible: true,data: 'S61',name: 'S61'},
                {visible: true,data: 'S62',name: 'S62'},
                {visible: true,data: 'S63',name: 'S63'},
                {visible: true,data: 'S64',name: 'S64'},
                {visible: true,data: 'S65',name: 'S65'},
                {visible: true,data: 'S66',name: 'S66'},
                {visible: true,data: 'S67',name: 'S67'},
                {visible: true,data: 'S68',name: 'S68'},
                {visible: true,data: 'S69',name: 'S69'},
                {visible: true,data: 'S70',name: 'S70'},
                {visible: true,data: 'S71',name: 'S71'},
                {visible: true,data: 'S72',name: 'S72'},
                {visible: true,data: 'S73',name: 'S73'},
                {visible: true,data: 'S74',name: 'S74'},
                {visible: true,data: 'S75',name: 'S75'},
                {visible: true,data: 'S76',name: 'S76'},
                {visible: true,data: 'S77',name: 'S77'},
                {visible: true,data: 'S78',name: 'S78'},
                {visible: true,data: 'S79',name: 'S79'},
                {visible: true,data: 'S80',name: 'S80'},
                {visible: true,data: 'S81',name: 'S81'},
                {visible: true,data: 'S82',name: 'S82'},
                {visible: true,data: 'S83',name: 'S83'},
                {visible: true,data: 'S84',name: 'S84'},
                {visible: true,data: 'S85',name: 'S85'},
                {visible: true,data: 'S86',name: 'S86'},
                {visible: true,data: 'S87',name: 'S87'},
                {visible: true,data: 'S88',name: 'S88'},
                {visible: true,data: 'S89',name: 'S89'},
                {visible: true,data: 'S90',name: 'S90'},
                {visible: true,data: 'S91',name: 'S91'},
                {visible: true,data: 'S92',name: 'S92'},
                {visible: true,data: 'S93',name: 'S93'},
                {visible: true,data: 'S94',name: 'S94'},
                {visible: true,data: 'S95',name: 'S95'},
                {visible: true,data: 'S96',name: 'S96'},
                {visible: true,data: 'As of Date',name: 'aodate'},
            ],
           groupFN: function(tdata) {
            let el = Enumerable.From(tdata).GroupBy(null,null,"{ site: $.site, aodate: $.aodate, driver: $.driver, label: $.label }","$.site + ' ' + $.aodate + ' ' + $.driver + ' ' + $.label").ToArray();
            return el
           }, 
           //YOU MUST USE A FUNCTION WHEN using groupFN 
           apiWrap: function(ginfo,tmp_calls){
               x =
                {
                "actualVolumes": [
                    {
                        "actualVolumesPerDay": [],
                        "driver": {"qualifier": ginfo.driver},
                        "externalLabel": {"qualifier": ginfo.label}
                    }
                ],
                "asOfDate": "",
                "site": {"qualifier": ginfo.site}
                }
                //push calls into request
                tmp_calls.forEach((t)=>{x.actualVolumes[0].actualVolumesPerDay.push(t)})
                
                if (ginfo.aodate) x.asOfDate = dateAdjust(ginfo.aodate)
                else delete x.asOfDate

                return x
            },
           apiMap: function(data){
            /*    
            x = 
            {
                "actualVolumesPerDay": [],
                "driver": {"qualifier": data.driver},
                "externalLabel": {"qualifier": data.label}
            }
            */
            baseVolumeRecord =  {
                "date": dateAdjust(data.effdt),
                "intervalAmounts": "",
                "totalAmount": 0
            }
            go = false
            Object.keys(data).forEach((v,k)=>{
                if (v == 'S1') go = true
                if (go && v.substring(0,1) == 'S') {
                    baseVolumeRecord.intervalAmounts = baseVolumeRecord.intervalAmounts + String(data[v]) + ','
                    //console.log(baseVolumeRecord.intervalAmounts.split(',').length)
                    baseVolumeRecord.totalAmount = baseVolumeRecord.totalAmount + parseFloat(data[v])
                }
            })
            baseVolumeRecord.intervalAmounts = baseVolumeRecord.intervalAmounts.substring(0,baseVolumeRecord.intervalAmounts.length - 1)
            //x.actualVolumesPerDay = baseVolumeRecord
            return baseVolumeRecord
           },
            data : [
                //["BEE-10393","2019-03-27","MTOSANDWICH","Close",0.000000,1.24,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000]
                ["//RCo/Food  Service/Region 1/District 1/Restaurant 201","12/18/2018","Drive Thru","Transactions",0.000000,1.24,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000],
                ["//RCo/Food  Service/Region 1/District 1/Restaurant 201","12/18/2018","Drive Thru","Transactions",0.000000,1.24,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000],
                ["//RCo/Food  Service/Region 1/District 1/Restaurant 201","12/18/2018","Drive Thru","Transactions",0.000000,1.24,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000],
                ["//RCo/Food  Service/Region 1/District 1/Restaurant 201","12/18/2018","Drive Thru","Transactions",0.000000,1.24,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000],
                ["//RCo/Food  Service/Region 1/District 1/Restaurant 201","12/18/2018","Drive Thru","Transactions",0.000000,1.24,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000],
                ["//RCo/Food  Service/Region 1/District 1/Restaurant 201","12/18/2018","Drive Thru","Transactions",0.000000,1.24,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000],
                ["//RCo/Food  Service/Region 1/District 1/Restaurant 201","12/18/2018","Drive Thru","Transactions",0.000000,1.24,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000],
                ["//RCo/Food  Service/Region 1/District 1/Restaurant 201","12/18/2018","Drive Thru","Transactions",0.000000,1.24,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000],
                ["//RCo/Food  Service/Region 1/District 1/Restaurant 201","12/18/2018","Drive Thru","Transactions",0.000000,1.24,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000],
                ["//RCo/Food  Service/Region 1/District 1/Restaurant 201","12/18/2018","Drive Thru","Transactions",0.000000,1.24,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000],
                ["A","12/19/2018","Drive Thru","Transactions",0.000000,1.24,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000],
                ["A","12/18/2018","Drive Thru","Transactions",0.000000,1.24,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000],               
                ["B","12/18/2018","Drive Thru","Transactions",0.000000,1.24,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000],
                ["C","12/18/2018","Drive Thru","Transactions",0.000000,1.24,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000]    
            ],
            descHTML: `
            <p>This type is capable of import POS volume actuals.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                  <th>Optional</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
                <tr><th scope="row">Site</th><td>Text</td><td>Store 1234</td><td></td></tr>
                <tr><th>Effective Date</th><td>Date</td><td>2020-01-01</td><td></td></tr>                   
                <tr><th>Label</th><td>Text</td><td>COFFEEBAR</td><td></td></tr>   
                <tr><th>Driver</th><td>Text</td><td>Sales</td><td>Y</td></tr> 
                <tr><th>15 Minute Increment Data (S1-S96)</th><td>Numeric</td><td>10</td><td>Y</td></tr> 
              </tbody>
              <!--Table body-->

            </table>
            <!--Table-->
            <p>* Segments 1-96 must all be provided.</p>
            `
        },
        { //LABOR FORECAST
            type_name: 'Labor Forecast',
            apiUrl: 'v1/forecasting/labor_forecast/multi_update',
            apiThreads: 5,
            apiBatchQty: 100,
            cdata: [
                {visible: true,data: 'Department Path',name: 'deptpath'},
                {visible: true,data: 'Effective Date',name: 'effdt'},
				{visible: true,data: 'Job Path',name: 'jobpath'},
                {visible: true,data: 'S1',name: 'S1'},
                {visible: true,data: 'S2',name: 'S2'},
                {visible: true,data: 'S3',name: 'S3'},
                {visible: true,data: 'S4',name: 'S4'},
                {visible: true,data: 'S5',name: 'S5'},
                {visible: true,data: 'S6',name: 'S6'},
                {visible: true,data: 'S7',name: 'S7'},
                {visible: true,data: 'S8',name: 'S8'},
                {visible: true,data: 'S9',name: 'S9'},
                {visible: true,data: 'S10',name: 'S10'},
                {visible: true,data: 'S11',name: 'S11'},
                {visible: true,data: 'S12',name: 'S12'},
                {visible: true,data: 'S13',name: 'S13'},
                {visible: true,data: 'S14',name: 'S14'},
                {visible: true,data: 'S15',name: 'S15'},
                {visible: true,data: 'S16',name: 'S16'},
                {visible: true,data: 'S17',name: 'S17'},
                {visible: true,data: 'S18',name: 'S18'},
                {visible: true,data: 'S19',name: 'S19'},
                {visible: true,data: 'S20',name: 'S20'},
                {visible: true,data: 'S21',name: 'S21'},
                {visible: true,data: 'S22',name: 'S22'},
                {visible: true,data: 'S23',name: 'S23'},
                {visible: true,data: 'S24',name: 'S24'},
                {visible: true,data: 'S25',name: 'S25'},
                {visible: true,data: 'S26',name: 'S26'},
                {visible: true,data: 'S27',name: 'S27'},
                {visible: true,data: 'S28',name: 'S28'},
                {visible: true,data: 'S29',name: 'S29'},
                {visible: true,data: 'S30',name: 'S30'},
                {visible: true,data: 'S31',name: 'S31'},
                {visible: true,data: 'S32',name: 'S32'},
                {visible: true,data: 'S33',name: 'S33'},
                {visible: true,data: 'S34',name: 'S34'},
                {visible: true,data: 'S35',name: 'S35'},
                {visible: true,data: 'S36',name: 'S36'},
                {visible: true,data: 'S37',name: 'S37'},
                {visible: true,data: 'S38',name: 'S38'},
                {visible: true,data: 'S39',name: 'S39'},
                {visible: true,data: 'S40',name: 'S40'},
                {visible: true,data: 'S41',name: 'S41'},
                {visible: true,data: 'S42',name: 'S42'},
                {visible: true,data: 'S43',name: 'S43'},
                {visible: true,data: 'S44',name: 'S44'},
                {visible: true,data: 'S45',name: 'S45'},
                {visible: true,data: 'S46',name: 'S46'},
                {visible: true,data: 'S47',name: 'S47'},
                {visible: true,data: 'S48',name: 'S48'},
                {visible: true,data: 'S49',name: 'S49'},
                {visible: true,data: 'S50',name: 'S50'},
                {visible: true,data: 'S51',name: 'S51'},
                {visible: true,data: 'S52',name: 'S52'},
                {visible: true,data: 'S53',name: 'S53'},
                {visible: true,data: 'S54',name: 'S54'},
                {visible: true,data: 'S55',name: 'S55'},
                {visible: true,data: 'S56',name: 'S56'},
                {visible: true,data: 'S57',name: 'S57'},
                {visible: true,data: 'S58',name: 'S58'},
                {visible: true,data: 'S59',name: 'S59'},
                {visible: true,data: 'S60',name: 'S60'},
                {visible: true,data: 'S61',name: 'S61'},
                {visible: true,data: 'S62',name: 'S62'},
                {visible: true,data: 'S63',name: 'S63'},
                {visible: true,data: 'S64',name: 'S64'},
                {visible: true,data: 'S65',name: 'S65'},
                {visible: true,data: 'S66',name: 'S66'},
                {visible: true,data: 'S67',name: 'S67'},
                {visible: true,data: 'S68',name: 'S68'},
                {visible: true,data: 'S69',name: 'S69'},
                {visible: true,data: 'S70',name: 'S70'},
                {visible: true,data: 'S71',name: 'S71'},
                {visible: true,data: 'S72',name: 'S72'},
                {visible: true,data: 'S73',name: 'S73'},
                {visible: true,data: 'S74',name: 'S74'},
                {visible: true,data: 'S75',name: 'S75'},
                {visible: true,data: 'S76',name: 'S76'},
                {visible: true,data: 'S77',name: 'S77'},
                {visible: true,data: 'S78',name: 'S78'},
                {visible: true,data: 'S79',name: 'S79'},
                {visible: true,data: 'S80',name: 'S80'},
                {visible: true,data: 'S81',name: 'S81'},
                {visible: true,data: 'S82',name: 'S82'},
                {visible: true,data: 'S83',name: 'S83'},
                {visible: true,data: 'S84',name: 'S84'},
                {visible: true,data: 'S85',name: 'S85'},
                {visible: true,data: 'S86',name: 'S86'},
                {visible: true,data: 'S87',name: 'S87'},
                {visible: true,data: 'S88',name: 'S88'},
                {visible: true,data: 'S89',name: 'S89'},
                {visible: true,data: 'S90',name: 'S90'},
                {visible: true,data: 'S91',name: 'S91'},
                {visible: true,data: 'S92',name: 'S92'},
                {visible: true,data: 'S93',name: 'S93'},
                {visible: true,data: 'S94',name: 'S94'},
                {visible: true,data: 'S95',name: 'S95'},
                {visible: true,data: 'S96',name: 'S96'}
            ],
           groupFN: function(tdata) {
            let el = Enumerable.From(tdata).GroupBy(null,null,"{ deptpath: $.deptpath, effdt: $.effdt}","$.deptpath + ' ' + $.effdt").ToArray();
            return el
           }, 
           //YOU MUST USE A FUNCTION WHEN using groupFN 
           apiWrap: function(ginfo,tmp_calls){
                x = 
                {
					"date": dateAdjust(ginfo.effdt),
					"department": {
						"qualifier": ginfo.deptpath
					},
				  "laborForecasts": []
                }
                //push calls into request
                tmp_calls.forEach((t)=>{x.laborForecasts.push(t)})
                return x
            },
           apiMap: function(data){
            
            baseLaborRecord = 
            {
                "intervalAmounts": [],
                "laborForecastType": "ADJUSTED",
                "orgJob": {
                    "qualifier": data.jobpath
                }
            }

            go = false
            sdt = new Date(); 
            edt = new Date(); 

            Object.keys(data).forEach((v,k)=>{
                if (v == 'S1') go = true
                if (go && v.substring(0,1) == 'S') {                                        
                     
                    i = parseInt(v.toUpperCase().replace('S','')) - 1
                    sdt.setHours(0,i * 15,0);
                    edt.setHours(0,(i + 1) * 15,0)

                    stm = sdt.toTimeString().split(' ')[0]
                    etm = edt.toTimeString().split(' ')[0]
                        
                    baseInterval =   {
                        "amount": data[v],
                        "startTime": stm,
                        "endTime": etm
                    }

                    baseLaborRecord.intervalAmounts.push(baseInterval)
                }
                
            })
            return baseLaborRecord
           },
            data : [
                ['Kroger/Retail/Supermarket/021 - Central/04/00990/Front End','08/23/2020','Kroger/Retail/Supermarket/021 - Central/04/00990/Front End/File Maint','0','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0']    
            ],
            descHTML: `
            <p>This type is capable of importing labor forecast data.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                  <th>Optional</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
                <tr><th>Department Path</th><td>Text</td><td>Org/Store/Dept</td><td></td></tr>
                <tr><th>Effective Date</th><td>Date</td><td>2020-01-01</td><td></td></tr>                   
                <tr><th>Job Path</th><td>Text</td><td>COFFEEBAR</td><td></td></tr>   
                <tr><th>Driver</th><td>Text</td><td>Org/Store/Dept/Job</td><td>Y</td></tr> 
                <tr><th>15 Minute Increment Data (S1-S96)</th><td>Numeric</td><td>10</td><td>Y</td></tr> 
              </tbody>
              <!--Table body-->

            </table>
            <!--Table-->
            <p>* Segments 1-96 must all be provided.</p>
            `
        },
        { //SCHED GROUP
            type_name: 'Schedule Group',
            //excludeEnvironmentTypes: ['Production'],
            /*
            allowableErrors: [{
                errorCode: 'WTK-170000',
                errorMessage: 'The Cost Center has previously been loaded'
            }],
            */
            cdata: [
                {visible: true,data: 'Person Number',name: 'employee'},
                {visible: true,data: 'Schedule Group',name: 'schedulegroup',datasource: {apiurl: '/v1/scheduling/setup/schedule_groups',tag: 'name'}},
				{visible: true,data: 'Effective Date',name: 'effectiveDate'}
            ],
            apiMap: function(data) {
                request = {}
            
				request.apiUrl = '/v1/commons/persons/schedule_groups/multi_upsert'
				request.apiWrap = []
				request.apiBatchQty = 5
				request.apiThreads = 5
				request.api = 
				{
				  "assignments": [
					{
								 "effectiveDate": dateAdjust(data.effectiveDate),
								 "expirationDate": "3000-01-01",
								 "removeFromOtherGroups": true,
								 "scheduleGroup": {
								   "qualifier": data.schedulegroup
								 }
					},        
				  ],
				  "assignmentSpan": {
					"end": "3000-01-01",
					"start": dateAdjust(data.effectiveDate)
				  },
				  "personIdentity": {
					"personNumber": data.employee
				  }
				}

                return request
            },
            data : [
                ["20326","Schedule Group","2020-02-06"]
            ],
            descHTML: `
            <p>This import is capable of performing schedule group assignments.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                  <th>Optional</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
             <tr>
                
                <tr><th scope="row">Person Number</th><td>Text</td><td>12345</td><td></td></tr>
                <tr><th>Schedule Group</th><td>Text</td><td>Group A</td><td></td></tr>
                <tr><th>Effective Date</th><td>Date</td><td>2020-01-01</td><td></td></tr>
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            `
        },
        { //HIST PCE
            type_name: 'Pay Code Edit Historical',
            apiType: 'payCodeEdits', //OPTIONAL only needed for remove existing logic
            //excludeEnvironmentTypes: ['Production'],
            /*
            allowableErrors: [{
                errorCode: 'WTK-170000',
                errorMessage: 'The Cost Center has previously been loaded'
            }],
            */
            cdata: [
                { visible: true,data: 'Person Number',name: 'personnum'},
                { visible: true,data: 'Apply Date',name: 'applydt'},
                { visible: true,data: 'Start Time',name: 'stm'},
                { visible: true,data: 'Paycode',name: 'paycode',datasource: {apiurl: '/v1/timekeeping/setup/pay_codes',tag: 'name'}}, 
                { visible: true,data: 'Amount',name: 'amount'},
                { visible: true,data: 'Type',name: 'type',datasource: ['Hour','Day','Money']},
                { visible: true,data: 'Job Path',name: 'jobpath'},
                //{ visible: true,data: 'Workrule',name: 'workrule'}, //NEEDAPI
                { visible: true,data: 'Cost Center',name: 'costcenter', datasource:{apiurl:'/v1/commons/cost_centers',tag:'name'}},
                { visible: true,data: 'Labor Categories',name: 'laborcats',datasource:{apiurl:'/v1/commons/labor_categories',tag:'name'}}
            ],
            apiMap: function(d) {
                request = {}
				request.apiUrl = 'v1/timekeeping/timecard'
                request.apiThreads = 5
                
                if (d.jobpath == null) d.jobpath = ""
                //if (d.workrule == null) d.workrule = ""
                if (d.costcenter == null) d.costcenter = ""
                if (d.laborcats == null) d.laborcats = ""
                tString = d.jobpath + ';' + '' + ';' + d.costcenter + ';' + d.laborcats

                if (d.type) d.type = d.type.toUpperCase()
                if (d.stm) d.stm = dateAdjust(d.applydt) + 'T' + stringToTime(d.stm)
                else d.stm = dateAdjust(d.applydt) + 'T00:00'
				
				var x = 
                {
                    "startDateTime": d.stm,
                    "amountType": d.type,
                    "paycode": {
                        "qualifier": d.paycode
                    },
                    "moneyAmount": "",
                    "applyDate": dateAdjust(d.applydt),
                    "employee": {
                        "qualifier": d.personnum
                    },
                    "durationInHours": "",
                    "durationInDays": "",
                    "transfer": {
                        "transferString": tString,
                        }
                }

                if (tString == ';;;') delete x.transfer
                //if (!x.startDateTime) delete x.startDateTime

                if (d.type == 'MONEY') x.moneyAmount = d.amount
                if (d.type == 'DAY') x.durationInDays = d.amount
                if (!d.type || d.type == 'HOUR') x.durationInHours = d.amount
                if (!x.amountType) x.amountType = "HOUR"

                if (!x.moneyAmount) delete x['moneyAmount']
                if (!x.durationInDays) delete x['durationInDays']
                if (!x.durationInHours) delete x['durationInHours']
				
				request.api = 
				{
					"do": {
						"payCodeEdits": {
							"added": [x]
						}
					},
					"where": {
						"dateRange": {
							"endDate": dateAdjust(d.applydt),
							"startDate": dateAdjust(d.applydt)
						},
						"employee": {
							"qualifier": d.personnum
						}
					}
				}

                return request
            },
            removeExisting: 
            {
                datasource: {
                    type:'payCodeEdits',
                    apiUrl: '/v1/timekeeping/timecard/multi_read',
                    pdata: {
                        "select": ["PAYCODE_EDITS"],
                        "where": {
                            "dateRange": {
                            "endDate": "",
                            "startDate": ""
                            },
                            "employees": {
                            "qualifiers": []
                            }
                        }
                    },
                    apiBatchOn: 'where.employees.qualifiers',
                    apiBatchQty: 100
                },
                checkExists: function(o,existRes,apiType){                    
                    inExistArr = false
                    pce = o.api.do.payCodeEdits.added[0]
                    if (apiType == 'payCodeEdits') {
                        $(existRes).each(function(k,v){
                            let amount                             
                            if (pce.amountType == 'MONEY') amount = pce.moneyAmount
                            if (pce.amountType == 'DAY') amount = pce.durationInDays
                            if (pce.amountType == 'HOUR') amount = pce.durationInHours
    
                            if (v.amountType == 'MONEY') v.amount = v.moneyAmount
                            if (v.amountType == 'DAY') v.amount = v.durationInDays
                            if (v.amountType == 'HOUR') v.amount = v.durationInHours
    
                            if (
                                dateAdjust(pce.applyDate) == v.applyDate &&
                                pce.employee.qualifier == v.employee.qualifier &&
                                pce.paycode.qualifier == v.paycode.qualifier &&
                                amount == v.amount
                            ) inExistArr = true
                            //console.log(inExistArr,pce.employee.qualifier + '|' + pce.applyDate + '|' + pce.paycode.qualifier + '|' + pce.amount + '||' + v.employee.qualifier + '|' + v.applyDate + '|' + v.paycode.qualifier + '|' + v.amount)
                        })
                    }
                    return inExistArr
                }
            },
            updateRemoveExisting: function(da) {
                da.forEach((d)=>{
                    d.__applydt = new Date(d.applydt)
                })

                let el = Enumerable.From(da)
                .GroupBy("$.personnum", null,
                    function (key, g) {
                        return {
                        personnum: key,
                        min_effdt: g.Min("$.__applydt"),
                        max_effdt: g.Max("$.__applydt")
                    }
                })
                .ToArray();

                let empList = []
                $(el).each(function(k,v){empList.push(v.personnum)})

                let mnmxEffDt = Enumerable.From(da)
                .GroupBy("1", null,
                    function (key, g) {
                        return {
                        id: key,
                        mn_applydt: g.Min("$.__applydt"),
                        mx_applydt: g.Max("$.__applydt")
                    }
                }).ToArray()
                
                try {
                    this.removeExisting.datasource.pdata.where.dateRange.startDate = mnmxEffDt[0].mn_applydt.toISOString().split('T')[0]
                    this.removeExisting.datasource.pdata.where.dateRange.endDate = mnmxEffDt[0].mx_applydt.toISOString().split('T')[0]
                    this.removeExisting.datasource.pdata.where.employees.qualifiers = empList
                }
                catch(e) {}
            },
            removeExistingResponse: "",
            updateRemoveExistingResponse: function(res){
                oArr = []
                $(res).each(function(k,v){
                    if (v.hasOwnProperty('payCodeEdits')) {
                        $(v.payCodeEdits).each(function(kk,vv){
                            oArr.push(vv)
                        })
                    }
                })
                return oArr
            },
            data : [
                ["20329","08/11/2020","08:00","Regular",1.00,""]
            ],
            descHTML: `
            <p>This import is capable of importing historical pay code edits.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                  <th>Optional</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
                <tr><th scope="row">Person Number</th><td>Text</td><td>12345</td><td></td></tr>
                <tr><th>Apply Date</th><td>Date</td><td>2020-01-01</td><td></td></tr>                   
                <tr><th>Start Time</th><td>Time</td><td>08:00</td><td></td></tr>                   
                <tr><th>Paycode</th><td>Text</td><td>Regular</td><td></td></tr>   
                <tr><th>Type</th><td>Text</td><td>Hour,Day,Money</td><td>Y</td></tr> 
                <tr><th>Job Path</th><td>Text</td><td>./Organization/path/to/job</td><td>Y</td></tr> 
                <tr><th>Workrule</th><td>Text</td><td>Non-Exempt Union</td><td>Y</td></tr> 
                <tr><th>Cost Center</th><td>Text</td><td>CC1</td><td>Y</td></tr> 
                <tr><th>Labor Categories</th><td>Text</td><td>Contract Code</td><td>Y</td></tr> 
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            <p>* Labor Categories, Cost Centers, Types, and Paycodes are validated against WFD.</p>
            `
        },
        { //PERSON ASSIGNMENTS
            type_name: 'Person Assignments',
            //excludeEnvironmentTypes: ['Production'],
            /*
            allowableErrors: [{
                errorCode: 'WTK-170000',
                errorMessage: 'The Cost Center has previously been loaded'
            }],
            */
            cdata: [
                
                {visible: true,data: 'Person Number',name: 'pnum'},
				{visible: true,data: 'Assignment Type',name: 'atype',datasource:['Cascade Profile','Adjustment Rule']},
				{visible: true,data: 'Assignment Value',name: 'aval'},
                {visible: true,data: 'Effective Date',name: 'effDate'},
				{visible: true,data: 'Expiration Date',name: 'expDate'},
                
                
            ],
            apiMap: function(data) {
                request = {}
                if (data.atype.toUpperCase() == 'CASCADE PROFILE')
                {
                    request.apiUrl = '/v1/commons/persons/cascade_profile/multi_update'
                    request.apiWrap = []
                    request.apiBatchQty = 500
                    request.apiThreads = 5
                    request.api =                     
					{
					  "assignmentProfile": data.aval,
					  "personIdentity": {
						"personNumber": data.pnum,
					  }
					}
					
                }

                if (data.atype.toUpperCase() == 'ADJUSTMENT RULE') {
                    request.apiUrl = 'v1/commons/persons/adjustment_rule/multi_create'
                    request.apiWrap = []
                    request.apiBatchQty = 500
                    request.apiThreads = 5
                    request.api = 
                    {
                        "effectiveDate" : dateAdjust(data.effDate),
                        "expirationDate" : dateAdjust(data.expDate),
                        "personIdentity" : {
                          "personNumber" : data.pnum
                        },
                        "processor" : data.aval
                    }

                    if (!request.api.expirationDate) delete request.api.expirationDate
                }
            
                return request
            },
            data : [
                ["20326","Adjustment Rule","Test","04/10/2020",""]
            ],
            descHTML: `
            <p>This import is capable of importing persononality assignments.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                  <th>Optional</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
             <tr>
                <tr><th>Person Number</th><td>Text</td><td>12345</td><td></td></tr>
                <tr><th>Assignment Type</th><td>Text</td><td>Adjustment Rule,Cascade Profile,etc..</td><td></td></tr>
                <tr><th>Assignment Value</th><td>Text</td><td>Profile Name</td><td></td></tr>
                <tr><th>Effective Date</th><td>Date</td><td>2020-01-01</td><td></td></tr>
                <tr><th>Expiration Date</th><td>Date</td><td>2020-01-01</td><td></td></tr>

              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            <p>* Expiration Date is optional
            `
        }
    ],
    exportHelpers: {
        execHyperfind: async (hf,dateRange)=>{
            let emps = []
            let execHyperfindAPIUrl = '/v1/commons/hyperfind/execute'
            //console.log('dateRange',dateRange)
            let execPostData = {
                dateRange,
                "hyperfind": {
                  "id": parseInt(hf)
                },
                "threshold": 9999999
            }
            let execHyperfindParams = {url:execHyperfindAPIUrl,pdata:execPostData}
            res = await iwfdAPI.apiRequest(execHyperfindParams)
            res.request.response.result.refs.forEach((e,k)=>{ emps.push(e.qualifier)})
            return emps
        },
        getObjectDotNotation(obj, path) {
            path = path.replace(/\[(\w+)\]/g, '.$1')
            path = path.replace(/^\./, '')
            var a = path.split('.')
            var o = obj
            while (a.length) {
              var n = a.shift()
              if (!(n in o)) return
              o = o[n]
            }
            return o
        },
        setObjectDotNotation(obj, path, value) {
            var schema = obj;  // a moving reference to internal objects within obj
            var pList = path.split('.');
            var len = pList.length;
            for(var i = 0; i < len-1; i++) {
                var elem = pList[i];
                if( !schema[elem] ) schema[elem] = {}
                schema = schema[elem];
            }
        
            schema[pList[len-1]] = value;
        }
    },
    exports: [
        {
            type_name: 'Attendance Balance',                
            prompts: [
                //{label: 'Hyperfind Query',name: 'hyperfind_query',datasource:[{name:'a',id:-1},{name:'b',id:-2}]},
                {label: 'Hyperfind Query',name: 'hyperfind_query',datasource: {apiurl: '/v1/commons/hyperfind/public',tag: 'name',wrap:'hyperfindQueries'}},
                {label: 'Range Type',name:'symbolicPeriod',datasource: [{name:'Today',id:13},{name:'Previous Pay Period',id:0},{name:'Current Pay Period',id:1},{name:'Next Pay Period',id:2},{name:'Range of Dates',id:99}]},
                {label: 'Start Date',name: 'start_date',type:'date',requireToShow: {name:'symbolicPeriod',values:[99]}},
                {label: 'End Date',name: 'end_date',type:'date',requireToShow: {name:'symbolicPeriod',values:[99]}}
            ],
            promptModalAdjust: ()=>{ //ALLOWS FOR ADJUSTMENT OF STOCK RENDERED MODAL (each field in a new form group with label)
                ////$('#exportModal').find('#export__start_date_formgroup').append('<div id="exportDateGrouper" class="pull-right"></div>')
                ////$('#exportModal').find('#export__end_date_formgroup').find('label').detach().appendTo('#exportDateGrouper')
                ////$('#exportModal').find('#export__end_date').detach().appendTo('#exportDateGrouper')
            },
            promptValidation: (fields,et)=>{
                errMsg = ''
                if (!fields['hyperfind_query'].value) errMsg = 'You must select a Hyperfind Query.'
                if (!fields['symbolicPeriod'].value) errMsg = 'You must select a Range Type.'
                if (!fields['start_date'].value || !fields['end_date'].value)
                {
                    if (fields['symbolicPeriod'].value == '99') errMsg = 'You must supply start and end dates.' 
                }
                if (fields['start_date'].value && fields['end_date'].value) {
                    d1 = new Date(fields['start_date'].value)
                    d2 = new Date(fields['end_date'].value)
                    if (d1 > d2) errMsg = 'The start date must be prior to the end date.'
                    diff_in_days = Math.abs((d1 - d2)/86400000)
                    //console.log('diff',diff_in_days)
                    if (diff_in_days > 365) errMsg = "The date range cannot exceed 365 days"
                    
                }
                return errMsg
            },
            apiUrl: '/v1/attendance/balances/multi_read',
            apiRequest: async (data)=> {
                data.symbolicPeriod = String(data.symbolicPeriod)
                x = {
                    "dateRange": {
                        "endDate": data.end_date,
                        "startDate": data.start_date,
                        "symbolicPeriod": {"id": data.symbolicPeriod}
                    },
                    "employees": {"qualifiers": []}
                }
                
                if (data.symbolicPeriod == "" || data.symbolicPeriod == '99') { 
                    delete x.dateRange.symbolicPeriod
                }
                else {delete x.dateRange.startDate; delete x.dateRange.endDate} 
    
                let employees = await QLIPTypes.exportHelpers.execHyperfind(data.hyperfind_query,x.dateRange)
                x.employees.qualifiers = employees
                return x
            },
            apiBatchQty:100,
            apiThreads:5,
            apiBatchOn: 'employees.qualifiers',
            requestToOutput: function(response) {
                headers = ['Person Number','Effective Date','Balance Name','Balance Type','Starting Balance','Ending Balance','Is Changed']
                o = {'headers':headers,data: []}
                response.forEach((r)=>{
                    tmpObj = {}
                    tmpObj.employee = r.employee.qualifier
                    tmpObj.date = r.dateTime.replace('T',' ').substring(0,10)
                    tmpObj.balname = r.name
                    tmpObj.baltype = r.pointBalanceType.qualifier
                    tmpObj.sbal = r.startBalanceAmount
                    tmpObj.ebal = r.endBalanceAmount
                    tmpObj.ischanged = String(r.isChanged).replace('false','').replace('true','True')
                    o.data.push(tmpObj)
                })
                //console.log('exportRequestToOutput',o)
                return o
            },
            descHTML: `
            <p>This export retrieves attendance balance data.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
             <tr><th scope="row">Person Number</th><td>Text</td><td>12345</td></tr>
                <tr><th>Effective Date</th><td>Date</td><td>2020-01-01</td></tr>
                <tr><th>Balance Name</th><td>Text</td><td>Attendance Points</td></tr>
                <tr><th>Balance Type</th><td>Text</td><td>Points,Occurences</td></tr>
                <tr><th>Starting Balance</th><td>Decimal</td><td>8.5</td></tr>
                <tr><th>Ending Balance</th><td>Decimal</td><td>8.5</td></tr>
                <tr><th>Is Changed</th><td>True/False</td><td></td></tr>
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            `
        },
        {
            type_name: 'Profile Field Mappings',    
                       //visible:false,            
                       apiUrl: 'v1/commons/profiles/profile_field_mappings',
                       apiRequest: async (data)=> {
                            var x   
                            return x
                       },
                       requestToOutput: function(response,fdata) {
                           headers = ['Profile Template','Profile Name','ID','Status']
                           o = {'headers':headers,data: []}
                           
                           response.forEach((r)=>{
                               tmpObj = {}
                               tmpObj.pt = r.profileTemplate.qualifier
                               tmpObj.pn = r.name
                               tmpObj.id = r.persistentId
                               tmpObj.status = r.active
                               o.data.push(tmpObj)
                           })

                           return o
                       },
                       descHTML: `
                       <p>This import is capable of exporting Profile Field Mappings.</p>
                       `
                   },
        {
            type_name: 'Punch',    
            //visible:false,            
            prompts: [
                //{label: 'Hyperfind Query',name: 'hyperfind_query',datasource:[{name:'a',id:-1},{name:'b',id:-2}]},
                {label: 'Hyperfind Query',name: 'hyperfind_query',datasource: {apiurl: '/v1/commons/hyperfind/public',tag: 'name',wrap:'hyperfindQueries'}},
                {label: 'Range Type',name:'symbolicPeriod',datasource: [{name:'Today',id:13},{name:'Previous Pay Period',id:0},{name:'Current Pay Period',id:1},{name:'Next Pay Period',id:2},{name:'Range of Dates',id:99}]},
                {label: 'Start Date',name: 'start_date',type:'date',requireToShow: {name:'symbolicPeriod',values:[99]}},
                {label: 'End Date',name: 'end_date',type:'date',requireToShow: {name:'symbolicPeriod',values:[99]}}
            ],
            promptModalAdjust: ()=>{ //ALLOWS FOR ADJUSTMENT OF STOCK RENDERED MODAL (each field in a new form group with label)
                ////$('#exportModal').find('#export__start_date_formgroup').append('<div id="exportDateGrouper" class="pull-right"></div>')
                ////$('#exportModal').find('#export__end_date_formgroup').find('label').detach().appendTo('#exportDateGrouper')
                ////$('#exportModal').find('#export__end_date').detach().appendTo('#exportDateGrouper')
            },
            promptValidation: (fields,et)=>{
                errMsg = ''
                if (!fields['hyperfind_query'].value) errMsg = 'You must select a Hyperfind Query.'
                if (!fields['symbolicPeriod'].value) errMsg = 'You must select a Range Type.'
                if (!fields['start_date'].value || !fields['end_date'].value)
                {
                    if (fields['symbolicPeriod'].value == '99') errMsg = 'You must supply start and end dates.' 
                }
                if (fields['start_date'].value && fields['end_date'].value) {
                    d1 = new Date(fields['start_date'].value)
                    d2 = new Date(fields['end_date'].value)
                    if (d1 > d2) errMsg = 'The start date must be prior to the end date.'
                    diff_in_days = Math.abs((d1 - d2)/86400000)
                    if (diff_in_days > 365) errMsg = "The date range cannot exceed 365 days"
                    
                }
                return errMsg
            },
            apiUrl: 'v1/timekeeping/timecard/multi_read',
            apiRequest: async (data)=> {
                data.symbolicPeriod = String(data.symbolicPeriod)
                data.symbolicPeriod = String(data.symbolicPeriod)
                x = {
                    "select": ["PUNCHES"],
                    "where": {
                        "dateRange": {
                            "endDate": data.start_date,
                            "startDate": data.start_date,
                            "symbolicPeriod": {"id": data.symbolicPeriod}
                        },
                        "employees": {
                            "qualifiers": ["098710"]
                        }
                    }
                }
                
                if (data.symbolicPeriod == "" || data.symbolicPeriod == '99') { 
                    delete x.where.dateRange.symbolicPeriod
                }
                else {delete x.where.dateRange.startDate; delete x.where.dateRange.endDate} 
    
                let employees = await QLIPTypes.exportHelpers.execHyperfind(data.hyperfind_query,x.where.dateRange)
                x.where.employees.qualifiers = employees
                return x
            },
            apiBatchQty:1000,
            apiThreads:10,
            apiBatchOn: 'where.employees.qualifiers',
            requestToOutput: function(response,fdata) {
                headers = ['Person Number','Punch DTM','Override Type']
                o = {'headers':headers,data: []}
                response.forEach((emp)=>{
                    emp.punches.forEach((punch)=>{
                        tmpObj = {}
                        tmpObj.pnum = punch.employee.qualifier
                        tmpObj.pdtm = punch.punchDtm.replace('T',' ')
                        tmpObj.overrideType = ''
                        if (punch.typeOverride) tmpObj.overrideType = punch.typeOverride.name
                        o.data.push(tmpObj)
                    })
                })
                return o

                //headers = ['Person Number','Effective Date','Paycode','Amount','Wages','Job Path']
                //o = {'headers':headers,data: []}
                /*
                response.forEach((r)=>{
                    r.totals.forEach((t)=>{
                        t.aggregatedTotals.forEach((at)=>{
                            tmpObj = {}
                            tmpObj.employee = at.employee.qualifier
                            tmpObj.paycode = at.payCode.name   
                            tmpObj.amount = at.amount
                            tmpObj.wages = at.wages
                            if (at.location) tmpObj.location = at.location.qualifier
                            else tmpObj.location = ""
                            if (at.job) tmpObj.job = at.job.name
                            else tmpObj.job = ""
                            o.data.push(tmpObj)        
                        })
                    })
                })
                */
               /*
               response.forEach((r)=>{
                if (r.hasOwnProperty('dailyActualTotalSummary')) {
                    r.dailyActualTotalSummary.forEach((at)=>{
                        tmpObj = {}
                        tmpObj.employee = at.employee.qualifier
                        tmpObj.effdt = at.applyDate
                        tmpObj.paycode = at.payCode.name   
                        tmpObj.amount = ""
                        if (at.amountType.indexOf('HOUR') > -1) tmpObj.amount = at.durationInHours
                        if (at.amountType.indexOf('DAY') > -1) tmpObj.amount = at.daysAmount
                        tmpObj.wages = at.wages
                        tmpObj.job = ""
                        if (at.job) tmpObj.job = at.job.name
                        o.data.push(tmpObj)      
                    })
                }
                })
                
                return o
                */
               console.log(response)
            },
            descHTML: `
            <p>This import is capable of exporting punches.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                  <th>Optional</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
                <tr><th>Person Number</th><td>Text</td><td>12345</td><td></td></tr>
                <tr><th>Punch DTM</th><td>Datetime</td><td>2019-09-11 08:00</td><td></td></tr>                   
                <tr><th>Override Type</th><td>Text</td><td>In Punch</td><td></td></tr>                   
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            `
        },
        {
            type_name: 'Totals',    
            //visible:false,            
            prompts: [
                //{label: 'Hyperfind Query',name: 'hyperfind_query',datasource:[{name:'a',id:-1},{name:'b',id:-2}]},
                {label: 'Hyperfind Query',name: 'hyperfind_query',datasource: {apiurl: '/v1/commons/hyperfind/public',tag: 'name',wrap:'hyperfindQueries'}},
                {label: 'Range Type',name:'symbolicPeriod',datasource: [{name:'Today',id:13},{name:'Previous Pay Period',id:0},{name:'Current Pay Period',id:1},{name:'Next Pay Period',id:2},{name:'Range of Dates',id:99}]},
                {label: 'Start Date',name: 'start_date',type:'date',requireToShow: {name:'symbolicPeriod',values:[99]}},
                {label: 'End Date',name: 'end_date',type:'date',requireToShow: {name:'symbolicPeriod',values:[99]}}
            ],
            promptModalAdjust: ()=>{ //ALLOWS FOR ADJUSTMENT OF STOCK RENDERED MODAL (each field in a new form group with label)
                //$('#exportModal').find('#export__start_date_formgroup').append('<div id="exportDateGrouper" class="pull-right"></div>')
                //$('#exportModal').find('#export__end_date_formgroup').find('label').detach().appendTo('#exportDateGrouper')
                //$('#exportModal').find('#export__end_date').detach().appendTo('#exportDateGrouper')
            },
            promptValidation: (fields,et)=>{
                errMsg = ''
                if (!fields['hyperfind_query'].value) errMsg = 'You must select a Hyperfind Query.'
                if (!fields['symbolicPeriod'].value) errMsg = 'You must select a Range Type.'
                if (!fields['start_date'].value || !fields['end_date'].value)
                {
                    if (fields['symbolicPeriod'].value == '99') errMsg = 'You must supply start and end dates.' 
                }
                if (fields['start_date'].value && fields['end_date'].value) {
                    d1 = new Date(fields['start_date'].value)
                    d2 = new Date(fields['end_date'].value)
                    if (d1 > d2) errMsg = 'The start date must be prior to the end date.'
                    diff_in_days = Math.abs((d1 - d2)/86400000)
                    if (diff_in_days > 365) errMsg = "The date range cannot exceed 365 days"
                    
                }
                return errMsg
            },
            apiUrl: 'v1/timekeeping/timecard_metrics/multi_read',
            apiRequest: async (data)=> {
                data.symbolicPeriod = String(data.symbolicPeriod)
                data.symbolicPeriod = String(data.symbolicPeriod)
                x = {
                    "select": ["DAILY_ACTUAL_TOTAL_SUMMARY"],
                    "where": {
                        "employeeSet": {
                            "dateRange": {
                                "endDate": data.start_date,
                                "startDate": data.start_date,
                                "symbolicPeriod": {"id": data.symbolicPeriod}
                            },
                            "employees": {
                                "qualifiers": ["098710"]
                            }
                        }
                    }
                }
                
                if (data.symbolicPeriod == "" || data.symbolicPeriod == '99') { 
                    delete x.where.employeeSet.dateRange.symbolicPeriod
                }
                else {delete x.where.employeeSet.dateRange.startDate; delete x.where.employeeSet.dateRange.endDate} 
    
                let employees = await QLIPTypes.exportHelpers.execHyperfind(data.hyperfind_query,x.where.employeeSet.dateRange)
                x.where.employeeSet.employees.qualifiers = employees
                return x
            },
            apiBatchQty:1000,
            apiThreads:10,
            apiBatchOn: 'where.employeeSet.employees.qualifiers',
            requestToOutput: function(response,fdata) {
                headers = ['Person Number','Effective Date','Paycode','Amount','Wages','Job Path']
                o = {'headers':headers,data: []}
                /*
                response.forEach((r)=>{
                    r.totals.forEach((t)=>{
                        t.aggregatedTotals.forEach((at)=>{
                            tmpObj = {}
                            tmpObj.employee = at.employee.qualifier
                            tmpObj.paycode = at.payCode.name   
                            tmpObj.amount = at.amount
                            tmpObj.wages = at.wages
                            if (at.location) tmpObj.location = at.location.qualifier
                            else tmpObj.location = ""
                            if (at.job) tmpObj.job = at.job.name
                            else tmpObj.job = ""
                            o.data.push(tmpObj)        
                        })
                    })
                })
                */
               response.forEach((r)=>{
                if (r.hasOwnProperty('dailyActualTotalSummary')) {
                    r.dailyActualTotalSummary.forEach((at)=>{
                        tmpObj = {}
                        tmpObj.employee = at.employee.qualifier
                        tmpObj.effdt = at.applyDate
                        tmpObj.paycode = at.payCode.name   
                        tmpObj.amount = ""
                        if (at.amountType.indexOf('HOUR') > -1) tmpObj.amount = at.durationInHours
                        if (at.amountType.indexOf('DAY') > -1) tmpObj.amount = at.daysAmount
                        tmpObj.wages = at.wages
                        tmpObj.job = ""
                        if (at.job) tmpObj.job = at.job.name
                        o.data.push(tmpObj)      
                    })
                }
                })
                
                return o
            },
            descHTML: `
            <p>This export retrieves totals data.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
              
                <tr><th scope="row">Person Number</th><td>Text</td><td>12345</td></tr>
                <tr><th>Effective Date</th><td>Date</td><td>2020-01-01</td></tr>
                <tr><th>Paycode</th><td>Text</td><td>Regular</td></tr>
                <tr><th>Amount</th><td>Decimal</td><td>8.5</td></tr>
                <tr><th>Wages</th><td>Decimal</td><td>85</td></tr>
                <tr><th>Job Path</th><td>Text</td><td>./Organization/Path/To/Job</td><td>Y</td></tr> 
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            `
        },
        {
            type_name: 'Accrual Balance',                
            prompts: [
                //{label: 'Hyperfind Query',name: 'hyperfind_query',datasource:[{name:'a',id:-1},{name:'b',id:-2}]},
                {label: 'Hyperfind Query',name: 'hyperfind_query',datasource: {apiurl: '/v1/commons/hyperfind/public',tag: 'name',wrap:'hyperfindQueries'}},
                //{label: 'Range Type',name:'symbolicPeriod',datasource: [{name:'Today',id:13},{name:'Previous Pay Period',id:0},{name:'Current Pay Period',id:1},{name:'Next Pay Period',id:2},{name:'Range of Dates',id:99}]},
                {label: 'Range Type',name:'symbolicPeriod',datasource: [{name:'Today',id:13},{name:'Specific Date',id:99}]},
                {label: 'Effective Date',name: 'start_date',type:'date',requireToShow: {name:'symbolicPeriod',values:[99]}},
            ],
            promptModalAdjust: ()=>{ //ALLOWS FOR ADJUSTMENT OF STOCK RENDERED MODAL (each field in a new form group with label)
                //$('#exportModal').find('#export__start_date_formgroup').append('<div id="exportDateGrouper" class="pull-right"></div>')
                ////$('#exportModal').find('#export__end_date_formgroup').find('label').detach().appendTo('#exportDateGrouper')
                ////$('#exportModal').find('#export__end_date').detach().appendTo('#exportDateGrouper')
            },
            promptValidation: (fields,et)=>{
                errMsg = ''
                if (!fields['hyperfind_query'].value) errMsg = 'You must select a Hyperfind Query.'
                if (!fields['symbolicPeriod'].value) errMsg = 'You must select a Range Type.'
                //if (!fields['start_date'].value || !fields['end_date'].value)
                if (!fields['start_date'].value)
                {
                    if (fields['symbolicPeriod'].value == '99') errMsg = 'You must supply an effective date.' 
                }
                /*
                if (fields['start_date'].value && fields['end_date'].value) {
                    d1 = new Date(fields['start_date'].value)
                    d2 = new Date(fields['end_date'].value)
                    if (d1 > d2) errMsg = 'The start date must be prior to the end date.'
                    diff_in_days = Math.abs((d1 - d2)/86400000)
                    //console.log('diff',diff_in_days)
                    if (diff_in_days > 365) errMsg = "The date range cannot exceed 365 days"
                }
                */
                return errMsg
            },
            apiUrl: 'v1/timekeeping/timecard_metrics/multi_read',
            apiRequest: async (data)=> {
                data.symbolicPeriod = String(data.symbolicPeriod)
                x = {
                    "select": ["ACCRUAL_SUMMARY","ACCRUAL_TRANSACTIONS"],
                    "where": {
                        "employeeSet": {
                            "dateRange": {
                                "endDate": data.start_date,
                                "startDate": data.start_date,
                                "symbolicPeriod": {"id": data.symbolicPeriod}
                            },
                            "employees": {
                                "qualifiers": ["098710"]
                            }
                        }
                    }
                }
                
                if (data.symbolicPeriod == "" || data.symbolicPeriod == '99') { 
                    delete x.where.employeeSet.dateRange.symbolicPeriod
                }
                else {delete x.where.employeeSet.dateRange.startDate; delete x.where.employeeSet.dateRange.endDate} 
    
                let employees = await QLIPTypes.exportHelpers.execHyperfind(data.hyperfind_query,x.where.employeeSet.dateRange)
                x.where.employeeSet.employees.qualifiers = employees
                return x
            },
            apiBatchQty:100,
            apiThreads:5,
            apiBatchOn: 'where.employeeSet.employees.qualifiers',
            requestToOutput: function(response) {
                headers = ['Transaction Type','Person Number','Effective Date','Accrual Code','Amount']
                o = {'headers':headers,data: []}

                response.forEach((r)=>{
                    if (r.hasOwnProperty('accrualSummaryData')) {
                        r.accrualSummaryData.forEach((ad)=>{
                            tmpObj = {}
                            tmpObj.trans_type = 'Balance'
                            tmpObj.employee = ad.employee.qualifier
                            tmpObj.date = ad.endDateTime.replace('T',' ').substring(0,10)
                            tmpObj.accrualCode = ad.accrualCode.name
                            kys = Object.keys(ad.dailySummaries[0].currentBalance)
                            balanceTypeName = kys.find(element => element.indexOf('vest') > -1);
                            if (kys) {
                                tmpObj.balance = ad.dailySummaries[0].currentBalance[balanceTypeName]
                                o.data.push(tmpObj)    
                            }       
                        })
                    }
                    if (r.hasOwnProperty('accrualTransactions')) {
                        r.accrualTransactions.forEach((ad)=>{
                            if (ad.type == 'GRANT') {
                                tmpObj = {}
                                tmpObj.trans_type = 'Grant'
                                tmpObj.employee = ad.employee.qualifier
                                tmpObj.date = ad.effectiveDate
                                tmpObj.accrualCode = ad.accrualCode.name
                                tmpObj.amount = ''
                                if (ad.hasOwnProperty('hoursAmount')) tmpObj.amount = ad.hoursAmount
                                if (ad.hasOwnProperty('daysAmount')) tmpObj.amount = ad.daysAmount
                                if (ad.hasOwnProperty('moneyAmount')) tmpObj.amount = ad.moneyAmount
                                o.data.push(tmpObj)    
                            }
                        })
                    }
                })
                //console.log('exportRequestToOutput',o)
                return o
            },
            descHTML: `
            <p>This export retrieves accrual balance data.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
              <tr><th scope="row">Transaction Type Number</th><td>Text</td><td>Balance,Grant</td></tr>
                <tr><th>Person Number</th><td>Text</td><td>12345</td></tr>
                <tr><th>Effective Date</th><td>Date</td><td>2020-01-01</td></tr>
                <tr><th>Accrual Code Name</th><td>Text</td><td>Attendance Points</td></tr>
                <tr><th>Amount</th><td>Decimal</td><td>8.5</td></tr>
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            `
        },
        {
            type_name: 'Attendance Event',                
            prompts: [
                //{label: 'Hyperfind Query',name: 'hyperfind_query',datasource:[{name:'a',id:-1},{name:'b',id:-2}]},
                {label: 'Hyperfind Query',name: 'hyperfind_query',datasource: {apiurl: '/v1/commons/hyperfind/public',tag: 'name',wrap:'hyperfindQueries'}},
                {label: 'Range Type',name:'symbolicPeriod',datasource: [{name:'Today',id:13},{name:'Previous Pay Period',id:0},{name:'Current Pay Period',id:1},{name:'Next Pay Period',id:2},{name:'Range of Dates',id:99}]},
                {label: 'Start Date',name: 'start_date',type:'date',requireToShow: {name:'symbolicPeriod',values:[99]}},
                {label: 'End Date',name: 'end_date',type:'date',requireToShow: {name:'symbolicPeriod',values:[99]}}
            ],
            promptModalAdjust: ()=>{ //ALLOWS FOR ADJUSTMENT OF STOCK RENDERED MODAL (each field in a new form group with label)
                //$('#exportModal').find('#export__start_date_formgroup').append('<div id="exportDateGrouper" class="pull-right"></div>')
                //$('#exportModal').find('#export__end_date_formgroup').find('label').detach().appendTo('#exportDateGrouper')
                //$('#exportModal').find('#export__end_date').detach().appendTo('#exportDateGrouper')
            },
            promptValidation: (fields,et)=>{
                errMsg = ''
                if (!fields['hyperfind_query'].value) errMsg = 'You must select a Hyperfind Query.'
                if (!fields['symbolicPeriod'].value) errMsg = 'You must select a Range Type.'
                if (!fields['start_date'].value || !fields['end_date'].value)
                {
                    if (fields['symbolicPeriod'].value == '99') errMsg = 'You must supply start and end dates.' 
                }
                if (fields['start_date'].value && fields['end_date'].value) {
                    d1 = new Date(fields['start_date'].value)
                    d2 = new Date(fields['end_date'].value)
                    if (d1 > d2) errMsg = 'The start date must be prior to the end date.'
                    diff_in_days = Math.abs((d1 - d2)/86400000)
                    //console.log('diff',diff_in_days)
                    if (diff_in_days > 365) errMsg = "The date range cannot exceed 365 days"
                    
                }
                return errMsg
            },
            apiUrl: '/v1/attendance/events/multi_read',
            apiRequest: async (data)=> {
                data.symbolicPeriod = String(data.symbolicPeriod)
                x = {
                    "dateRange": {
                        "endDate": data.end_date,
                        "startDate": data.start_date,
                        "symbolicPeriod": {"id": data.symbolicPeriod}
                    },
                    "employees": {"qualifiers": []}
                }
                
                if (data.symbolicPeriod == "" || data.symbolicPeriod == '99') { 
                    delete x.dateRange.symbolicPeriod
                }
                else {delete x.dateRange.startDate; delete x.dateRange.endDate} 
    
                let employees = await QLIPTypes.exportHelpers.execHyperfind(data.hyperfind_query,x.dateRange)
                x.employees.qualifiers = employees
                return x
            },
            apiBatchQty:100,
            apiThreads:5,
            apiBatchOn: 'employees.qualifiers',
            requestToOutput: function(response) {
                headers = ['Person Number','Effective Date','Event Name','Event Time','Event Type','Amount','ID']
                o = {'headers':headers,data: []}
                response.forEach((r)=>{
                    //console.log(r)
                    tmpObj = {}
                    tmpObj.employee = r.employee.qualifier
                    tmpObj.date = r.applyDate
                    tmpObj.eventName = r.eventDefinition.qualifier
                    tmpObj.eventTime = r.eventTime
                    tmpObj.eventType = r.type.displayName
                    tmpObj.amount = r.amount
                    tmpObj.id = r.id
                    o.data.push(tmpObj)
                })
                //console.log('exportRequestToOutput',o)
                return o
            },
            descHTML: `
            <p>This export retrieves attendance event data.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
             <tr><th scope="row">Person Number</th><td>Text</td><td>12345</td></tr>
                <tr><th>Effective Date</th><td>Date</td><td>2020-01-01</td></tr>
                <tr><th>Event Name</th><td>Text</td><td>Attendance Points</td></tr>
                <tr><th>Event Time</th><td>Text</td><td>08:00</td></tr>
                <tr><th>Event Type</th><td>Text</td><td>Points,Occurences</td></tr>
                <tr><th>Amount</th><td>Decimal</td><td>8.5</td></tr>
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            `
        },
        {
            type_name: 'Pay Code Edit',                
            prompts: [
                //{label: 'Hyperfind Query',name: 'hyperfind_query',datasource:[{name:'a',id:-1},{name:'b',id:-2}]},
                {label: 'Hyperfind Query',name: 'hyperfind_query',datasource: {apiurl: '/v1/commons/hyperfind/public',tag: 'name',wrap:'hyperfindQueries'}},
                {label: 'Range Type',name:'symbolicPeriod',datasource: [{name:'Today',id:13},{name:'Previous Pay Period',id:0},{name:'Current Pay Period',id:1},{name:'Next Pay Period',id:2},{name:'Range of Dates',id:99}]},
                {label: 'Start Date',name: 'start_date',type:'date',requireToShow: {name:'symbolicPeriod',values:[99]}},
                {label: 'End Date',name: 'end_date',type:'date',requireToShow: {name:'symbolicPeriod',values:[99]}}
            ],
            promptModalAdjust: ()=>{ //ALLOWS FOR ADJUSTMENT OF STOCK RENDERED MODAL (each field in a new form group with label)
                //$('#exportModal').find('#export__start_date_formgroup').append('<div id="exportDateGrouper" class="pull-right"></div>')
                //$('#exportModal').find('#export__end_date_formgroup').find('label').detach().appendTo('#exportDateGrouper')
                //$('#exportModal').find('#export__end_date').detach().appendTo('#exportDateGrouper')
            },
            promptValidation: (fields,et)=>{
                errMsg = ''
                if (!fields['hyperfind_query'].value) errMsg = 'You must select a Hyperfind Query.'
                if (!fields['symbolicPeriod'].value) errMsg = 'You must select a Range Type.'
                if (!fields['start_date'].value || !fields['end_date'].value)
                {
                    if (fields['symbolicPeriod'].value == '99') errMsg = 'You must supply start and end dates.' 
                }
                if (fields['start_date'].value && fields['end_date'].value) {
                    d1 = new Date(fields['start_date'].value)
                    d2 = new Date(fields['end_date'].value)
                    if (d1 > d2) errMsg = 'The start date must be prior to the end date.'
                    diff_in_days = Math.abs((d1 - d2)/86400000)
                    //console.log('diff',diff_in_days)
                    if (diff_in_days > 365) errMsg = "The date range cannot exceed 365 days"
                    
                }
                return errMsg
            },
            apiUrl: '/v1/timekeeping/timecard/multi_read',
            apiRequest: async (data)=> {
                data.symbolicPeriod = String(data.symbolicPeriod)
                x = 
                {
                    "select": ["PAYCODE_EDITS"],
                    "where": {
                        "dateRange": {
                            "endDate": data.end_date,
                            "startDate": data.start_date,
                            "symbolicPeriod": {"id": data.symbolicPeriod}
                        },
                        "employees": {"qualifiers": []}
                    }
                }
                
                if (data.symbolicPeriod == "" || data.symbolicPeriod == '99') { 
                    delete x.dateRange.symbolicPeriod
                }
                else {delete x.where.dateRange.startDate; delete x.where.dateRange.endDate} 
    
                let employees = await QLIPTypes.exportHelpers.execHyperfind(data.hyperfind_query,x.where.dateRange)
                x.where.employees.qualifiers = employees
                return x
            },
            apiBatchQty:100,
            apiThreads:5,
            apiBatchOn: 'where.employees.qualifiers',
            requestToOutput: function(response) {
                headers = ["Person Number","Apply Date","Start Time","Paycode","Amount","Type","Job Path","Cost Center","Labor Categories","Transaction ID"]
                o = {'headers':headers,data: []}

                response.forEach((r)=>{
                    if (r.hasOwnProperty('payCodeEdits')) {
                        r.payCodeEdits.forEach((pce)=>{
                            tmpObj = {}
                            tmpObj.pnum = pce.employee.qualifier
                            tmpObj.applydate = pce.applyDate
                            tmpObj.stime = ''
                            if (pce.hasOwnProperty('startDateTime')) tmpObj.stime = left(right(pce.startDateTime,8),5)
                            tmpObj.paycode = pce.paycode.qualifier

                            kys = Object.keys(pce)
                            amountFieldName = kys.find(element => element.indexOf('duration') > -1);
                            tmpObj.amount = pce[amountFieldName]
                            tmpObj.type = pce.amountType
                            tmpObj.jobpath = ''
                            tmpObj.costcenter = ''
                            tmpObj.laborcats = ''
                            tmpObj.id = pce.id

                            o.data.push(tmpObj)

                        })
                    }
                })

                /*response.forEach((r)=>{
                    tmpObj = {}
                    tmpObj.employee = r.employee.qualifier
                    tmpObj.date = r.dateTime.replace('T',' ').substring(0,10)
                    tmpObj.balname = r.name
                    tmpObj.baltype = r.pointBalanceType.qualifier
                    tmpObj.sbal = r.startBalanceAmount
                    tmpObj.ebal = r.endBalanceAmount
                    tmpObj.ischanged = String(r.isChanged).replace('false','').replace('true','True')
                    o.data.push(tmpObj)
                })
                */
                return o
            },
            descHTML: `
            <p>This export is capable of exporting pay code edits.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                  <th>Optional</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
                <tr><th scope="row">Person Number</th><td>Text</td><td>12345</td><td></td></tr>
                <tr><th>Apply Date</th><td>Date</td><td>2020-01-01</td><td></td></tr>                   
                <tr><th>Start Time</th><td>Time</td><td>08:00</td><td></td></tr>                   
                <tr><th>Paycode</th><td>Text</td><td>Regular</td><td></td></tr>   
                <tr><th>Type</th><td>Text</td><td>Hour,Day,Money</td><td>Y</td></tr> 
                <tr><th>Job Path</th><td>Text</td><td>./Organization/Path/To/Job</td><td>Y</td></tr> 
                <tr><th>Workrule</th><td>Text</td><td>Non-Exempt Union</td><td>Y</td></tr> 
                <tr><th>Cost Center</th><td>Text</td><td>CC1</td><td>Y</td></tr> 
                <tr><th>Labor Categories</th><td>Text</td><td>Contract Code</td><td>Y</td></tr> 
                <tr><th>Transaction ID</th><td>Text</td><td>1234</td><td>Y</td></tr> 
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            `
        },
        {
            type_name: 'Leave Case',                
            prompts: [
                //{label: 'Hyperfind Query',name: 'hyperfind_query',datasource:[{name:'a',id:-1},{name:'b',id:-2}]},
                {label: 'Hyperfind Query',name: 'hyperfind_query',datasource: {apiurl: '/v1/commons/hyperfind/public',tag: 'name',wrap:'hyperfindQueries'}},
                {label: 'Range Type',name:'symbolicPeriod',datasource: [{name:'Today',id:13},{name:'Previous Pay Period',id:0},{name:'Current Pay Period',id:1},{name:'Next Pay Period',id:2},{name:'Range of Dates',id:99}]},
                {label: 'Start Date',name: 'start_date',type:'date',requireToShow: {name:'symbolicPeriod',values:[99]}},
                {label: 'End Date',name: 'end_date',type:'date',requireToShow: {name:'symbolicPeriod',values:[99]}}
            ],
            promptModalAdjust: ()=>{ //ALLOWS FOR ADJUSTMENT OF STOCK RENDERED MODAL (each field in a new form group with label)
                //$('#exportModal').find('#export__start_date_formgroup').append('<div id="exportDateGrouper" class="pull-right"></div>')
                //$('#exportModal').find('#export__end_date_formgroup').find('label').detach().appendTo('#exportDateGrouper')
                //$('#exportModal').find('#export__end_date').detach().appendTo('#exportDateGrouper')
            },
            promptValidation: (fields,et)=>{
                errMsg = ''
                if (!fields['hyperfind_query'].value) errMsg = 'You must select a Hyperfind Query.'
                if (!fields['symbolicPeriod'].value) errMsg = 'You must select a Range Type.'
                if (!fields['start_date'].value || !fields['end_date'].value)
                {
                    if (fields['symbolicPeriod'].value == '99') errMsg = 'You must supply start and end dates.' 
                }
                if (fields['start_date'].value && fields['end_date'].value) {
                    d1 = new Date(fields['start_date'].value)
                    d2 = new Date(fields['end_date'].value)
                    if (d1 > d2) errMsg = 'The start date must be prior to the end date.'
                    diff_in_days = Math.abs((d1 - d2)/86400000)
                    //console.log('diff',diff_in_days)
                    if (diff_in_days > 365) errMsg = "The date range cannot exceed 365 days"
                    
                }
                return errMsg
            },
            apiUrl: '/v1/leave/leave_cases/multi_read',
            apiRequest: async (data)=> {
                data.symbolicPeriod = String(data.symbolicPeriod)
                x = {
                    "dateRange": {
                        "endDate": data.end_date,
                        "startDate": data.start_date,
                        "symbolicPeriod": {"id": data.symbolicPeriod}
                    },
                    "employees": {"qualifiers": []}
                }

                x = {
                    //"caseStatus": {"qualifier": "QUALIFIER"},
                    "dateRange": {
                      "endDate": data.end_date,
                      "startDate": data.start_date,
                      "symbolicPeriod": {"id": data.symbolicPeriod}
                    },
                    "employees": {"qualifiers": []}
                  }
                
                if (data.symbolicPeriod == "" || data.symbolicPeriod == '99') { 
                    delete x.dateRange.symbolicPeriod
                }
                else {delete x.dateRange.startDate; delete x.dateRange.endDate} 
    
                let employees = await QLIPTypes.exportHelpers.execHyperfind(data.hyperfind_query,x.dateRange)
                x.employees.qualifiers = employees
                return x
            },
            apiBatchQty:100,
            apiThreads:5,
            apiBatchOn: 'employees.qualifiers',
            requestToOutput: function(response) {
                headers = ["Person Number","Frequency Type","Start Date","End Date","Request Date","Case Code","Category Name","Reason","Case Status","Case Approval Name","External ID"]
                o = {'headers':headers,data: []}
                response.forEach((r)=>{
                    tmpObj = {}
                    tmpObj.employee = r.employee.qualifier
                    //tmpObj.frequency = r.frequency.name.replace('T',' ').substring(0,10)
                    tmpObj.frequency = r.frequency.name
                    tmpObj.sdate = r.startDate
                    tmpObj.edate = ""
                    if (r.hasOwnProperty('endDate')) tmpObj.edate = r.endDate
                    tmpObj.rdate = r.requestDate
                    tmpObj.casecode = r.caseCode
                    tmpObj.catname = r.category.name
                    tmpObj.reason = r.reason.name
                    tmpObj.caseStatus = r.status.name
                    tmpObj.approvalStatus = r.approvalStatus.name
                    tmpObj.externalid = r.code
                    o.data.push(tmpObj)
                })
                //console.log('exportRequestToOutput',o)
                return o
            },
            descHTML: `
            <p>This type is capable of exporting leave cases</p>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                  <th>Optional</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
                <tr><td scope="row">Person Number</td><td>Text</td><td>12345</td><td></td></tr>
                <tr><td>Frequency Type</td><td>Text</td><td>Continuous,Intermittent</td><td></td></tr>                   
                <tr><td>Start Date</td><td>Date</td><td>2020-01-01</td><td></td></tr>                   
                <tr><td>End Date</td><td>Date</td><td>2020-01-01</td><td></td></tr>                   
                <tr><td>Request Date</td><td>Date</td><td>2020-01-01</td><td></td></tr>                   
                <tr><td>Case Code</td><td>Text</td><td>C100</td><td></td></tr>
                <tr><td>Category Name</td><td>Text</td><td>Family</td><td></td></tr>
                <tr><td>Reason</td><td>Text</td><td>Spouse</td><td></td></tr>
                <tr><td>Case Status</td><td>Text</td><td>Open,Closed</td><td></td></tr>
                <tr><td>Case Approval Name</td><td>Text</td><td>Pending</td><td></td></tr>
                <tr><td>External ID</td><td>Number</td><td>456</td><td></td></tr>   
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            `
        },
        {
            type_name: 'Person',  
            //visible:true,              
            prompts: [
                //{label: 'Hyperfind Query',name: 'hyperfind_query',datasource:[{name:'a',id:-1},{name:'b',id:-2}]},
                {label: 'Hyperfind Query',name: 'hyperfind_query',datasource: {apiurl: '/v1/commons/hyperfind/public',tag: 'name',wrap:'hyperfindQueries'}},
                {label: 'Snapshot Date',name: 'start_date',type:'date'},
                {label: 'Person Number Filter (Optional)',name: 'filter',info:"Example: (123* | *123 | 123456) * as wildcard"}
            ],
            promptModalAdjust: ()=>{ //ALLOWS FOR ADJUSTMENT OF STOCK RENDERED MODAL (each field in a new form group with label)
                $('#exportModal').find('#export__hyperfind_query')[0].selectize.setValue('1')
                $('#exportModal').find('#export__start_date').data('datepicker').setDate(new Date())
                //$('#exportModal').find('#export__filter_formgroup').append('<span></span>')
                ////$('#exportModal').find('#export__start_date_formgroup').append('<div id="exportDateGrouper" class="pull-right"></div>')
                ////$('#exportModal').find('#export__end_date_formgroup').find('label').detach().appendTo('#exportDateGrouper')
                ////$('#exportModal').find('#export__end_date').detach().appendTo('#exportDateGrouper')
            },
            promptValidation: (fields,et)=>{
                errMsg = ''
                if (!fields['hyperfind_query'].value) errMsg = 'You must select a Hyperfind Query.'                
                if (!fields['start_date'].value) errMsg = 'You must supply a snapshot date.' 
                return errMsg
            },
            apiUrl: '/v1/commons/persons/extensions/multi_read/',
            apiRequest: async (data)=> {
                data.symbolicPeriod = String(data.symbolicPeriod)

                x = {
                    "where" : {
                      "employees" : {
                        "key" : "personnumber",
                        "values" : []
                      },
                      "snapshotDate" : data.start_date
                    }
                  }
                
                dr = {'startDate':data.start_date,'endDate':data.start_date}
                //console.log('dr',dr)
    
                let employees = await QLIPTypes.exportHelpers.execHyperfind(data.hyperfind_query,dr)
                x.where.employees.values = employees
                return x
            },
            apiBatchQty:100,
            apiThreads:5,
            apiBatchOn: 'where.employees.values',
            requestToOutput: function(response,formData) {
                
                //OUTPUT HELPERS
                function getEffVersion(dateArr,effDate,allRecords)
                {
                    outArr = []
                    if (!effDate) effDate = new Date()
                    else {
                        effDate = effDate
                        effDate = new Date(effDate.substring(0,4),effDate.substring(5,7),effDate.substring(8,10))
                    }
                    if (dateArr) {
                        dateArr.forEach((v,k)=>{
                            DeffDt = v.effectiveDate
                            DexpDt = v.expirationDate
                            DeffDt = new Date(DeffDt.substring(0,4),DeffDt.substring(5,7),DeffDt.substring(8,10))
                            DexpDt = new Date(DexpDt.substring(0,4),DexpDt.substring(5,7),DexpDt.substring(8,10))
                            //console.log('DeffDt',DeffDt)
                            //console.log('DexpDt',DexpDt)
                            //console.log('effDate',effDate)
                            if (DeffDt <= effDate && DexpDt > effDate) outArr.push(v)
                        })
                    }
                    if (allRecords) return outArr
                    else return outArr[0]
                }

                function determineLicenses(l) {
                    lmap = {
                        'TimekeepingLicense':'',
                        'SchedulingLicense':'',
                        'WAMLicense':'',
                        'AnalyticsLicense':'',
                        'WorkLicense':'',
                        'EmployeeLicense':'',
                        'ManagerLicense':''
                    }

                    tkArr = ['Hourly Timekeeping','Salaried Timekeeping']
                    shArr = ['Scheduling','Advanced Scheduling','Advanced Planning and Scheduling','TeleStaff','Optimized Scheduling with Forecasting']
                    wmArr = ['Accruals & Leave','Absence','Accruals']
                    wfArr = ['Analytics','Analytics & Healthcare Analytics']
                    wkArr = ['Work']
                    emArr = ['Employee']
                    mgArr = ['Manager']
                    if (l) {
                        l.forEach((v,k)=>{
                            if (tkArr.indexOf(v.licenseType) > -1) lmap.TimekeepingLicense = tkArr[tkArr.indexOf(v.licenseType)]
                            if (shArr.indexOf(v.licenseType) > -1) lmap.SchedulingLicense = shArr[shArr.indexOf(v.licenseType)]
                            if (wmArr.indexOf(v.licenseType) > -1) lmap.WAMLicense = wmArr[wmArr.indexOf(v.licenseType)]
                            if (wfArr.indexOf(v.licenseType) > -1) lmap.AnalyticsLicense = wfArr[wfArr.indexOf(v.licenseType)]
                            if (wkArr.indexOf(v.licenseType) > -1) lmap.WorkLicense = wkArr[wkArr.indexOf(v.licenseType)]
                            if (emArr.indexOf(v.licenseType) > -1) lmap.EmployeeLicense = emArr[emArr.indexOf(v.licenseType)]
                            if (mgArr.indexOf(v.licenseType) > -1) lmap.ManagerLicense = mgArr[mgArr.indexOf(v.licenseType)]
                        })
                    }
                    return lmap
                }
                
                headers = ['Person Number*','First Name','Last Name*','Short Name','Middle Initial','Hire Date','Employment Status','Employment Status Eff Date','User Account Status','User Account Status Eff Date','Authentication Type','Username','Password','Logon Profile','MFA Required?','Analytics Type','Analytics Type Eff Date','Phone Label','Phone Number','Phone SMS?','State','City','Zip Code','Country','Worker Type','Email','Expected Daily Hours','Expected Weekly Hours','Expected PP Hours','Schedule Group','Display Profile','Function Access Profile','Time Zone','Locale','Notification Profile','Pay Rule*','Pay Rule Eff Date','Primary Job*','Labor Category','Primary Labor Eff Date','Employment Term','Employment Eff Date','Accrual Profile','Accrual Profile Eff Date','Full Time %','Full Time Hours','Full Time Employee Hours','Full Time Eq. Eff Date','Wage Rate','Wage Eff Date','Wage Currency','Reports To ID','Device Group','Badge Number','Badge Number Eff Date','Work Profile Name','Default Activity','Idle Activity','Paid Activity','Unpaid Activity','Time Entry Method','Time Entry Eff Date','Employee Pay Code Profile','Employee Work Rule Profile','Employee Labor Category Profile','Labor Category Manager Additions','GDAP','GDAP Eff Date','Person Access Eff Date','PA - Employee JTS','PA - JTS Manager Additions','PA - Organisational Set','PA - Employee Group','PA - Home Hyperfind','Manager Work Rule','Manager Pay Code Edit','Manager Pay Code View','Manager Labor Category Profile','Pattern Template Profile','Shift Template Profile','Schedule Group Profile','Report Profile','Forecasting Category Profile','Delegate Profile','Manager Currency Preference','Timekeeping License','Scheduling License','WAM License','Analytics License','Work License','Employee License','Manager License','Custom Data Names','Custom Data Values','Custom Date Names','Custom Date Values','Override Effective Date']
                o = {'headers':headers,data: []}
                response.forEach((r)=>{
                    tmp = {}
                    
                    snapshotDate = formData.start_date.value

                    tmp.personNumber = r.allExtension.employeeExtension.personNumber
                    tmp.firstName = r.allExtension.employeeExtension.firstName
                    tmp.lastName = r.allExtension.employeeExtension.lastName
                    tmp.shortName = r.allExtension.employeeExtension.shortName
                    tmp.middleName = r.allExtension.employeeExtension.middleName
                    tmp.hireDate = r.allExtension.employeeExtension.hireDate
                    
                    tmp.empStatus = ''
                    tmp.empStatusEffDt = ''
                    eStatus = getEffVersion(r.allExtension.employeeExtension.employmentStatuses,snapshotDate)
                    if (eStatus) {
                        tmp.empStatus = eStatus.employmentStatus
                        tmp.empStatusEffDt = eStatus.effectiveDate
                    }
                    
                    userStatus = getEffVersion(r.allExtension.employeeExtension.effDatedAccountStatuses,snapshotDate)
                    if (userStatus) {
                        tmp.userAccountStatus = userStatus.accountStatus
                        tmp.userAccountStatusEffDt = userStatus.effectiveDate
                    }
                    
                    tmp.authType = r.allExtension.employeeExtension.authenticationType
                    tmp.Username = r.allExtension.employeeExtension.userName
                    tmp.Password = ""
                    tmp.logonProfile = r.allExtension.employeeExtension.logonProfile
                    if (r.allExtension.employeeExtension.mfaRequired) tmp.mfa = 'True'
                    else tmp.mfa = 'False'
                    tmp.wfanType = ""                    //tmp.Analytics Type
                    tmp.wfanTypeEffDt = ""
                    
                    telContactDataEntries = r.allExtension.employeeExtension.telContactDataEntries
                    tmp.p1label = ''
                    tmp.p1num = ''
                    tmp.p1sms = ''
                    if (telContactDataEntries) {
                        tmp.p1label = telContactDataEntries[0].contactType
                        tmp.p1num = telContactDataEntries[0].contactData
                        if (telContactDataEntries[0].smsswitch) tmp.p1sms = 'True'
                        else tmp.p1sms = 'False'
                    }
                    
                    postalAddressDataEntries = r.allExtension.employeeExtension.postalAddressDataEntries
                    tmp.State = ''
                    tmp.City = ''
                    tmp.zip = ''
                    tmp.country = ''
                    if (postalAddressDataEntries) {
                        tmp.State = postalAddressDataEntries[0].state
                        tmp.City = postalAddressDataEntries[0].city
                        tmp.zip = postalAddressDataEntries[0].zipCode
                        tmp.country = postalAddressDataEntries[0].country
                    }
                    
                    tmp.workerType = r.allExtension.schedulingExtension.workerType

                    emailContactDataEntries = r.allExtension.employeeExtension.emailContactDataEntries
                    if (emailContactDataEntries) tmp.Email = emailContactDataEntries[0].contactData
                    else tmp.Email = ''
                    
                    tmp.dyhrs = r.allExtension.schedulingExtension.expectedDailyHours
                    tmp.wkhrs = r.allExtension.schedulingExtension.expectedWeeklyHours
                    tmp.pphrs = r.allExtension.schedulingExtension.expectedByPayPeriodHours

                    scheduleGroup = getEffVersion(r.allExtension.schedulingExtension.groupAssignments,snapshotDate)
                    tmp.scheduleGroup = ''
                    if (scheduleGroup) tmp.scheduleGroup = scheduleGroup.group
                    
                    tmp.dap = r.allExtension.employeeExtension.preferenceProfile
                    tmp.fap = r.allExtension.employeeExtension.accessProfile
                    tmp.timezone = r.allExtension.employeeExtension.timeZone
                    tmp.localeProfile = r.allExtension.employeeExtension.localeProfile
                    tmp.notificationProfile = r.allExtension.employeeExtension.notificationProfile
                    
                    payrule = getEffVersion(r.allExtension.timekeepingExtension.directPayRuleEntries,snapshotDate)
                    tmp.payrule = ''
                    tmp.payruleEffDt = ''
                    if (payrule) {
                        tmp.payrule = payrule.payRuleName
                        tmp.payruleEffDt = payrule.effectiveDate
                    }
                    
                    primJobInfo = getEffVersion(r.allExtension.employeeExtension.effDatedPrimaryJobAccountEntries,snapshotDate)
                    tmp.primJob = ''
                    tmp.labcat = ''
                    tmp.primJobEffDt = ''
                    if (primJobInfo) {
                        tmp.primJob = primJobInfo.primaryJob
                        tmp.labcat = primJobInfo.primaryLaborCategory
                        tmp.primJobEffDt = primJobInfo.effectiveDate
                    }
                    
                    eTerm = getEffVersion(r.allExtension.timekeepingExtension.employeeTerms,snapshotDate)
                    tmp.empTerm = ''
                    tmp.empTermEffDt = ''
                    if (eTerm) {
                        tmp.empTerm = eTerm.employmentTerm
                        tmp.empTermEffDt = eTerm.effectiveDate
                    }
                    
                    accrProf = getEffVersion(r.allExtension.accrualExtension.accrualProfilesEntries,snapshotDate)
                    tmp.accrProf = ''
                    tmp.accrProfEffDt = ''
                    if (accrProf)
                    {
                        tmp.accrProf = accrProf.accrualProfileName
                        tmp.accrProfEffDt = accrProf.effectiveDate
                    }
                     
                    fteInfo = getEffVersion(r.allExtension.accrualExtension.fullTimeEquivalency,snapshotDate)
                    tmp.ftepct = ''
                    tmp.ftehrs = ""                    //tmp.Full Time Hours
                    tmp.fteemphrs = ""
                    tmp.fteEffDt = ''

                    if (fteInfo) {
                        tmp.ftepct = fteInfo.fullTimeEquivalencyPercent
                        tmp.ftehrs = fteInfo.fullTimeStandardHoursQuantity
                        tmp.fteemphrs = fteInfo.employeeStandardHoursQuantity
                        tmp.fteEffDt = fteInfo.effectiveDate
                    }

                    basewage = getEffVersion(r.allExtension.timekeepingExtension.baseWages,snapshotDate)
                    tmp.basewage = ''
                    tmp.basewageEffDt = ''
                    if (basewage) {
                        tmp.basewage = basewage.hourlyRate
                        tmp.basewageEffDt = basewage.effectiveDate
                    }
                    
                    tmp.empcurrency = r.allExtension.timekeepingExtension.employeeCurrency.currencyCode
                    tmp.reportsto = r.allExtension.employeeExtension.supervisorPersonNumber
                    tmp.deviceGroup = r.allExtension.deviceExtension.deviceGroup

                    badgeNum = getEffVersion(r.allExtension.deviceExtension.badgeDetails,snapshotDate)
                    tmp.badgeNum = ''
                    tmp.badgeNumEffDt = ''
                    if (badgeNum) {
                        tmp.badgeNum = badgeNum.badgeNumber
                        tmp.badgeNumEffDt = badgeNum.effectiveDate
                    }                    

                    workEmpInfo = r.allExtension.timekeepingExtension.workEmployee
                    tmp.workProf = ''
                    tmp.dactivity = ''
                    tmp.iactivity = ''
                    tmp.pactivity = ''
                    tmp.upactivity = ''
                    if (workEmpInfo) {
                        tmp.workProf = workEmpInfo.profileName
                        tmp.dactivity = workEmpInfo.defaultActivityName
                        tmp.iactivity = workEmpInfo.idleActivityName
                        tmp.pactivity = workEmpInfo.paidActivityName
                        tmp.upactivity = workEmpInfo.unpaidActivityName
                    }
                    
                    
                    timeentry = getEffVersion(r.allExtension.timekeepingExtension.accessAssignmentDataEntries,snapshotDate)
                    tmp.timeentry = ''
                    tmp.timeentryEffDt = ''
                    if (timeentry)
                    {
                        tmp.timeentry = timeentry.timeEntryType
                        tmp.timeentryEffDt = timeentry.effectiveDate
                    }

                    tmp.EmployeePayCodeProfile = r.allExtension.timekeepingExtension.accessAssignmentDetailsDataEntries.ssePayCode
                    tmp.EmployeeWorkRuleProfile = r.allExtension.timekeepingExtension.accessAssignmentDetailsDataEntries.sseWorkRuleProfile
                    tmp.EmployeeLaborCategoryProfile = r.allExtension.timekeepingExtension.accessAssignmentDetailsDataEntries.laborCategoryProfile
                    tmp.LaborCategoryManagerAdditions = r.allExtension.timekeepingExtension.accessAssignmentDetailsDataEntries.sseMgrLaborCategoryProfile
                    
                    gdap = getEffVersion(r.allExtension.employeeExtension.dataAccessExtensionGroups,snapshotDate)
                    tmp.gdap = ''
                    tmp.gdapEffDt = ''
                    if (gdap)
                    {
                        tmp.gdap = gdap.dataAccessGroup
                        tmp.gdapEffDt = gdap.effectiveDate
                    }
                    jtsInfo = getEffVersion(r.allExtension.schedulingExtension.jobTransferEntries,snapshotDate)
                    tmp.paeffdt = ''
                    tmp.pajts = ''
                    tmp.pamajts = ''
                    tmp.paoset = ''
                    tmp.paegroup = ''
                    tmp.pahhypfind = ''
                    if (jtsInfo)
                    {
                        tmp.paeffdt = jtsInfo.effectiveDate
                        tmp.pajts = jtsInfo.professionalTransferOrganizationSet
                        tmp.pamajts = jtsInfo.empMgrTransferOrganizationSet
                        tmp.paoset = jtsInfo.managerTransferOrganizationSet
                        tmp.paegroup = jtsInfo.managerAccessOrganizationSet
                        tmp.pahhypfind = jtsInfo.homeHyperFindQueryName
                    }
                    
                    tmp.mgrwruleprof = r.allExtension.timekeepingExtension.accessAssignmentDetailsDataEntries.workRuleProfile
                    tmp.mgrpceprof = r.allExtension.timekeepingExtension.accessAssignmentDetailsDataEntries.payCodeProfile
                    tmp.mgrpcvprof = r.allExtension.timekeepingExtension.accessAssignmentDetailsDataEntries.payCodeViewProfile
                    tmp.mgrlabcatprof = r.allExtension.timekeepingExtension.accessAssignmentDetailsDataEntries.sseMgrLaborCategoryProfile
                    tmp.pattmpprof = r.allExtension.schedulingExtension.schedulePattern
                    tmp.shifttempprof = r.allExtension.schedulingExtension.shiftCode
                    tmp.schgrpprof = r.allExtension.schedulingExtension.groupSchedule
                    tmp.repprof = r.allExtension.timekeepingExtension.accessAssignmentDetailsDataEntries.reportName
                    tmp.fcmaprof = r.allExtension.schedulingExtension.forecastingCategoryProfile
                    tmp.dlgprof = r.allExtension.employeeExtension.delegateProfile
                    
                    userCurrency = r.allExtension.timekeepingExtension.userCurrency
                    tmp.mgrcurpref = ''
                    if (userCurrency) tmp.mgrcurpref = userCurrency.currencyCode

                    lmap = determineLicenses(r.allExtension.employeeExtension.licenseTypeList)
                    tmp.TimekeepingLicense = lmap.TimekeepingLicense
                    tmp.SchedulingLicense = lmap.SchedulingLicense
                    tmp.WAMLicense = lmap.WAMLicense
                    tmp.AnalyticsLicense = lmap.AnalyticsLicense
                    tmp.WorkLicense = lmap.WorkLicense
                    tmp.EmployeeLicense = lmap.EmployeeLicense
                    tmp.ManagerLicense = lmap.ManagerLicense

                    cdata = r.allExtension.employeeExtension.personCustomDataEntries
                    tmp.customDataNames = ''
                    tmp.customDataValues = ''
                    if (cdata) tmp.customDataNames = cdata.map((v)=>{ return v.customDataType; }).join("|");
                    if (cdata) tmp.customDataValues = cdata.map((v)=>{ return v.customText; }).join("|");

                    cdates = r.allExtension.employeeExtension.personDatesEntries
                    tmp.customDateNames = ''
                    tmp.customDatevalues = ''
                    if (cdata) tmp.customDateNames = cdata.map((v)=>{ return v.dateName; }).join("|");
                    if (cdata) tmp.customDatevalues = cdata.map((v)=>{ return v.overrideDate; }).join("|");
                    
                    tmp.overrideEffDt = ''

                    //FIX ME AT SOME POINT
                    //matchVal = ''
                    //if (formData.filter.value) matchVal = new RegExp('/^123.*$/')
                    //console.log(formData.filter.value,matchVal)
                    //if (!matchVal) o.data.push(tmp)
                    //else if (formData.filter.value.match(matchVal)) o.data.push(tmp)


                    if (formData.filter.value) {
                        filter = formData.filter.value
                        filter = '^' + filter.split('*').join('.*') + '$'
                        regex = new RegExp(filter,'g');
                        if (tmp.personNumber.match(regex)) o.data.push(tmp)
                    }
                    else o.data.push(tmp)
                })
                //console.log('exportRequestToOutput',o)
                return o
            },
            descHTML: `
            <p>This exports personality data.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
              <tr><th scope="row">Person Number*</th><td>Text</td><td></td></tr>
              <tr><th>First Name</th><td>Text</td><td></td></tr>
              <tr><th>Last Name*</th><td>Text</td><td></td></tr>
              <tr><th>Short Name</th><td>Text</td><td></td></tr>
              <tr><th>Middle Initial</th><td>Text</td><td></td></tr>
              <tr><th>Hire Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Employment Status</th><td>Text</td><td></td></tr>
              <tr><th>Employment Status Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>User Account Status</th><td>Text</td><td></td></tr>
              <tr><th>User Account Status Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Authentication Type</th><td>Text</td><td></td></tr>
              <tr><th>Username</th><td>Text</td><td></td></tr>
              <tr><th>Password</th><td>Text</td><td>Blank</td></tr>
              <tr><th>Logon Profile</th><td>Text</td><td></td></tr>
              <tr><th>MFA Required?</th><td>Text</td><td></td></tr>
              <tr><th>Analytics Type</th><td>Text</td><td></td></tr>
              <tr><th>Analytics Type Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Phone Label</th><td>Text</td><td></td></tr>
              <tr><th>Phone Number</th><td>Text</td><td></td></tr>
              <tr><th>Phone SMS?</th><td>Text</td><td></td></tr>
              <tr><th>State</th><td>Text</td><td></td></tr>
              <tr><th>City</th><td>Text</td><td></td></tr>
              <tr><th>Zip Code</th><td>Text</td><td></td></tr>
              <tr><th>Country</th><td>Text</td><td></td></tr>
              <tr><th>Worker Type</th><td>Text</td><td></td></tr>
              <tr><th>Email</th><td>Text</td><td></td></tr>
              <tr><th>Expected Daily Hours</th><td>Text</td><td></td></tr>
              <tr><th>Expected Weekly Hours</th><td>Text</td><td></td></tr>
              <tr><th>Expected PP Hours</th><td>Text</td><td></td></tr>
              <tr><th>Schedule Group</th><td>Text</td><td></td></tr>
              <tr><th>Display Profile</th><td>Text</td><td></td></tr>
              <tr><th>Function Access Profile</th><td>Text</td><td></td></tr>
              <tr><th>Time Zone</th><td>Text</td><td></td></tr>
              <tr><th>Locale</th><td>Text</td><td></td></tr>
              <tr><th>Notification Profile</th><td>Text</td><td></td></tr>
              <tr><th>Pay Rule*</th><td>Text</td><td></td></tr>
              <tr><th>Pay Rule Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Primary Job*</th><td>Text</td><td></td></tr>
              <tr><th>Labor Category</th><td>Text</td><td></td></tr>
              <tr><th>Primary Labor Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Employment Term</th><td>Text</td><td></td></tr>
              <tr><th>Employment Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Accrual Profile</th><td>Text</td><td></td></tr>
              <tr><th>Accrual Profile Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Full Time %</th><td>Text</td><td></td></tr>
              <tr><th>Full Time Hours</th><td>Text</td><td></td></tr>
              <tr><th>Full Time Employee Hours</th><td>Text</td><td></td></tr>
              <tr><th>Full Time Eq. Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Wage Rate</th><td>Text</td><td></td></tr>
              <tr><th>Wage Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Wage Currency</th><td>Text</td><td></td></tr>
              <tr><th>Reports To ID</th><td>Text</td><td></td></tr>
              <tr><th>Device Group</th><td>Text</td><td></td></tr>
              <tr><th>Badge Number</th><td>Text</td><td></td></tr>
              <tr><th>Badge Number Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Work Profile Name</th><td>Text</td><td></td></tr>
              <tr><th>Default Activity</th><td>Text</td><td></td></tr>
              <tr><th>Idle Activity</th><td>Text</td><td></td></tr>
              <tr><th>Paid Activity</th><td>Text</td><td></td></tr>
              <tr><th>Unpaid Activity</th><td>Text</td><td></td></tr>
              <tr><th>Time Entry Method</th><td>Text</td><td></td></tr>
              <tr><th>Time Entry Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Employee Pay Code Profile</th><td>Text</td><td></td></tr>
              <tr><th>Employee Work Rule Profile</th><td>Text</td><td></td></tr>
              <tr><th>Employee Labor Category Profile</th><td>Text</td><td></td></tr>
              <tr><th>Labor Category Manager Additions</th><td>Text</td><td></td></tr>
              <tr><th>GDAP</th><td>Text</td><td></td></tr>
              <tr><th>GDAP Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>Person Access Eff Date</th><td>Date</td><td>2020-01-01</td></tr>
              <tr><th>PA - Employee JTS</th><td>Text</td><td></td></tr>
              <tr><th>PA - JTS Manager Additions</th><td>Text</td><td></td></tr>
              <tr><th>PA - Organisational Set</th><td>Text</td><td></td></tr>
              <tr><th>PA - Employee Group</th><td>Text</td><td></td></tr>
              <tr><th>PA - Home Hyperfind</th><td>Text</td><td></td></tr>
              <tr><th>Manager Work Rule</th><td>Text</td><td></td></tr>
              <tr><th>Manager Pay Code Edit</th><td>Text</td><td></td></tr>
              <tr><th>Manager Pay Code View</th><td>Text</td><td></td></tr>
              <tr><th>Manager Labor Category Profile</th><td>Text</td><td></td></tr>
              <tr><th>Pattern Template Profile</th><td>Text</td><td></td></tr>
              <tr><th>Shift Template Profile</th><td>Text</td><td></td></tr>
              <tr><th>Schedule Group Profile</th><td>Text</td><td></td></tr>
              <tr><th>Report Profile</th><td>Text</td><td></td></tr>
              <tr><th>Forecasting Category Profile</th><td>Text</td><td></td></tr>
              <tr><th>Delegate Profile</th><td>Text</td><td></td></tr>
              <tr><th>Manager Currency Preference</th><td>Text</td><td></td></tr>
              <tr><th>Timekeeping License</th><td>Text</td><td></td></tr>
              <tr><th>Scheduling License</th><td>Text</td><td></td></tr>
              <tr><th>WAM License</th><td>Text</td><td></td></tr>
              <tr><th>Analytics License</th><td>Text</td><td></td></tr>
              <tr><th>Work License</th><td>Text</td><td></td></tr>
              <tr><th>Employee License</th><td>Text</td><td></td></tr>
              <tr><th>Manager License</th><td>Text</td><td></td></tr>
              <tr><th>Custom Data Names</th><td>Text</td><td>CName1|CName2|..</td></tr>
              <tr><th>Custom Data Values</th><td>Text</td><td>CValue1|CValue2|..</td></tr>
              <tr><th>Custom Date Names</th><td>Text</td><td>CDate1|Cdate2|...</td></tr>
              <tr><th>Custom Date Values</th><td>Text</td><td>CValue1|CValue2|..</td></tr>
              <tr><th>Override Effective Date</th><td>Date</td><td>Blank</td></tr>
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            `
        },
        {
            type_name: 'Person Assignments',                
            prompts: [
                {label: 'Hyperfind Query',name: 'hyperfind_query',datasource: {apiurl: '/v1/commons/hyperfind/public',tag: 'name',wrap:'hyperfindQueries'}},
                {label: 'Snapshot Date',name: 'start_date',type:'date'},
                {label: 'Person Number Filter (Optional)',name: 'filter',info:"Example: (123* | *123 | 123456) * as wildcard"}
            ],
            promptModalAdjust: ()=>{ //ALLOWS FOR ADJUSTMENT OF STOCK RENDERED MODAL (each field in a new form group with label)
                $('#exportModal').find('#export__hyperfind_query')[0].selectize.setValue('1')
                $('#exportModal').find('#export__start_date').data('datepicker').setDate(new Date())
                //$('#exportModal').find('#export__filter_formgroup').append('<span></span>')
                ////$('#exportModal').find('#export__start_date_formgroup').append('<div id="exportDateGrouper" class="pull-right"></div>')
                ////$('#exportModal').find('#export__end_date_formgroup').find('label').detach().appendTo('#exportDateGrouper')
                ////$('#exportModal').find('#export__end_date').detach().appendTo('#exportDateGrouper')
            },
            promptValidation: (fields,et)=>{
                errMsg = ''
                if (!fields['hyperfind_query'].value) errMsg = 'You must select a Hyperfind Query.'                
                if (!fields['start_date'].value) errMsg = 'You must supply a snapshot date.' 
                return errMsg
            },
            apiRequest: async (data)=> {
				tmpapiArr = []

                let employees = await QLIPTypes.exportHelpers.execHyperfind(data.hyperfind_query,{"endDate": data.start_date,"startDate": data.start_date})
				
				//ADJUSTMENT RULES
				x = {
                    apiUrl: '/v1/commons/persons/adjustment_rule/multi_read',
                    apiType: 'ADJUSTMENT_RULES',
					apiBatchQty:100,
					apiThreads:5,
                    apiBatchOn: 'where.employees.values',
                    apiRequest:function() {
                        y = {
                            "failOnNoAssignment": false,
                            "returnUnassignedEmployees": false,
                            "where": {
                              "employees": {
                                "key": "personnumber",
                                "values": employees
                              },
                              "extensionType": "employee",
                              "snapshotDate": data.start_date
                            }
                          }
                        return y
                    }
                }
                
                tmpapiArr.push(x)

				//CASCADE PROFILES
				x = {
                    apiUrl: '/v1/commons/persons/cascade_profile/multi_read',
                    apiType: 'CASCADE_PROFILES',
					apiBatchQty:100,
					apiThreads:5,
                    apiBatchOn: 'where.employees.values',
                    apiRequest:function() {
                        y = {
                            "failOnNoAssignment": false,
                            "returnUnassignedEmployees": false,
                            "where": {
                              "employees": {
                                "key": "personnumber",
                                "values": employees
                              },
                              "extensionType": "employee",
                              "snapshotDate": data.start_date
                            }
                          }
                        return y
                    }
                }
                
                tmpapiArr.push(x)
                return tmpapiArr
            },
            requestToOutput: function(response) {
                headers = ['Person Number','Assignment','Assignment Value','Effective Date','Expiration Date']
				
                o = {'headers':headers,data: []}

                if (response.ADJUSTMENT_RULES) {
                    response.ADJUSTMENT_RULES.forEach((ar)=>{
                        to = {}
                        to.pnum = ar.personIdentity.personNumber
                        to.assign = 'Adjustment Rule'
                        to.val = ar.processor
                        to.effdt = ar.effectiveDate
                        to.expdt = ar.expirationDate
                        o.data.push(to)
                    })
                }

                if (response.CASCADE_PROFILES) {
                    response.CASCADE_PROFILES.forEach((cp)=>{
                        if (cp.assignmentProfile) {
                            to = {}
                            to.pnum = cp.personIdentity.personNumber
                            to.assign = 'Cascade Profile'
                            to.val = cp.assignmentProfile
                            to.effdt = ''
                            to.expdt = ''
                            o.data.push(to)
                        }
                    })
                }

                return o
            },
            descHTML: `
            <p>This export retrieves person assignment data.</p>
            <br>
            <!--Table-->
            <table id="tablePreview" class="table table-striped table-hover table-sm">
            <!--Table head-->
              <thead>
                <tr>
                  <th>Field Name</th>
                  <th>Field Type</th>
                  <th>Example</th>
                </tr>
              </thead>
              <!--Table head-->
              <!--Table body-->
              <tbody>
                <tr><th>Person Number</th><td>Text</td><td>12345</td></tr>
                <tr><th>Assignment</th><td>Text</td><td>Cascade Profile, Adjustment Rule, etc. Points</td></tr>
                <tr><th>Assignment Value Name</th><td>Text</td><td>Profile Name,etc.</td></tr>
                <tr><th>Effective Date</th><td>Date</td><td>2020-01-01</td></tr>
                <tr><th>Expiration Date</th><td>Date</td><td>3000-01-01</td></tr>
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
            <p>
            <strong>Currently Includes:</strong> Cascade Profile's, Adjustment Rules.
            </p>
            `
        }
    ]
}
