var tmp_pkg_pth;
var upd_zip_pth;
var upd_zip_fld;
var upd_pth;
var srv_version;
var srv_url;

//Initiate Auto Update Check
//================================================================================
var intervalUpdate
var startChecks = function(minutes) {
      checkForUpdates()
  intervalUpdate = setInterval(function() {
      checkForUpdates()
  }, minutes * 60000); 
}

var stopChecks = function() {
  clearTimeout(intervalUpdate);
}

//BRAINS
//================================================================================
function checkForUpdates()
{
  upd_pth = cwd + '/_update'
  cur_package = getPackageDetails(cwd + '/package.json')
  
  $.ajax({
      cache: false,
      url: cur_package['manifestUrl'],
      dataType: "json",
      success: function(srv_package) {
          srv_version = srv_package['version']
          srv_url = srv_package['downloadUrl']
          if (cur_package['version'] != srv_version) showNewVersionVisuals()
      }
  })
}

function autoUpdate()
{
  upd_zip_pth = srv_version + '.zip'
  upd_zip_fld = upd_pth + '\\' + upd_zip_pth.replace('.zip','')
  upd_zip_pth = upd_pth + '\\' + upd_zip_pth  
  cleanup()

  //DOWNLOAD ZIP
  download(srv_url,upd_zip_pth,function(err){
      try { 
        //UNZIP
        var zip = new AdmZip(upd_zip_pth);
        zip.extractAllTo(upd_zip_fld,true);
        master_pth = cwd
        //master_pth = upd_pth + '\\testing' //TESTING   

        //RECURSIVE COPY 
        fs.copy(upd_zip_fld, master_pth, function (err) {
          if (err) { 
            console.error(err) 
            showUpdateVisuals(true)
          } 
          else {
              jc.close()
              showUpdateVisuals(false)
			  deleteFolderRecursive(upd_pth)
			  //Cleanup Current Folder Structure.
			  deleteFolderRecursive(upd_pth.substring(0, upd_pth.length - 7) + ".vscode")
			  deleteFolderRecursive(upd_pth.substring(0, upd_pth.length - 7) + "MOVE_TO_REPO")
		      deleteFolderRecursive(upd_pth.substring(0, upd_pth.length - 7) + "Quickload  - WFD Export")
			  deleteFolderRecursive(upd_pth.substring(0, upd_pth.length - 21) + ".vs")
			  		  
			  
          }
        }); //copies directory, even if it has subdirectories or files     
      } 
      catch (e) { 
        console.log(e)
        showUpdateVisuals(true) }
  })
}

function getDirectories(path) {
  return fs.readdirSync(path).filter(function (file) {
    return fs.statSync(path+'/'+file).isDirectory();
  });
}

//Helpers
//================================================================================
function getPackageDetails(pth,type) {
  var obj = JSON.parse(fs.readFileSync(pth, 'utf8'))
  return obj
}

function getFoldersFromDirectory(path)
{
  return fs.readdirSync(path).filter(function (file) {
    return fs.statSync(path+'/'+file).isDirectory();
  });
}

function makeFolderIfNotExist(pth)
{
  //MAKE FOLDER IF NOT EXIST
  if (!fs.existsSync(pth)){ fs.mkdirSync(pth) }
}

var download = function(url, dest, cb) {
  var file = fs.createWriteStream(dest)
  var urlparser = document.createElement('a');
  urlparser.href = url
  var options = {host:urlparser.hostname,path:url,headers:{'Cache-Control':'no-cache'}}
  var request = https.get(options, function(response) {
    response.pipe(file)
    file.on('finish', function() {
      file.close(cb)  // close() is async, call cb after close completes.
    });
  }).on('error', function(err) { // Handle errors
    fs.unlink(dest); // Delete the file async. (But we don't check the result)
    if (cb) cb(err.message)
  })
}


function deleteFile(pth)
{
  if (fs.existsSync(cfile)){ fs.unlink(cfile) }
}

function getDirectories(path) {
  return fs.readdirSync(path).filter(function (file) {
    return fs.statSync(path+'/'+file).isDirectory();
  });
}

function showNewVersionVisuals()
{
    $.confirm({
      title: 'New Version Available!',
      type: 'green',
      useBootstrap:false,
      boxWidth:'50%',
      content: 'A new version is available! Would you like to update now?',
      buttons: {
          update: {
              text: 'Update Now',
              keys: ['enter'],
              action: function(){
                  jc = $.dialog({
                    title: 'Now Updating!',
                    type: 'green',
                    content: 'Please wait...'                             
                  });
                  autoUpdate()
              }
          },
          cancel: {
              text: "Later...",
              keys: ['esc'],
              action: function(){                                                              
                
                stopChecks();
              }
          }
      }
  });
}

function showUpdateVisuals(err)
{
  if (!(err))
    {
      $.confirm({
        title: 'Update Complete!',
        type: 'green',
        useBootstrap:false,
        boxWidth:'50%',
        content: 'The application will need to be restarted for the updated changes to take affect. Would you like to restart now?',
        buttons: {
            close: {
                text: 'Yes',
                keys: ['enter'],
                action: function(){
                    location.reload()                  
                }
            }
        }
      });
    }
  else
    {
      $.confirm({
        title: 'Update Error!',
        type: 'red',
        useBootstrap:false,
        boxWidth:'50%',
        content: 'There was a problem with the update. The application will be restarted.',
        buttons: {
            close: {
                text: 'Yes',
                keys: ['enter'],
                action: function(){
                    location.reload()                  
                }
            }
        }
      });
    }
}

function cleanup()
{
  if (fs.existsSync(upd_pth)){ deleteFolderRecursive(upd_pth) }
  makeFolderIfNotExist(upd_pth)
}

function deleteFolderRecursive(path) 
{
  try {
    if( fs.existsSync(path) ) {
      fs.readdirSync(path).forEach(function(file,index){
        var curPath = path + "/" + file;
        if(fs.lstatSync(curPath).isDirectory()) { // recurse
          deleteFolderRecursive(curPath);
        } else { // delete file
          fs.unlinkSync(curPath);
        }
      });
      fs.rmdirSync(path);
    }
  }
  catch (e) { 
      console.log(e)
      showUpdateVisuals(true)
  }
}