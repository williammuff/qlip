var prv_conn = ""
function populateConnections(conn,del_flg)
{
    //console.log('populate connections')
    if (!fs.existsSync(conn_path))  fs.writeFileSync(conn_path,'')
    conns = getLocalConnections()
    if (conns)
    {
        var selectize = $('#connectionsSelect').get(0).selectize;
        if (selectize) selectize.destroy()
         $('#connectionsSelect').selectize({
            persist: false,
            maxItems: 1,
            valueField: 'name',
            labelField: 'name',
            searchField: ['name', 'url'],
            options: conns,
            render: {
                item: function(item, escape) {
                    return '<div>' +
                        '<span class="name">' + escape(item.name) + '</span>' + ' - ' +
                        '<small class="url">' + escape(item.url) + '</small>' +
                    '</div>';
                },
                option: function(item, escape) {
                    return '<div>' +
                        '<span class="label">' + escape(item.name) + '</span>' +
                        '<small class="caption">' + escape(item.url) + '</small>' +
                    '</div>';
                }
            },
            onDropdownClose: function (value) {
                tmp_prv_conn = prv_conn
                if (gl_table_edited)
                {
                    disregardChanges(function(confirmed) {
                        if (confirmed) {
                            connectionSelectizeDropDownClose(value)
                            gl_table_edited = false;
                        }
                        else {
                            so = $('#connectionsSelect').get(0).selectize;
                            so.setValue(tmp_prv_conn)
                        }
                    })
                }
                else connectionSelectizeDropDownClose(value)
            },
            onChange: function (t) {
               prv_conn = t
            }
        });
        
        if (conns.length == 0) {
            $('#connectionsSelect')[0].selectize.disable()
            addWFDConnection()
        }
        else $('#connectionsSelect')[0].selectize.enable() 
    }
    
    if (conn) {
        $('#connectionsSelect')[0].selectize.setValue(conn.name)
        //$('.tables').css('visibility','visible') //mrb remove, lookup tables no longer needed
    }

    if (del_flg) {
        $('#data_table').css('visibility','hidden')
        $('.otherNav').css('visibility','hidden')
        $('.tables').css('visibility','hidden')
    }
}

function connectionSelectizeDropDownClose(v) {
    so = getSelectizeOption('#connectionsSelect')
    if (so) 
    {
        //HIDE OTHER NAV AND UPLOAD SELECTION BOX
        //$('.otherNav').css('visibility','hidden')
        //$('#uploadSelectionBox').css('visibility','hidden')
        //$('#data_table').css('visibility','hidden')

        //TODO-connections
        $('#wfd_alias').val(so.name);
        $('#wfd_url').val(so.url);
        $('#wfd_tenantId').val(so.tenantId);
        $('#wfd_uname').val(so.username);
        
        $('#wfd_passwd').val('')
        $('#connectionModal').modal()

        ps = so.password
        $('#wfd_passwd').val(ps)
        $('#connectionModal').modal()
        
        $('#wfd_clientId').val(so.clientId);
        $('#wfd_clientSecret').val(so.clientSecret);
        $('#wfd_appKey').val(so.appKey);
        //$('#wfd_connect').focus()
        $('#wfd_alias').focus()

        $('#detailedLoginOptions').hide()
        if (so.username) $('#detailedLogin').click()

        return so
    }
}

function getLocalConnections()
{
   conn_str = fs.readFileSync(conn_path,"utf-8")
   if (conn_str) return JSON.parse(conn_str)
   else return []
}

function setLocalConnections(cur_conns)
{
    //console.log('setLocalConnections')
    cur_conns = sortByKey(cur_conns,'name')
    fdata = JSON.stringify(cur_conns)
    fs.writeFileSync(conn_path,fdata)
}

function addUpdateConnection(conn,del_flg)
{
    //console.log('addUpdateConnections')
    var add_conn = true
    var del_conn_index = null
    cur_conns = getLocalConnections()
    $(cur_conns).each(function(k,v){
        //if (conn.name == v.name) {
        if (conn.url == v.url) {
            add_conn = false
            cur_conns[k] = {name: conn.name, url: conn.url, tenantId: conn.tenantId, username: conn.username, password: conn.password, clientId: conn.clientId, clientSecret: conn.clientSecret, appKey: conn.appKey}
            if(del_flg) del_conn_index = k
        }
    })

    if (del_conn_index != null) cur_conns.splice(del_conn_index,1)
    if (add_conn) cur_conns.push(conn)
    setLocalConnections(cur_conns)
    populateConnections(conn,del_flg)
}